import React, {createContext, useEffect, useState} from "react";
import Axios from "axios";
import nextId from "react-id-generator";
import upcomingimg from "../static/images/navbar-icons/upcomingimg.png";
import meeticon from "../static/images/meeticon.png";
import Icon from "@ant-design/icons";
import {Card} from "antd";
import ReactPlayer from "react-player";

export const CounselAppContext = createContext();

function CounselAppContextProvider({children}) {
  const [email, setEmail] = useState(
    localStorage.getItem("CounselAppLoginAccount") || ""
  );
  const [accessToken, setAccessToken] = useState(
    localStorage.getItem("CounselAppAccessToken") || ""
  );
  const [token, setToken] = useState(
    localStorage.getItem("CounselAppToken") || ""
  );
  const [socket, setSocket] = useState(null);

  const [Name, setName] = useState("");
  const [host, setHost] = useState(false);
  const [secondtreamStep, setSecondStreamStep] = useState(0);
  const [description, setDescription] = useState("");
  const [currentDashboard, setCurrentDashboard] = useState("");
  const [profilePic, setProfilePic] = useState("");
  const [profileName, setProfileName] = useState("");
  const [meetingRoom, setMeetingRoom] = useState("");
  const [drawer, setDrawer] = useState("");
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [presentMeeting, setPresentMeeting] = useState({});
  const [profileData, setProfileData] = useState(null);
  const [step, setStep] = useState("base");
  const [currentDrawer, setCurrentDrawer] = useState("base");
  const [stepStream, setStepStream] = useState("stream-base");
  const [currentStreamDrawer, setCurrentStreamDrawer] = useState("stream-base");
  const [view, setview] = useState("w-100 h-100");
  const [drawerType, setDrawerType] = useState("");
  const [Secondstep, setSecondStep] = useState(0);
  const [temp, settemp] = useState("");
  const [loading, setLoading] = useState(false);
  const [subDomainData, setSubDomainData] = useState({});
  const [brainDetails, setBrainDetails] = useState("");
  const [chatOpen, setChatOpen] = useState(
    window.innerWidth < 600 ? false : true
  );
  const [cohostName, setCoshostName] = useState("");
  const [imageToShow, setImageToShow] = useState(null);
  const [visibleShowImage, setVisibleShowImage] = useState(false);
  const [userGXAccount, setUserGXAccount] = useState();
  const [userObj, setUserObj] = useState({});
  const [chatboxType, setChatBoxType] = useState("");
  const [thetab1, settab1] = useState(false);
  const [switchprofile, setswitchprofile] = useState("view");
  const [hostName, setHostName] = useState("");
  const [coHosts, setCohosts] = useState([]);
  const [visible, setVisible] = useState(false);
  const [visible1, setVisible1] = useState(false);
  const [visible2, setVisible2] = useState(false);
  const [visible3, setVisible3] = useState(false);
  const [deskType, setDeskType] = useState("record");
  const validateJson = (str) => {
    try {
      return <div dangerouslySetInnerHTML={{__html: JSON.parse(str)}} />;
    } catch (e) {
      return str;
    }
  };

  const validateJsonUrl = (str) => {
    try {
      // console.log("parsed val", JSON.parse(str).text());
      var span = document.createElement("span");
      span.innerHTML = JSON.parse(str);
      return span.textContent || span.innerText;
      // return JSON.parse(str);
    } catch (e) {
      return str;
    }
  };
  const renderMessage = (item) => {
    if (item.type === "file") {
      if (
        item.location.slice(item.location.lastIndexOf(".") + 1) === "png" ||
        item.location.slice(item.location.lastIndexOf(".") + 1) === "PNG" ||
        item.location.slice(item.location.lastIndexOf(".") + 1) === "jpg" ||
        item.location.slice(item.location.lastIndexOf(".") + 1) === "JPG" ||
        item.location.slice(item.location.lastIndexOf(".") + 1) === "jpeg" ||
        item.location.slice(item.location.lastIndexOf(".") + 1) === "JPEG"
      ) {
        return (
          <div
            onClick={(e) => {
              setImageToShow(item.location);
              setVisibleShowImage(true);
            }}
          >
            <img
              className="light-shadow-bottom"
              src={item.location}
              style={{width: "350px", borderRadius: "10px"}}
              alt="item"
            />
          </div>
        );
      } else {
        return (
          <div onClick={(e) => window.open(item.location, "_blank")}>
            <Icon type="file" />{" "}
            {item.message.length > 20 ? (
              <>
                {item.message.substring(0, 10)}.......
                {item.message.substring(
                  item.message.length - 10,
                  item.message.length
                )}
              </>
            ) : (
              item.message
            )}
          </div>
        );
      }
    } else if (item.type === "transfer") {
      return (
        <>
          <Card bodyStyle={{padding: "15px"}} className="shadow-sm">
            <div
              style={{
                fontSize: window.innerWidth > 600 ? "30px" : "20px",
                fontWeight: "bold",
              }}
            >
              {item.message}
            </div>
            <div>SENT</div>
          </Card>
        </>
      );
    } else if (item.type === "video") {
      return (
        <ReactPlayer
          width="350px"
          height="200px"
          url={validateJsonUrl(item.message)}
          controls
          playing={true}
          light={true}
        />
      );
    } else {
      return (
        <div
          style={{
            display: "inline-block",
            wordBreak: "break-word",
          }}
        >
          {validateJson(item.message)}
        </div>
      );
    }
  };
  const [thebilltab, setbilltab] = useState([
    {
      key: 1,
      tabName: "App Config ",
      currency: "",
      tab: "bills-full-search-lx",
      imgTab: meeticon,
    },
  ]);
  const [selectedTab, setSelectedTab] = useState(
    thebilltab[thebilltab.length - 1]
  );
  const [user_psa_id, setuser_psa_id] = useState("");
  const [meetingLink, setMeetingLink] = useState("");
  const [dashboardComponent, setDashboardComponent] = useState({
    key: "1",
    name: "bills-full-search-lx",
    type: "bills",
  });
  // useEffect(() => {
  //   console.log("setting email");
  //   if (email !== "") localStorage.setItem("CounselAppLoginAccount", email);
  // }, [email]);

  // useEffect(() => {
  //   localStorage.setItem("CounselAppAccessToken", accessToken);
  // }, [accessToken]);

  // useEffect(() => {
  //   console.log("setting token");

  //   localStorage.setItem("CounselAppToken", token);
  // }, [token]);

  // useEffect(() => {
  //   setSelectedTab(thebilltab[0])
  // })

  const billstab1 = (array) => {
    setbilltab([...array], () => console.log("bills tab", thebilltab));
  };
  const getSelectedTab = () => {
    return selectedTab;
  };
  const theSelectedTab = (obj) => {
    setSelectedTab(obj);
    // this.setState({ dashboardVaults: { currency: obj.currency, amount: '' } });
  };
  const changeDashboardComponent = (obj) => {
    console.log("sel", JSON.stringify(selectedTab));
    console.log(obj.key, JSON.stringify(selectedTab));
    setDashboardComponent(obj, () => console.log("x", dashboardComponent));
  };
  const handleAddTab = (data) => {
    console.log("data", data);
    let id = nextId();
    setDashboardComponent({
      name: data.dashboard,
      type: data.type,
      key: id,
    });
    if (data.type.length !== 0) {
      setSelectedTab({key: id, ...data});
      billstab1([
        ...thebilltab,
        {
          key: id,
          tabName: data.tabName,
          imgTab: data.imgTab,
          tab: data.dashboard,
          // resultData: (data.e = data.resultData ? data.resultData : ''),
          // currency: data.dashboard === 'bills-vaults' ? data.currency : '',
        },
      ]);
    }
    console.log(selectedTab);
  };

  const getBrain = async () => {
    let data = {
      psa_id: "ea422e04-d561-4f3f-951a-db183b699bf0",
    };
    console.log(data);
    let res = await Axios.post(
      "https://psabck.pulse.stream/operator_brainid",
      data
    );
    console.log(res);
    setBrainDetails(res.data.payload);
  };
  const getSubDomain = async () => {
    return new Promise(async (resolve, reject) => {
      let res = await Axios.get(
        `https://ms.apimachine.com/api/subdomainlist`,

        {
          headers: {
            email: email,
            token: token,
          },
        }
      );
      resolve(res);
    });
  };
  const login = async (paramEmail, paramAccessToken, paramToken) => {
    localStorage.setItem("CounselAppLoginAccount", paramEmail);
    localStorage.setItem("CounselAppAccessToken", paramAccessToken);
    localStorage.setItem("CounselAppToken", paramToken);
    setEmail(paramEmail);
    setAccessToken(paramAccessToken);
    setToken(paramToken);
    setbilltab([
      {
        key: 1,
        tabName: "App Config  ",
        currency: "",
        tab: "bills-full-search-lx",
        imgTab: meeticon,
      },
    ]);
    setSelectedTab(thebilltab[thebilltab.length - 1]);
    if (paramEmail !== "" || paramToken !== "") {
      await getDetails(paramEmail, paramAccessToken, paramToken);
    }
  };
  const beforeLogout = () => {
    localStorage.setItem("current_meeting", "");
    localStorage.setItem("co-host", "");
    setStepStream("stream-base");
    setSecondStreamStep(0);
    setCurrentDrawer("base");
    setSecondStep(0);
    setDrawerOpen(false);
    setStep("base");
    setCurrentStreamDrawer("stream-base");
    handleAddTab({
      tabName: "Profile",
      detail: "new-tab",
      img: profilePic,
      dashboard: "New-Tab",
      type: "New-Tab",
      imgTab: profilePic,
    });
  };
  const getDetails = async (paramEmail, paramAccessToken, paramToken) => {
    if (
      localStorage.getItem("CounselAppToken") &&
      localStorage.getItem("CounselAppLoginAccount")
    ) {
      setLoading(true);
      if (!paramEmail && !paramToken) {
        let res = await Axios.post(
          "https://comms.globalxchange.com/coin/verifyToken",
          {
            email: paramEmail ? paramEmail : email,
            token: paramToken ? paramToken : token,
          }
        );
        if (!res.data.status) {
          login("", "", "");
        }
      }
      let res1 = await Axios.get(
        `https://comms.globalxchange.com/user/details/get?email=${
          paramEmail ? paramEmail : email
        }`
      );
      let body = {};
      let res2 = await Axios.post(
        "https://ms.apimachine.com/api/user/login",
        body,
        {
          headers: {
            email: paramEmail ? paramEmail : email,
            token: paramToken ? paramToken : token,
          },
        }
      );

      const {data} = res1;
      console.log("data :>> ", data);

      if (data.status && res2.data.success) {
        console.log(res2.data.data);
        setProfileData(res2.data.data);
        setProfileName(data.user.name);
        setProfilePic(data.user.profile_img);
        setLoading(false);
      } else {
        login("", "", "");
        setLoading(false);
      }
    }
  };
  const getMeetingDetails = async () => {
    return new Promise(async (resolve, reject) => {
      let res = await Axios.get(
        `https://ms.apimachine.com/api/meetings/${meetingRoom}`,
        {
          headers: {
            token: localStorage.getItem("chatToken"),
            email: localStorage.getItem("user_account"),
          },
        }
      );
      if (res.data.success) {
        setMeetingData(res.data.data);
        resolve({status: true});
      } else {
        reject({status: "false"});
      }
    });
  };
  //--------------------
  const [audioTypes, setAudioTypes] = useState([]);
  const [videoTypes, setVideoTypes] = useState([]);
  const [finalVideoType, setFinalVideoType] = useState([]);
  const [finalAudioType, setFinalAudioType] = useState([]);

  //--------------------

  const [selectedType, setSelectedType] = useState(null);
  const [selectedFriend, setSelectedFriend] = useState(null);
  const [visibleUserInfo, setVisibleUserInfo] = useState(false);
  const [groupReadUnreadList, setgroupReadUnreadList] = useState([]);
  const [supportgroupReadUnreadList, setsupportgroupReadUnreadList] = useState(
    []
  );
  const [selectedGroup, setSelectedGroup] = useState({});
  const [currentUserObj, setCurrentUserObj] = useState({});
  const [userReadUnreadList, setuserReadUnreadList] = useState([]);
  const [unreadInteractionList, setUnreadInteractionList] = useState([]); 
  const [selectedSupport, setSelectedSupport] = useState(null);
  const [messagearray, setmessagearray] = useState([]);
  const [groupMembers, setgroupMembers] = useState([]);
  const [droppedFile, setDroppedFile] = useState(null);
  const [clipImage, setClipImage] = useState("");
  const [visibleDroppedImage, setVisibleDroppedImage] = useState(false);
  const [selectedReplyMessage, setSelectedReplyMessage] = useState(null);

  const [allUserList, setallUserList] = useState([]);
  const [currentUserList, setcurrentUserList] = useState([]);
  const [messageLoading, setMessageLoading] = useState(false);
  const [friendsLoading, setFriendsLoading] = useState(false);
  const [groupsLoading, setGroupsLoading] = useState(false);
  const [supportLoading, setSupportLoading] = useState(false);

  const [selectedAdminSupport, setSelectedAdminSupport] = useState(null);
  const [archiveList, setArchiveList] = useState([]);
  const [typingFlag, setTypingFlag] = useState(false);
  const [typingUser, setTypingUser] = useState("");

  const [onlineUsers, setOnlineUsers] = useState([]);
  const [adminUsernames, setAdminUsernames] = useState([]);
  const [visibleControlPanel, setVisibleControlPanel] = useState(false);
  const [visibleProfile, setVisibleProfile] = useState(false);
  const [visibleVault, setVisibleVault] = useState(false);
  const [visibleMarket, setVisibleMarket] = useState(false);
  const [visibleActions, setVisibleActions] = useState(false);
  const [chatBoxArr, setChatBoxArr] = useState([]);
  const [activeTab, setActiveTab] = useState(0);

  const [visibleSetting, setVisibleSetting] = useState(false);
  const [visibleMailKings, setVisibleMailKings] = useState(false);

  //Multi Account Support

  const [newLogin, setNewLogin] = useState(false);
  const [accountArr, setAccountArr] = useState(
    localStorage.getItem("accountArr")
      ? JSON.parse(localStorage.getItem("accountArr"))
      : ""
  );

  //New Info Addition

  const [refreshGroup, setRefreshGroup] = useState(false);
  const [meetingData, setMeetingData] = useState(null);
  useEffect(() => {
    getDetails();
  }, []);

  let bodyRef = React.useRef();
  let app_id = "559af22d-5e79-4948-acf6-e48733ee1daa";
  const userReference = React.useRef(null);
  const screenReference = React.useRef(null);
  const [rc, setRC] = useState("");
  const [streamSocket, setStreamSocket] = useState(null);
  const [groupSocket, setGroupSocket] = useState(null);
  return (
    <CounselAppContext.Provider
      value={{
        beforeLogout,
        streamSocket,
        setStreamSocket,
        groupSocket,
        setGroupSocket,
        rc,
        setRC,
        userReference,
        screenReference,
        app_id,
        bodyRef,
        allUserList,
        setallUserList,
        unreadInteractionList,
        setUnreadInteractionList,
        userReadUnreadList,
        setuserReadUnreadList,
        selectedGroup,
        setSelectedGroup,
        selectedFriend,
        setSelectedFriend,
        visibleUserInfo,
        setVisibleUserInfo,
        groupReadUnreadList,
        setgroupReadUnreadList,
        supportgroupReadUnreadList,
        setsupportgroupReadUnreadList,
        currentUserObj,
        setCurrentUserObj,
        selectedSupport,
        getMeetingDetails,
        renderMessage,
        setSelectedSupport,
        messagearray,
        setmessagearray,
        groupMembers,
        setgroupMembers,
        droppedFile,
        setDroppedFile,
        clipImage,
        setClipImage,
        selectedReplyMessage,
        setSelectedReplyMessage,
        visibleDroppedImage,
        setVisibleDroppedImage,
        currentUserList,
        setcurrentUserList,
        messageLoading,
        cohostName,
        setCoshostName,
        setMessageLoading,
        setFriendsLoading,
        friendsLoading,
        coHosts,
        setCohosts,
        groupsLoading,
        setGroupsLoading,
        supportLoading,
        setSupportLoading,
        newLogin,
        setNewLogin,
        accountArr,
        setAccountArr,
        selectedAdminSupport,
        setSelectedAdminSupport,
        archiveList,
        setArchiveList,
        typingUser,
        setTypingUser,
        typingFlag,
        setTypingFlag,
        selectedType,
        setSelectedType,
        stepStream,
        setStepStream,
        onlineUsers,
        setOnlineUsers,
        adminUsernames,
        setAdminUsernames,
        visibleVault,
        secondtreamStep,
        setSecondStreamStep,
        setVisibleVault,
        visibleProfile,
        setVisibleProfile,
        visibleMarket,
        finalVideoType,
        finalAudioType,
        setFinalVideoType,
        setFinalAudioType,
        setVisibleMarket,
        visibleActions,
        setVisibleActions,
        drawerType,
        setDrawerType,
        visibleControlPanel,
        setVisibleControlPanel,
        chatBoxArr,
        setChatBoxArr,
        activeTab,
        setActiveTab,
        visibleSetting,
        setVisibleSetting,
        deskType,
        setDeskType,
        visibleMailKings,
        setVisibleMailKings,
        userObj,
        setUserObj,
        refreshGroup,
        audioTypes,
        setAudioTypes,
        setRefreshGroup,
        currentStreamDrawer,
        setCurrentStreamDrawer,
        chatOpen,
        setChatOpen,
        view,
        setview,
        userGXAccount,
        setUserGXAccount,
        thetab1,
        settab1,
        switchprofile,
        setswitchprofile,
        thebilltab,
        getBrain,
        setbilltab,
        chatboxType,
        setChatBoxType,
        selectedTab,
        hostName,
        setHostName,
        setSelectedTab,
        dashboardComponent,
        setDashboardComponent,
        billstab1,
        getSelectedTab,
        theSelectedTab,
        changeDashboardComponent,
        handleAddTab,
        subDomainData,
        setSubDomainData,
        videoTypes,
        setVideoTypes,
        login,
        meetingLink,
        setMeetingLink,
        loading,
        setLoading,
        email,
        token,
        setToken,
        setEmail,
        profilePic,
        socket,
        setSocket,
        profileName,
        host,
        Name,
        setName,
        setHost,
        description,
        meetingRoom,
        setMeetingRoom,
        setDescription,
        currentDashboard,
        getSubDomain,
        setCurrentDashboard,
        drawer,
        setDrawer,
        profileData,
        setProfileData,
        drawerOpen,
        setDrawerOpen,
        presentMeeting,
        setPresentMeeting,
        step,
        setStep,
        temp,
        settemp,
        brainDetails,
        setBrainDetails,
        currentDrawer,
        setCurrentDrawer,
        Secondstep,
        user_psa_id,
        setuser_psa_id,
        setSecondStep,
        visible,
        setVisible,
        visible1,
        setVisible1,
        visible2,
        setVisible2,
        visible3,
        setVisible3,
      }}
    >
      {children}
    </CounselAppContext.Provider>
  );
}

export default CounselAppContextProvider;
