import Axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { CounselAppContext } from "../../Context_Api/Context";
import { List, Avatar, Tag, Divider } from "antd";
import { deskStreamlogo } from "../../static/images/images";
import {
  videoImage,
  audioImage,
  pdfImage,
  powerpoint,
  unknown,
  excel,
  compressed,
  document,
  imageImage,
} from "../../static/images/images";
const CastFiles = () => {
  const context = useContext(CounselAppContext);
  const [flag, setFlag] = useState(false);
  const [data, setData] = useState();
  const [dataStats, setDataStats] = useState([]);
  const dataTypes = [
    {
      type: "image",
      image: imageImage,
      desc: "Images",
    },
    {
      type: "audio",
      image: audioImage,
      desc: "Audio Files",
    },
    {
      type: "video",
      image: videoImage,
      desc: "Videos",
    },
    {
      type: "doc",
      image: document,
      desc: "Documents",
    },
    {
      type: "excel",
      image: excel,
      desc: "Excel Sheets",
    },
    {
      type: "powerpoint",
      image: powerpoint,
      desc: "Powerpoint",
    },
    {
      type: "pdf",
      image: pdfImage,
      desc: "PDF's",
    },
    {
      type: "unknown",
      image: unknown,
      desc: "Unknown",
    },
    {
      type: "compressed",
      image: compressed,
      desc: "Compressed",
    },
  ];
  const getData = async () => {
    setFlag(false);
    let res = await Axios.post(
      "https://drivetest.globalxchange.io/folder/list-contents",
      {
        email: context.email,
        token: context.token,
      }
    );
    let all_data = [...res.data];
    let last = all_data.pop();
    setDataStats(last);
    setData([...all_data]);
    setFlag(true);
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <div>
      {flag ? (
        <div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              padding: "2vw",
              background: "white",
            }}
          >
            {dataStats.map((item) => {
              let selecteditem = dataTypes.filter(
                (items) => items.type === item.type
              );
              console.log(selecteditem);
              //   let testing = dataTypes.find((items) => items.type === item.type);
              //   console.log(testing);
              return (
                <div
                  style={{
                    border: "0.5px solid #e3e3e3",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    padding: "1vw",
                    borderRadius: "5px",
                    color: "grey",
                    margin: "5px",
                    boxShadow: "0px 1px 5px -3px",
                  }}
                >
                  <img
                    style={{ height: "5vh" }}
                    src={selecteditem[0].image}
                    alt={selecteditem[0].image}
                  />
                  <p>{selecteditem[0].desc}</p>
                  <Divider type="horizontal" width="90%" />
                  <h5>{(item.size / 10 ** 9).toFixed(4)} Gb</h5>
                </div>
              );
            })}
          </div>
          <List
            style={{ paddingTop: "1vh" }}
            itemLayout="horizontal"
            dataSource={data}
            renderItem={(item) => {
              if (item.folder) {
                return;
              } else {
                let colors =
                  item.file_type === "image"
                    ? "#7265e6"
                    : item.file_type === "audio"
                    ? "#f56a00"
                    : item.file_type === "video"
                    ? "#ffbf00"
                    : item.file_type === "doc"
                    ? "#00a2ae"
                    : "#87d068";
                return (
                  <List.Item
                    style={{ padding: "12px 30px", paddingRight: "50px" }}
                  >
                    <List.Item.Meta
                      avatar={
                        <Avatar
                          style={{
                            backgroundColor: colors,
                            color: "#ffffff",
                          }}
                        >
                          {item.file_type.charAt(0).toUpperCase()}
                        </Avatar>
                      }
                      title={item.name}
                      description={item.path}
                    />
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "12vw",
                      }}
                    >
                      <Tag color={colors}>{item.file_type}</Tag>
                      <Divider type="vertical" />

                      <p style={{ margin: "0px" }}>
                        {(item.file_size / 10 ** 6).toFixed(2)} Mb
                      </p>
                    </div>
                  </List.Item>
                );
              }
            }}
          />
        </div>
      ) : (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            marginLeft: "auto",
            marginRight: "auto",
          }}
        >
          <img src={deskStreamlogo} alt="" height="100px" width="100px" />
        </div>
      )}
    </div>
  );
};
export default CastFiles;
