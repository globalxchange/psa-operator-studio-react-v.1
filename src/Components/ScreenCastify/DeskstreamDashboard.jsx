import React, { useContext, useEffect } from "react";
// import MainLayout from "../../Layouts/MainLayout";
import { CounselAppContext } from "../../Context_Api/Context";
// import Tabs11 from "../pages/Home/Tabs12";
// import { Drawer } from "antd";
// import WorkSpaceOne from "../../pages/Home/WorkSpaceOne";
// import WorkSpaceTwo from "../../pages/Home/WorkSpaceTwo";
// import WorkSpaceThree from "../../pages/Home/WorkSpaceThree";
// import Viewusertype from "../../pages/Home/viewusertypes";
// import MeetingsDrawers from '../Dashboard-Components/Dasboard-Meetings-Drawer/MeetingsDrawers';
// import MainMeetingsDrawers from "../Dashboard-Components/Dasboard-Meetings-Drawer/MainMeetingsDrawer";
import "../../pages/Home/pages.scss";
// import ProfileTab from "../pages/Home/profileTab";
import DeskstreamMain from "./DeskstreamMain";
import { streamTriangle } from "../../static/images/images";
import MainLayout from "../../Layouts/MainLayout";

function DeskstreamDashboard() {
  const context = useContext(CounselAppContext);
  const { selectedTab, thebilltab } = useContext(CounselAppContext);
  useEffect(() => {
    context.setbilltab([
      {
        key: 1,
        tabName: "App Config",
        currency: "",
        tab: "bills-full-search-lx",
        imgTab: streamTriangle,
      },
    ]);
  }, []);

  return (
    <>
      <MainLayout active="deskstream">
        {thebilltab.map((x) => {
          return (
            <div
              key={x.key}
              className={x.key === selectedTab.key ? context.view : "d-none"}
            >
              <DeskstreamMain />
            </div>
          );
        })}
      </MainLayout>
    </>
  );
}

export default DeskstreamDashboard;
