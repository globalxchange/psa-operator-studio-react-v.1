import React, { useRef, useState, useEffect, useContext } from "react";
import mic from "./mic.svg";
import "./Deskstream.css";
import { getSeekableBlob } from "recordrtc";
import { CounselAppContext } from "../../Context_Api/Context";
import CastFiles from "./CastFiles";

function DeskstreamMain() {
  const [btndisable, setbtndisable] = useState(true);
  const [init_page, setinit_page] = useState(true);
  const context = useContext(CounselAppContext);
  const [chosen_type, setchosen_type] = useState("");
  const [file_extention_type, setfile_extention_type] = useState("");
  const [mime_type, setmime_type] = useState("");
  const [media_recorder, setmedia_recorder] = useState();

  const screen_stream_ref = useRef(null);
  const user_media_ref = useRef(null);
  const media_recorder_ref = useRef(null);
  let recordedBlobs = useRef([]);

  const file_name = Date.now();
  const save_every = 5500;

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (!init_page) {
      mainFunction();
    }
  }, [init_page, chosen_type]);

  const handleDataAvailable = async (event) => {
    if (event.data && event.data.size > 0) {
      console.log("handleDataAvailable", event);

      recordedBlobs.current.push(event.data);
    }
  };
  const mainFunction = async () => {
    switch (chosen_type) {
      case "screen":
        console.log("inside screen only");
        let screen_stream;

        screen_stream = await navigator.mediaDevices.getDisplayMedia({
          video: true,
          audio: {
            channelCount: 2,
            autoGainControl: false,
            echoCancellation: false,
            latency: 0,
            noiseSuppression: false,
          },
        });

        screen_stream_ref.current.srcObject = screen_stream;
        screen_stream_ref.current = screen_stream;

        recordedBlobs.current = [];

        let mime_options;
        if (MediaRecorder.isTypeSupported("video/webm;codecs=vp9")) {
          mime_options = { mimeType: "video/webm; codecs=vp9" };
        } else if (MediaRecorder.isTypeSupported("video/webm;codecs=vp8")) {
          mime_options = { mimeType: "video/webm; codecs=vp8" };
        }

        media_recorder_ref.current = new MediaRecorder(
          screen_stream_ref.current,
          mime_options
        );

        setmime_type(mime_options.mimeType);

        break;
      case "mic":
        console.log("inside mic");

        let mic_stream = await navigator.mediaDevices.getUserMedia({
          audio: {
            channelCount: 2,
            autoGainControl: false,
            echoCancellation: true,
            latency: 0,
            noiseSuppression: false,
            sampleRate: 48000,
            sampleSize: 16,
            volume: 1.0,
          },
        });

        user_media_ref.current = mic_stream;
        recordedBlobs.current = [];

        media_recorder_ref.current = new MediaRecorder(user_media_ref.current);

        setmime_type("audio/webm");
        break;
      case "screen_mic":
        let scr_stream = await navigator.mediaDevices.getDisplayMedia({
          video: true,
          audio: {
            channelCount: 2,
            autoGainControl: false,
            echoCancellation: false,
            latency: 0,
            noiseSuppression: false,
          },
        });

        let mic_strm = await navigator.mediaDevices.getUserMedia({
          audio: {
            channelCount: 2,
            autoGainControl: false,
            echoCancellation: true,
            latency: 0,
            noiseSuppression: false,
            sampleRate: 48000,
            sampleSize: 16,
          },
        });

        let new_stream = new MediaStream();
        new_stream.addTrack(scr_stream.getVideoTracks()[0]);

        if (!scr_stream.getAudioTracks()[0]) {
          new_stream.addTrack(mic_strm.getAudioTracks()[0]);
        } else {
          const audioContext = new AudioContext();
          const voice = audioContext.createMediaStreamSource(mic_strm);
          const desktop = audioContext.createMediaStreamSource(scr_stream);
          const destination = audioContext.createMediaStreamDestination();

          const desktopGain = audioContext.createGain();
          const voiceGain = audioContext.createGain();

          desktopGain.gain.value = 0.7;
          voiceGain.gain.value = 0.9;

          voice.connect(voiceGain).connect(destination);
          desktop.connect(desktopGain).connect(destination);

          new_stream.addTrack(destination.stream.getAudioTracks()[0]);
        }

        screen_stream_ref.current.srcObject = new_stream;
        screen_stream_ref.current = new_stream;

        recordedBlobs.current = [];
        let mime_opts;
        if (MediaRecorder.isTypeSupported("video/webm;codecs=vp9")) {
          mime_opts = { mimeType: "video/webm; codecs=vp9" };
        } else if (MediaRecorder.isTypeSupported("video/webm;codecs=vp8")) {
          mime_opts = { mimeType: "video/webm; codecs=vp8" };
        }

        media_recorder_ref.current = new MediaRecorder(
          screen_stream_ref.current,
          mime_opts
        );

        setmime_type(mime_opts.mimeType);

        break;
      case "webcam_mic":
        let user_media_strm = await navigator.mediaDevices.getUserMedia({
          video: true,
          audio: {
            channelCount: 2,
            autoGainControl: false,
            echoCancellation: true,
            latency: 0,
            noiseSuppression: false,
            sampleRate: 48000,
            sampleSize: 16,
            volume: 1.0,
          },
        });

        user_media_ref.current.srcObject = user_media_strm;
        user_media_ref.current = user_media_strm;

        recordedBlobs.current = [];

        let mime_opt;
        if (MediaRecorder.isTypeSupported("video/webm;codecs=vp9")) {
          mime_opt = { mimeType: "video/webm; codecs=vp9" };
        } else if (MediaRecorder.isTypeSupported("video/webm;codecs=vp8")) {
          mime_opt = { mimeType: "video/webm; codecs=vp8" };
        }

        media_recorder_ref.current = new MediaRecorder(
          user_media_ref.current,
          mime_opt
        );

        setmime_type(mime_opt.mimeType);

        break;
      case "screen_webcam_mic":
        let scr_strem = await navigator.mediaDevices.getDisplayMedia({
          video: true,
          audio: {
            channelCount: 2,
            autoGainControl: false,
            echoCancellation: false,
            latency: 0,
            noiseSuppression: false,
          },
        });

        let user_med_strm = await navigator.mediaDevices.getUserMedia({
          video: true,
          audio: {
            channelCount: 2,
            autoGainControl: false,
            echoCancellation: true,
            latency: 0,
            noiseSuppression: false,
            sampleRate: 48000,
            sampleSize: 16,
          },
        });

        let new_stream1 = new MediaStream();
        new_stream1.addTrack(scr_strem.getVideoTracks()[0]);

        let new_video_stream = new MediaStream();
        new_video_stream.addTrack(user_med_strm.getVideoTracks()[0]);

        user_media_ref.current.srcObject = new_video_stream;
        user_media_ref.current = new_video_stream;

        if (!scr_strem.getAudioTracks()[0]) {
          new_stream1.addTrack(user_med_strm.getAudioTracks()[0]);
        } else {
          const audioContext = new AudioContext();
          const voice = audioContext.createMediaStreamSource(user_med_strm);
          const desktop = audioContext.createMediaStreamSource(scr_strem);
          const destination = audioContext.createMediaStreamDestination();

          const desktopGain = audioContext.createGain();
          const voiceGain = audioContext.createGain();

          desktopGain.gain.value = 0.7;
          voiceGain.gain.value = 0.9;

          voice.connect(voiceGain).connect(destination);
          desktop.connect(desktopGain).connect(destination);

          new_stream1.addTrack(destination.stream.getAudioTracks()[0]);
        }

        screen_stream_ref.current.srcObject = new_stream1;
        screen_stream_ref.current = new_stream1;

        recordedBlobs.current = [];
        let mime_opts1;
        if (MediaRecorder.isTypeSupported("video/webm;codecs=vp9")) {
          mime_opts1 = { mimeType: "video/webm; codecs=vp9" };
        } else if (MediaRecorder.isTypeSupported("video/webm;codecs=vp8")) {
          mime_opts1 = { mimeType: "video/webm; codecs=vp8" };
        }

        media_recorder_ref.current = new MediaRecorder(
          screen_stream_ref.current,
          mime_opts1
        );

        setmime_type(mime_opts1.mimeType);

        break;
      default:
        break;
    }

    media_recorder_ref.current.addEventListener(
      "dataavailable",
      handleDataAvailable
    );
  };

  function start_recording() {
    console.log("starting_recording");

    media_recorder_ref.current.start(save_every);
  }

  function stop_recording() {
    console.log("stopping recording");
    setbtndisable(false);

    media_recorder_ref.current.stop();

    const downloadButton = document.getElementById("download");
    downloadButton.addEventListener("click", () => {
      const blob = new Blob(recordedBlobs.current, { type: mime_type });
      getSeekableBlob(blob, function (seekableBlob) {
        const url = window.URL.createObjectURL(seekableBlob);
        const a = document.createElement("a");
        a.style.display = "none";
        a.href = url;
        a.download = `${file_name}.webm`;
        document.body.appendChild(a);
        a.click();
        setTimeout(() => {
          document.body.removeChild(a);
          window.URL.revokeObjectURL(url);
        }, 100);
      });
    });
  }

  const windows_view = () => {
    switch (chosen_type) {
      case "screen":
        console.log("screeennnnnnnnn");
        return (
          <div style={{ paddingLeft: "18%", paddingTop: "30px" }}>
            <video
              autoPlay
              id="video"
              muted
              width="1200px"
              height="800px"
              poster="https://dvqlxo2m2q99q.cloudfront.net/000_clients/1208227/page/1208227g6cn3NKs.png"
              ref={screen_stream_ref}
            ></video>
            <div style={{ paddingTop: "20px", paddingLeft: "25%" }}>
              <button
                style={{
                  backgroundColor: "#4CAF50",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={start_recording}
              >
                Start Recording
              </button>
              <button
                style={{
                  backgroundColor: "#f44336",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={stop_recording}
              >
                Stop Recording
              </button>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <button
                id="download"
                disabled={btndisable}
                style={{
                  backgroundColor: btndisable ? "#555555" : "blue",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                }}
              >
                Download
              </button>
            </div>
          </div>
        );
      case "mic":
        return (
          <div style={{ paddingTop: "20px", paddingLeft: "25%" }}>
            <img src={mic} width="400px" height="400px" />
            <br /> <br />
            <button
              style={{
                backgroundColor: "#4CAF50",
                border: "none",
                color: "white",
                padding: "10px",
                textAlign: "center",
                textDecoration: "none",
                display: "inline-block",
                fontSize: "16px",
                margin: "2px",
                cursor: "pointer",
              }}
              onClick={start_recording}
            >
              Start Recording
            </button>
            <button
              style={{
                backgroundColor: "#f44336",
                border: "none",
                color: "white",
                padding: "10px",
                textAlign: "center",
                textDecoration: "none",
                display: "inline-block",
                fontSize: "16px",
                margin: "2px",
                cursor: "pointer",
              }}
              onClick={stop_recording}
            >
              Stop Recording
            </button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button
              id="download"
              disabled={btndisable}
              style={{
                backgroundColor: btndisable ? "#555555" : "blue",
                border: "none",
                color: "white",
                padding: "10px",
                textAlign: "center",
                textDecoration: "none",
                display: "inline-block",
                fontSize: "16px",
                margin: "2px",
              }}
            >
              Download
            </button>
          </div>
        );
        break;
      case "screen_mic":
        return (
          <div style={{ paddingLeft: "18%", paddingTop: "30px" }}>
            <video
              autoPlay
              id="video"
              muted
              width="1200px"
              height="800px"
              poster="https://dvqlxo2m2q99q.cloudfront.net/000_clients/1208227/page/1208227g6cn3NKs.png"
              ref={screen_stream_ref}
            ></video>
            <div style={{ paddingTop: "20px", paddingLeft: "25%" }}>
              <button
                style={{
                  backgroundColor: "#4CAF50",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={start_recording}
              >
                Start Recording
              </button>
              <button
                style={{
                  backgroundColor: "#f44336",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={stop_recording}
              >
                Stop Recording
              </button>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <button
                id="download"
                disabled={btndisable}
                style={{
                  backgroundColor: btndisable ? "#555555" : "blue",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                }}
              >
                Download
              </button>
            </div>
          </div>
        );
        break;
      case "webcam_mic":
        return (
          <div style={{ paddingLeft: "18%", paddingTop: "30px" }}>
            <video
              autoPlay
              id="video"
              muted
              width="1200px"
              height="800px"
              poster="https://dvqlxo2m2q99q.cloudfront.net/000_clients/1208227/page/1208227g6cn3NKs.png"
              ref={user_media_ref}
            ></video>
            <div style={{ paddingTop: "20px", paddingLeft: "25%" }}>
              <button
                style={{
                  backgroundColor: "#4CAF50",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={start_recording}
              >
                Start Recording
              </button>
              <button
                style={{
                  backgroundColor: "#f44336",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={stop_recording}
              >
                Stop Recording
              </button>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <button
                id="download"
                disabled={btndisable}
                style={{
                  backgroundColor: btndisable ? "#555555" : "blue",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                }}
              >
                Download
              </button>
            </div>
          </div>
        );
        break;
      case "screen_webcam_mic":
        return (
          <div style={{ paddingLeft: "8%", paddingTop: "30px" }}>
            <video
              autoPlay
              id="video"
              muted
              width="1200px"
              height="800px"
              poster="https://dvqlxo2m2q99q.cloudfront.net/000_clients/1208227/page/1208227g6cn3NKs.png"
              ref={screen_stream_ref}
            ></video>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <video
              autoPlay
              id="video"
              muted
              width="500px"
              height="500px"
              poster="https://usedsuperlawntrucks.com/wp-content/uploads/2018/06/camera-coming-soon.jpg"
              ref={user_media_ref}
            ></video>
            <div style={{ paddingTop: "20px", paddingLeft: "25%" }}>
              <button
                style={{
                  backgroundColor: "#4CAF50",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={start_recording}
              >
                Start Recording
              </button>
              <button
                style={{
                  backgroundColor: "#f44336",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                  cursor: "pointer",
                }}
                onClick={stop_recording}
              >
                Stop Recording
              </button>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <button
                id="download"
                disabled={btndisable}
                style={{
                  backgroundColor: btndisable ? "#555555" : "blue",
                  border: "none",
                  color: "white",
                  padding: "10px",
                  textAlign: "center",
                  textDecoration: "none",
                  display: "inline-block",
                  fontSize: "16px",
                  margin: "2px",
                }}
              >
                Download
              </button>
            </div>
          </div>
        );
        break;
      default:
        break;
    }
  };

  const deskFunction = () => {
    console.log("in deskFunction" + "-----" + context.deskType);
    switch (context.deskType) {
      case "record":
        return (
          <>
            {init_page ? (
              <div style={{ background: "#404040" }}>
                <div className="middle">
                  <h1 style={{ color: "white", fontFamily: "montserrat" }}>
                    Choose Recording Type
                  </h1>
                  <a
                    className="btn btn1"
                    onClick={() => {
                      setchosen_type("screen");
                      setinit_page(false);
                    }}
                  >
                    Screen
                  </a>
                  <a
                    className="btn btn1"
                    onClick={() => {
                      setchosen_type("mic");
                      setinit_page(false);
                    }}
                  >
                    Microphone
                  </a>
                  <hr />
                  <a
                    className="btn btn3"
                    onClick={() => {
                      setchosen_type("screen_mic");
                      setinit_page(false);
                    }}
                  >
                    Screen + Mic
                  </a>
                  <a
                    className="btn btn3"
                    onClick={() => {
                      setchosen_type("webcam_mic");
                      setinit_page(false);
                    }}
                  >
                    Webcam + Mic
                  </a>
                  <a
                    className="btn btn3"
                    onClick={() => {
                      setchosen_type("screen_webcam_mic");
                      setinit_page(false);
                    }}
                  >
                    Screen + Webcam + Mic
                  </a>
                </div>
              </div>
            ) : (
              <div> {windows_view()}</div>
            )}
          </>
        );
      case "fileInfo":
        return <CastFiles />;
      default:
        break;
    }
  };

  return (
    <div
      id="desk-main"
      style={{
        height: "calc(100%-80px)",
        width: " 100%",
      }}
    >
      {deskFunction()}
    </div>
  );
}

export default DeskstreamMain;
