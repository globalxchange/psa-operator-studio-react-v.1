import React, { useState } from "react";
import "./stream-option.style.scss";
import {
  loadingGIF,
  micOn,
  MicOff,
  VideoOff,
  VideoOn,
  CopyLoad,
  ScreenShare,
  LogOut,
  dots,
  stop,
  recordIcon,
} from "../../../../static/images/images";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { CounselAppContext } from "../../../../Context_Api/Context";
import { message } from "antd";

const mediaType = {
  audio: "audioType",
  video: "videoType",
  screen: "screenType",
};

const VideoOptionComponent = ({
  setCameraOn,
  options,
  cameraOn,
  camera,
  setCamera,
  roomReference,
  reference,
  mic,
  setMic,
  first,
  setFirst,
  setOptions,
  finalVideoType,
  finalAudioType,
  record,
  optionRecord,
  setOptionRecord,
}) => {
  const context = React.useContext(CounselAppContext);
  // console.log(context.host);
  const [copied, setCopied] = useState(false);
  const [firsts, setFirsts] = useState(first);
  const [optionVideo, setOptionvideo] = useState(true);
  const [optionMic, setOptionMic] = useState(true);
  const [optionChat, setOptionChat] = useState(true);
  const [minimize, setMinimize] = useState(false);

  const handleClick = async (val) => {
    switch (val) {
      case "dots":
        setMinimize(false);
        break;
      case "record":
        if (optionRecord) {
          record("stop_recording");
        } else {
          record("start_recording");
          setOptionRecord(true);
        }

        break;
      case "screenShare":
        if (first === false) {
          console.log("in screen produce");
          try {
            await roomReference.produce(
              "screenType",
              reference,
              roomReference.device,
              "screen",
              finalVideoType,
              finalAudioType
            );

            setFirst(true);
            setCamera(true);
            message.success("Started Screen Share");
            console.log("setting cbuttons");
          } catch (e) {
            console.log("setting cbuttons in catch");
            setCamera(false);
          }
        } else {
          if (camera) {
            message.success("Screen Resumed");
            roomReference.resumeProducer(mediaType.screen);
            setCamera(!camera);
          } else {
            message.success("Screen Paused");
            roomReference.pauseProducer(mediaType.screen);
            setCamera(!camera);
          }
        }
        break;
      case "video":
        console.log(roomReference);
        if (cameraOn === true) {
          await roomReference.pauseProducer(mediaType.video);
          message.success("Video Paused");
          setOptionvideo(false);
        } else {
          await roomReference.resumeProducer(mediaType.video);
          message.success("Video Resumed");
          setOptionvideo(true);
        }
        setCameraOn(!cameraOn);
        break;
      case "mic":
        if (mic === true) {
          await roomReference.pauseProducer(mediaType.audio);
          message.success("Microphone Paused");
          setOptionMic(false);
        } else {
          await roomReference.resumeProducer(mediaType.audio);
          message.success("Microphone Resumed");
          setOptionMic(true);
        }
        setMic(!mic);
        break;
      case "chat":
        if (context.chatOpen) {
          context.setChatOpen(false);
          setOptionChat(false);
        } else {
          context.setChatOpen(true);
          setOptionChat(true);
        }
        break;
      case "people":
        if (options) {
          setOptions(false);
        } else {
          setOptions(true);
        }
        break;
      case "exit":
        setCamera(1);
        roomReference.exit(true);
        window.close();
        break;
      default:
        return;
    }
  };
  const optionsButton = [
    {
      color: "white",
      image: micOn,
      type: "mic",
      show: " Mic ",
      opposite: MicOff,
      option: optionMic,
      streamtype: [
        "Streaming_CC",
        "Streaming_SS",
        "Streaming_HH",
        "Streaming_SC",
      ],
    },
    {
      color: "white",
      image: VideoOn,
      type: "video",
      show: " Camera ",
      opposite: VideoOff,
      option: optionVideo,
      streamtype: ["Streaming_CC", "Streaming_HH", "Streaming_SC"],
    },
    {
      color: "white",
      image: ScreenShare,
      type: "screenShare",
      show: " Share Screen ",
      opposite: false,
      streamtype: ["Streaming_SS", "Streaming_HH", "Streaming_SC"],
    },
  ];

  return (
    <div className={"stream-video-options"}>
      <div className="stream-video-options-container">
        {minimize ? (
          <div className="stream-video-options-center">
            {context.host ||
            context.coHosts === "co-host" ||
            context.coHosts === "host" ? (
              <button
                onClick={() => handleClick("record")}
                style={{ backgroundColor: `white` }}
              >
                <img src={!optionRecord ? recordIcon : stop} alt="" />
                <span>{"Record"}</span>
              </button>
            ) : (
              ""
            )}

            {optionsButton.map((item) => {
              if (context.coHosts === "host" || context.coHosts === "co-host") {
                let result = item.streamtype.includes(
                  context.presentMeeting.stream_type
                );
                if (result) {
                  return (
                    <button
                      onClick={() => handleClick(item.type)}
                      style={{ backgroundColor: `${item.color}` }}
                    >
                      <img
                        src={
                          !item.opposite
                            ? item.image
                            : item.option
                            ? item.image
                            : item.opposite
                        }
                        alt=""
                      />
                      <span>{item.show}</span>
                    </button>
                  );
                }
              }
            })}

            <CopyToClipboard text={context.meetingLink}>
              <button
                onClick={(e) => {
                  handleClick("copy");
                  setCopied(true);
                  setTimeout(() => {
                    setCopied(false);
                  }, 1000);
                }}
                style={{ backgroundColor: "white" }}
              >
                {copied ? (
                  <img src={loadingGIF} alt="" />
                ) : (
                  <img src={CopyLoad} alt="" />
                )}
                <span>{"Copy Meeting Link"}</span>
              </button>
            </CopyToClipboard>

            <button
              onClick={() => handleClick("exit")}
              style={{ backgroundColor: "white" }}
            >
              <img src={LogOut} alt="" />
              <span>{"Exit"}</span>
            </button>
            <button
              onClick={() => handleClick("dots")}
              style={{ backgroundColor: "white", marginRight: "0px" }}
            >
              <img src={dots} alt="" />
              <span>{"Minimize"}</span>
            </button>
          </div>
        ) : (
          <div className="stream-video-options-center">
            <button
              className="stream-button"
              onClick={() => setMinimize(true)}
              style={{ backgroundColor: "white", marginRight: "0px" }}
            >
              <img src={dots} alt="" />
              <span>{minimize ? "Minimize" : "Maximize"}</span>
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default VideoOptionComponent;
