import React, {useState, useContext, useEffect} from "react";
import {
  loginLoader,
  people,
  userposter,
  screenposter,
  Vector,
} from "../../../static/images/images";

import {Upload, message} from "antd";
import {CounselAppContext} from "../../../Context_Api/Context";
import Streaming from "./Mediasoup_Streaming_File";
import "./streamview.scss";
// import StreamModalSS from "../Stream_Modals/Stream_Modal_Single";
import StreamModalSingle from "../Stream_Modals/Stream_Modal_Single";
import Comments from "./Comments/Comments";
import Timing from "./Timing";
let socket;
const StreamView = () => {
  const {
    userReference,
    host, 
    coHosts,
    setVideoTypes,
    screenReference,
    setAudioTypes,
    presentMeeting,
  } = useContext(CounselAppContext);
  const [visible, setVisible] = useState(true);
  const [fileList, updateFileList] = useState([]);
  const [flag, setFlag] = useState(true);
  const [ModalClick, setModalClick] = useState(false);
  const [innerFlag, setInnerFlag] = useState(false);
  const [spotEmail, setSPotEMail] = useState("");
  const [reloadFlag, setReloadFlag] = useState(false);
  const props = {
    fileList,
    beforeUpload: (file) => {
      if (file.type !== "image/png") {
        message.error(`${file.name} is not a png file`);
      }
      return file.type === "image/png";
    },
    onChange: (info) => {
      console.log(info.fileList);
      // file.status is empty when beforeUpload return false
      updateFileList(info.fileList.filter((file) => !!file.status));
    },
  };

  //-----------------------------------------useeffects--------------------------------//

  useEffect(() => {
    console.log("get details");
    getTrackDetails();
  }, []);

  useEffect(() => {
    console.log("modal click");
    if (ModalClick === true) {
      setInnerFlag(true);
    }
  }, [ModalClick]);

  //------------------------------Functions-----------------------------------------//

  const getData = () => {
    if (coHosts !== "viewer") {
      return (
        <StreamModalSingle
          visible={visible}
          setVisible={setVisible}
          ModalClick={ModalClick}
          setModalClick={setModalClick}
          type={presentMeeting.stream_type}
        />
      );
    }
  };

  const getTrackDetails = async () => {
    console.log("in track details" + coHosts);
    setFlag(true);
    if (coHosts === "co-host" || host === true) {
      const mediaConstraintsAudio = {
        audio: {
          echoCancellation: true,
          latency: 0,
          noiseSuppression: true,
          sampleRate: 48000,
          sampleSize: 16,
          volume: 1.0,
        },
      };
      const mediaConstraintsVideo = {
        video: {
          width: {
            min: 640,
            ideal: 1920,
            max: 3840,
          },
          height: {
            min: 400,
            ideal: 1080,
            max: 2160,
          },
          frameRate: {
            min: 15,
            max: 120,
          },
        },
      };
      let detailsAudio = await navigator.mediaDevices.getUserMedia(
        mediaConstraintsAudio
      );
      let detailsVideo = await navigator.mediaDevices.getUserMedia(
        mediaConstraintsVideo
      );

      let audioTracks = detailsAudio.getAudioTracks();
      console.log(audioTracks);
      let resAudio = audioTracks.map((item, i) => {
        return {key: i, name: item.label};
      });
      console.log("-------audio----devices-----");
      console.log(resAudio);
      setAudioTypes(resAudio);
      let videoTracks = detailsVideo.getVideoTracks();
      console.log(videoTracks);

      let resVideo = videoTracks.map((item, i) => {
        return {key: i, name: item.label};
      });
      console.log("-------video----devices-----");
      console.log(resVideo);
      setVideoTypes(resVideo);
      setVisible(true);
      setFlag(false);
    } else if (coHosts === "viewer" || host === false) {
      setFlag(false);
      setInnerFlag(true);
      return;
    }
  };

  //--------------------------------------------------------------------------------//

  return (
    <div className="main">
      {flag ? (
        reloadFlag ? (
          <div
            style={{
              height: "100vh",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img
              src={loginLoader}
              alt="Loader"
              style={{alignSelf: "center", height: "10vh"}}
            />
            <br></br>
            Host Didn't start the stream yet!! Please wait ot Reload after some
            time
          </div>
        ) : (
          <div
            style={{
              height: "100vh",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img
              src={loginLoader}
              alt="Loader"
              style={{alignSelf: "center", height: "10vh"}}
            />
          </div>
        )
      ) : (
        <>
          <div className="div-main-one">
            <div className="div-one">
              <video
                width={"100%"}
                height={"100%"}
                className="bg-image-profile"
                autoPlay
                id="video"
                ref={userReference}
                poster={
                  presentMeeting.stream_type === "Streaming_SS"
                    ? screenposter
                    : userposter
                }
              />
              {innerFlag ? (
                <Streaming
                  socket={socket}
                  flag={flag}
                  reloadFlag={reloadFlag}
                  setFlag={setFlag}
                  setReloadFlag={setReloadFlag}
                />
              ) : (
                ""
              )}
            </div>
            <div className="div-two">
              <video
                width={"100%"}
                height={"100%"}
                className="bg-image-profile"
                autoPlay
                id="video"
                ref={screenReference}
                poster={
                  presentMeeting.stream_type === "Streaming_SC" ||
                  presentMeeting.stream_type === "Streaming_SS"
                    ? screenposter
                    : userposter
                }
              />
            </div>
          </div>
          <div className="div-main-two">
            <div className="div-three">
              <div className="div-three-one">
                <div className="div-three-one-one">
                  <img src={people} alt="" />
                </div>
                <div className="div-three-one-two">
                  <h3>
                    Speaking -{" "}
                    {presentMeeting.hostnames.map((item) => {
                      return `${item.name} `;
                    })}
                  </h3>
                  <p>
                    In This Episode We Are Thrilled To Award Larry Bird With 1
                    Billion Dollars In Bitcoin
                  </p>
                </div>
              </div>
              <Timing />
              <div className="div-three-three">
                <h4>Exclusive Offer</h4>
                <h3>$2,145.23</h3>
              </div>
              <div className="div-three-four">
                <h3>Enter Email To Secure Your Spot Today</h3>
                <div className="div-three-four-inner">
                  <input
                    value={spotEmail}
                    placeholder="ex. john@gmail.com"
                    onChange={(e) => {
                      setSPotEMail(e.target.value);
                    }}
                  ></input>
                </div>
              </div>
            </div>
            <div className="div-four">
              <div className="div-four-top">
                <div className="div-four-top-one">
                  <Upload {...props} listType="picture-card">
                    <img src={Vector} alt=""></img> <br></br>
                    <p>Attachments</p>
                  </Upload>
                </div>
                <div className="div-four-top-two"></div>
              </div>
              <Comments />{" "}
              <div className="div-four-three">
                <p>See All Comments</p>
              </div>
            </div>
          </div>
          {flag === false ? getData() : ""}
        </>
      )}
    </div>
  );
};
export default StreamView;
