import React, { useEffect } from 'react';
import './video-people.style.scss';

const VideoPeopleComponent = ({
  people,
  setOptions,
  options,
  bodyConnections,
}) => {
  useEffect(() => {
    console.log('video ', people);
  }, []);
  return (
    <div
      className={`terminal-video-people ${
        options ? 'show-options' : 'hide-options'
      }`}
    >
      <div
        className='terminal-video-people-body'
        id='terminal-body-others'
        ref={bodyConnections}
      ></div>
    </div>
  );
};
export default VideoPeopleComponent;
