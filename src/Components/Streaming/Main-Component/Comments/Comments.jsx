import React, {useEffect, useState, useContext} from "react";
import ioClient from "socket.io-client";
import {sign} from "jsonwebtoken";
import {CounselAppContext} from "../../../../Context_Api/Context";
import {loginLoader, rocket} from "../../../../static/images/images";
import {Avatar, Popover} from "antd";
import {SmileOutlined, DeleteOutlined} from "@ant-design/icons";

import {Picker} from "emoji-mart";
let commentSocket;
const Comments = () => {
  const {
    coHosts,
    profileData,
    presentMeeting,
    app_id,
    Name,
    email,
    token,
  } = useContext(CounselAppContext);
  const [loading, setLoading] = useState(true);
  const [comments, setComments] = useState([]);
  // const [comment, setComment] = useState("");

  const [emojiShow, setemojiShow] = useState(false);
  const bodyRef = React.createRef(null);
  //-------------------------------------------------------------useeffects------------------------------------------------------------------------------------
  useEffect(() => {
    setLoading(true);
    console.log("-----in socket connection------");
    if (coHosts !== "viewer") {
      commentSocket = ioClient("https://testsockchatsio.globalxchange.io", {
        reconnection: false,
        query: {
          email:
            localStorage.getItem("CounselAppLoginAccount") === ""
              ? email
              : localStorage.getItem("CounselAppLoginAccount"),
          token:
            localStorage.getItem("CounselAppToken") === ""
              ? token
              : localStorage.getItem("CounselAppToken"),
          tokfortest: "nvestisgx",
        },
      });
    } else {
      console.log(localStorage.getItem("CounselAppLoginAccount"));
      const xtoken = sign(
        {
          email:
            localStorage.getItem("CounselAppLoginAccount") === ""
              ? email
              : localStorage.getItem("CounselAppLoginAccount"),
        },

        "Z&Pblj&bcvsxP()9@Io%klHZpTyaTExTDoh3rc)e%)KuXEOoiS8uIYirCH9(dCvu",
        {
          algorithm: "HS512",
          expiresIn: 240,
          issuer: "streams.io",
        }
      );
      localStorage.setItem("streamCustomToken", xtoken);
      commentSocket = ioClient("https://testsockchatsio.globalxchange.io", {
        reconnection: false,
        query: {
          email: localStorage.getItem("CounselAppLoginAccount"),
          xtoken: xtoken,
          tokfortest: "nvestisgx",
        },
      });
    }
    getHistory();
    return () => {
      commentSocket.off();
    };
  }, []);
  useEffect(() => {
    commentSocket.on("comment_notify", (res) => {
      if (res.thread_id === presentMeeting.meetingId) {
        getHistory();
      }
    });
  }, []);
  //------------------------------------------------------------------------------------------------------------------------------------------------------------

  //------------------------------------------------------------functions---------------------------------------------------------------------------------------
  const getHistory = () => {
    let thread_id = presentMeeting.meetingId;
    console.log(
      "-- fetching comment history of -" + thread_id + "app_id" + app_id
    );
    console.log(commentSocket);
    commentSocket.emit(
      "get_comments_history",
      `${presentMeeting.meetingId}`,
      "559af22d-5e79-4948-acf6-e48733ee1daa",
      (response) => {
        // setComments(response);
        console.log(response);
        setComments([...response]);
        setLoading(false);
        console.log(comments);
      }
    );
  };
  const sendMessage = () => {
    if (bodyRef.current.innerHTML.length > 0) {
      let data = {
        comment: bodyRef.current.innerHTML,
        thread_id: presentMeeting.meetingId,
        timestamp: Date.now(),
        sender: {
          username: coHosts === "viewer" ? Name : profileData.firstName,
          email:
            localStorage.getItem("CounselAppLoginAccount") === ""
              ? email
              : localStorage.getItem("CounselAppLoginAccount"),
        },
      };
      commentSocket.emit("add_comment", data, app_id, (response) => {
        bodyRef.current.innerHTML = "";
        getHistory();
      });
    }
  };
  const getMessages = () => {
    let returnarray = comments.map((item, i) => {
      const ColorList = ["#f56a00", "#7265e6", "#ffbf00", "#00a2ae"];
      let color = Math.floor(Math.random() * 4) - 1;
      if (item.comment) {
        return (
          <div className="div-four-two-inner" key={item._id}>
            <Avatar
              style={{
                marginLeft: "15px",
                backgroundColor: ColorList[color],
                verticalAlign: "middle",
              }}
              size="medium"
            >
              {item.sender.username[0].toUpperCase()}
            </Avatar>
            <p style={{width: "67%", paddingLeft: "15px"}}>{item.comment}</p>
            <p style={{width: "18%", textAlign: "right"}}>
              -{item.sender.username}
            </p>
            {item.sender.email ===
            localStorage.getItem("CounselAppLoginAccount") ? (
              <DeleteOutlined
                onClick={(e) => {
                  deleteComment(item);
                }}
              />
            ) : (
              ""
            )}
          </div>
        );
      }
    });
    return returnarray;
  };
  const deleteComment = (item) => {
    console.log(item._id + "---");
    const data = {
      _id: item._id,
      thread_id: presentMeeting.meetingId,
    };
    commentSocket.emit("delete_comment", data, app_id, (response) => {
      console.log("delete succesfull");
      getHistory();
    });
  };
  const addEmoji = (e) => {
    //console.log(e.native)
    let emoji = e.native;
    bodyRef.current.innerHTML = bodyRef.current.innerHTML + " " + emoji;
    setemojiShow(false);
    // setInputText(inputText + " " + emoji);
  };

  //------------------------------------------------------------------------------------------------------------------------------------------------------------

  return (
    <div style={{height: "62%", borderTop: "1px solid rgba(0, 42, 81, 0.25)"}}>
      {loading ? (
        <div
          style={{
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={loginLoader}
            alt="Loader"
            style={{alignSelf: "center", height: "15vh"}}
          />
        </div>
      ) : (
        <div style={{height: "100%"}}>
          <div className="div-four-one">
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "7px",
                color: "#8D8D8D",
                justifyContent: "center",
                width: "8%",
              }}
            >
              <Popover
                placement="bottomRight"
                content={<Picker onSelect={addEmoji} skin={2} title="Nvest" />}
                trigger="click"
              >
                <SmileOutlined
                  type="smile"
                  style={{
                    fontSize: "18px",
                  }}
                  onClick={(e) => setemojiShow(!emojiShow)}
                />
              </Popover>
            </div>

            <div className="chat-input-div">
              <div
                ref={bodyRef}
                className={"chat-input"}
                contentEditable="true"
                placeholder="Write a Comment..."
              ></div>
            </div>

            {/* <input
              value={comment}
              placeholder="Write Comment..."
              onChange={(e) => {
                setComment(e.target.value);
              }}
            ></input> */}
            <div className="image-div">
              <img
                style={{cursor: "pointer"}}
                src={rocket}
                alt=""
                onClick={(e) => {
                  if (bodyRef.current.innerHTML.length > 0) {
                    sendMessage();
                  }
                }}
              ></img>
            </div>
          </div>

          <div className="div-four-two">{getMessages()}</div>
        </div>
      )}
    </div>
  );
};
export default Comments;
