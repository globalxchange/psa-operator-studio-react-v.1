import React, {useState, useContext} from "react";
import urlParser from "js-video-url-parser";
import {Picker} from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";
import {Popover, message, Modal} from "antd";
import S3 from "aws-s3";
import {CounselAppContext} from "../../../../../Context_Api/Context";
import {useEffect} from "react";
import {send} from "../../../../../static/images/images";
import {CloseOutlined, SmileOutlined} from "@ant-design/icons";
import "../../../../../css/myStyle.scss";
const StreamChatEditor = () => {
  const {
    socket,
    groupReadUnreadList,
    setgroupReadUnreadList,
    selectedFriend,
    selectedGroup,
    selectedReplyMessage,
    setSelectedReplyMessage,
    selectedSupport,
    messagearray,
    setmessagearray,
    bodyRef,
    activeTab,
    userObj,
    app_id,
  } = useContext(CounselAppContext);

  const [clipImage, setClipImage] = useState("");
  const [visiblePasteImage, setVisiblePasteImage] = useState(false);
  const [emojiShow, setemojiShow] = useState(false);
  // const [selectedReplyMessage, setSelectedReplyMessage] = useState({});

  const config = {
    bucketName: "chatsgx",
    dirName: localStorage.getItem("user_account"),
    region: "us-east-2",
    accessKeyId: "AKIAQD4QYXWFTC6JX6YM",
    secretAccessKey: "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",
  };

  useEffect(() => {
    bodyRef.current.innerHTML = "";
  }, [activeTab]);

  const handleSubmitGroupChat = (e) => {
    // e.preventDefault();
    console.log("inside submit group chat", selectedGroup);
    let obj = urlParser.parse(bodyRef.current.innerHTML);
    // console.log("url parsed", obj);
    if (obj !== undefined) {
      if (obj.mediaType === "video") {
        if (bodyRef.current.innerHTML.length > 0) {
          let tempArr = [...groupReadUnreadList];
          const objIndex = tempArr.findIndex(
            (obj) => obj.group_name === selectedGroup.group_name
          );
          console.log(objIndex);
          tempArr[objIndex].last_messaged_user = userObj.username;
          console.log(tempArr);
          setgroupReadUnreadList(tempArr);

          let notification_data = {
            sender: userObj.username,
            receiver: selectedGroup.group_name,
            group: true,
            group_type: "group",
          };

          socket.emit("new_message", notification_data);

          socket.emit(
            "new_group_message",
            {
              message: JSON.stringify(bodyRef.current.innerHTML),
              type: "video",
              thread_id: selectedGroup.thread_id,
              sender: userObj,
              timestamp: Date.now(),
            },
            app_id,
            (response) => {
              if (response == "success") {
                console.log(response);
                setmessagearray([
                  ...messagearray,
                  {
                    message: JSON.stringify(bodyRef.current.innerHTML),
                    type: "video",
                    threadId: selectedGroup.thread_id,
                    sender: userObj,
                    timestamp: Date.now(),
                  },
                ]);
                // setInputText("");
                bodyRef.current.innerHTML = "";
                let tempArr1 = [...groupReadUnreadList];

                const objIndex = tempArr1.findIndex(
                  (obj) => obj.group_name === selectedGroup.group_name
                );
                tempArr1[objIndex].sender = userObj.username;

                let newArr = [
                  ...tempArr1.filter(
                    (o) => o.group_name === selectedGroup.group_name
                  ),
                ];
                tempArr1 = tempArr1.filter(
                  (obj) => obj.group_name !== selectedGroup.group_name
                );
                newArr = [...newArr, ...tempArr1];
                console.log(newArr);
                setgroupReadUnreadList([...newArr]);
              } else {
                console.log(response);
              }
            }
          );
        } else {
          message.destroy();
          message.info("Please type something!", 2);
        }
      }
    } else {
      if (bodyRef.current.innerHTML.length > 0) {
        let tempArr = [...groupReadUnreadList];
        console.log(tempArr);
        const objIndex = tempArr.findIndex(
          (obj) => obj.group_name === selectedGroup.group_name
        );
        console.log(objIndex);
        tempArr[objIndex].last_messaged_user = userObj.username;
        console.log(tempArr);
        setgroupReadUnreadList(tempArr);

        let notification_data = {
          sender: userObj.username,
          receiver: selectedGroup.group_name,
          group: true,
          group_type: "group",
        };

        socket.emit("new_message", notification_data);

        socket.emit(
          "new_group_message",
          {
            message: JSON.stringify(bodyRef.current.innerHTML),
            thread_id: selectedGroup.thread_id,
            sender: userObj,
            timestamp: Date.now(),
          },
          app_id,
          (response) => {
            if (response == "success") {
              console.log(response);
              setmessagearray([
                ...messagearray,
                {
                  message: JSON.stringify(bodyRef.current.innerHTML),
                  threadId: selectedGroup.thread_id,
                  sender: userObj,
                  timestamp: Date.now(),
                },
              ]);
              // setInputText("");
              bodyRef.current.innerHTML = "";

              let tempArr1 = [...groupReadUnreadList];

              const objIndex = tempArr1.findIndex(
                (obj) => obj.group_name === selectedGroup.group_name
              );
              tempArr1[objIndex].sender = userObj.username;

              let newArr = [
                ...tempArr1.filter(
                  (o) => o.group_name === selectedGroup.group_name
                ),
              ];
              tempArr1 = tempArr1.filter(
                (obj) => obj.group_name !== selectedGroup.group_name
              );
              newArr = [...newArr, ...tempArr1];
              console.log(newArr);
              setgroupReadUnreadList([...newArr]);
            } else {
              console.log(response);
            }
          }
        );
      } else {
        message.destroy();
        message.info("Please type something!", 2);
      }
    }
  };

  const handleSubmitReplyGroupChat = (e) => {
    let obj = urlParser.parse(bodyRef.current.innerHTML);
    console.log(obj);
    if (obj !== undefined) {
      if (obj.mediaType === "video") {
        if (bodyRef.current.innerHTML.length > 0) {
          let tempArr = [...groupReadUnreadList];
          const objIndex = tempArr.findIndex(
            (obj) => obj.group_name === selectedGroup.group_name
          );
          tempArr[objIndex].last_messaged_user = userObj.username;
          console.log(tempArr);
          setgroupReadUnreadList([...tempArr]);

          let notification_data = {
            sender: userObj.username,
            receiver: selectedGroup.group_name,
            group: true,
            group_type: "group",
          };

          socket.emit("new_message", notification_data);

          socket.emit(
            "new_group_reply_message",
            {
              message: JSON.stringify(bodyRef.current.innerHTML),
              type: "video",
              thread_id: selectedGroup.thread_id,
              sender: userObj,
              timestamp: Date.now(),
            },
            selectedReplyMessage,
            app_id,
            (response) => {
              if (response == "success") {
                console.log(response);
                setmessagearray([
                  ...messagearray,
                  {
                    message: JSON.stringify(bodyRef.current.innerHTML),
                    type: "video",
                    replied_msg: true,
                    replied_to_data: {
                      message: selectedReplyMessage.message,
                      threadId: selectedReplyMessage.thread_id,
                      sender: selectedReplyMessage.sender,
                      timestamp: selectedReplyMessage.timestamp,
                      type: selectedReplyMessage.type
                        ? selectedReplyMessage.type
                        : null,
                      location: selectedReplyMessage.location
                        ? selectedReplyMessage.location
                        : null,
                    },
                    threadId: selectedGroup.thread_id,
                    sender: userObj,
                    timestamp: Date.now(),
                  },
                ]);
                // setInputText("");
                bodyRef.current.innerHTML = "";

                let tempArr1 = [...groupReadUnreadList];

                const objIndex = tempArr1.findIndex(
                  (obj) => obj.group_name === selectedGroup.group_name
                );
                tempArr1[objIndex].sender = userObj.username;

                let newArr = [
                  ...tempArr1.filter(
                    (o) => o.group_name === selectedGroup.group_name
                  ),
                ];
                tempArr1 = tempArr1.filter(
                  (obj) => obj.group_name !== selectedGroup.group_name
                );
                newArr = [...newArr, ...tempArr1];
                console.log(newArr);
                setgroupReadUnreadList([...newArr]);
              } else {
                console.log(response);
              }
            }
          );
        } else {
          message.destroy();
          message.info("Please type something!", 2);
        }
      }
    } else {
      if (bodyRef.current.innerHTML.length > 0) {
        let tempArr = [...groupReadUnreadList];
        const objIndex = tempArr.findIndex(
          (obj) => obj.group_name === selectedGroup.group_name
        );
        tempArr[objIndex].last_messaged_user = userObj.username;
        console.log(tempArr);
        setgroupReadUnreadList(tempArr);

        let notification_data = {
          sender: userObj.username,
          receiver: selectedGroup.group_name,
          group: true,
          group_type: "group",
        };

        socket.emit("new_message", notification_data);

        socket.emit(
          "new_group_reply_message",
          {
            message: JSON.stringify(bodyRef.current.innerHTML),
            thread_id: selectedGroup.thread_id,
            sender: userObj,
            timestamp: Date.now(),
          },
          selectedReplyMessage,
          app_id,
          (response) => {
            if (response == "success") {
              console.log(response);
              setmessagearray([
                ...messagearray,
                {
                  message: JSON.stringify(bodyRef.current.innerHTML),

                  replied_msg: true,
                  replied_to_data: {
                    message: selectedReplyMessage.message,
                    threadId: selectedReplyMessage.thread_id,
                    sender: selectedReplyMessage.sender,
                    timestamp: selectedReplyMessage.timestamp,
                    type: selectedReplyMessage.type
                      ? selectedReplyMessage.type
                      : null,
                    location: selectedReplyMessage.location
                      ? selectedReplyMessage.location
                      : null,
                  },
                  threadId: selectedGroup.thread_id,
                  sender: userObj,
                  timestamp: Date.now(),
                },
              ]);
              // setInputText("");
              bodyRef.current.innerHTML = "";

              let tempArr1 = [...groupReadUnreadList];

              const objIndex = tempArr1.findIndex(
                (obj) => obj.group_name === selectedGroup.group_name
              );
              tempArr1[objIndex].sender = userObj.username;

              let newArr = [
                ...tempArr1.filter(
                  (o) => o.group_name === selectedGroup.group_name
                ),
              ];
              tempArr1 = tempArr1.filter(
                (obj) => obj.group_name !== selectedGroup.group_name
              );
              newArr = [...newArr, ...tempArr1];
              console.log(newArr);
              setgroupReadUnreadList([...newArr]);
            } else {
              console.log(response);
            }
          }
        );
      } else {
        message.destroy();
        message.info("Please type something!", 2);
      }
    }
    setSelectedReplyMessage(null);
  };
  const conditionalSubmit = () => {
    console.log("allselected", selectedFriend, selectedGroup, selectedSupport);

    if (selectedGroup !== null) {
      if (selectedReplyMessage !== null) {
        handleSubmitReplyGroupChat();
      } else {
        handleSubmitGroupChat();
      }
    }
  };

  const pasteImageHandler = (event) => {
    // use event.originalEvent.clipboard for newer chrome versions
    var items = (event.clipboardData || event.originalEvent.clipboardData)
      .items;
    console.log(items);
    console.log(JSON.stringify(items)); // will give you the mime types
    // find pasted image among pasted items
    var blob = null;

    for (var i = 0; i < items.length; i++) {
      console.log("url obj", items[i]);
      if (items[i].type.indexOf("image") === 0) {
        blob = items[i].getAsFile();
      }
    }
    // load image if there is a pasted image
    if (blob !== null) {
      var reader = new FileReader();
      reader.onload = function (event) {
        // console.log(event.target.result); // data url!
        setClipImage(event.target.result);
        // document.getElementById("pastedImage").src = event.target.result;
      };
      reader.readAsDataURL(blob);
      setVisiblePasteImage(true);
    }
  };

  const uploadGroupImage = async () => {
    // var file = new File([clipImage], Date.now().toString().png);
    const base64Data = new Buffer.from(
      clipImage.replace(/^data:image\/\w+;base64,/, ""),
      "base64"
    );
    const file = new File([base64Data], "noname", {type: "image/png"});
    console.log("myfile", base64Data);

    const S3Client = new S3(config);
    let uploaded_data;
    try {
      uploaded_data = await S3Client.uploadFile(file, Date.now().toString());
      console.log(uploaded_data);
      message.destroy();
      message.success("File Upload Success", 2);
      setVisiblePasteImage(false);
    } catch (e) {
      console.log(e);
      message.destroy();
      message.error("File Upload Failed", 2);
    }

    let message_data = {
      message: Date.now().toString(),
      thread_id: selectedGroup.thread_id,
      sender: userObj,
      timestamp: Date.now(),
      filename: Date.now().toString(),
      type: "file",
      location: uploaded_data.location,
    };

    let notification_data = {
      sender: userObj.username,
      receiver: selectedGroup.group_name,
      group: true,
      group_type: "group",
    };

    socket.emit("new_message", notification_data);

    socket.emit("new_group_message", message_data, app_id, (response) => {
      if (response !== "success") {
        message.error("Error in sending message");
      } else {
        setmessagearray([...messagearray, {...message_data}]);
        // setInputText("");

        let tempArr1 = [...groupReadUnreadList];

        const objIndex = tempArr1.findIndex(
          (obj) => obj.group_name === selectedGroup.group_name
        );
        tempArr1[objIndex].sender = userObj.username;

        let newArr = [
          ...tempArr1.filter((o) => o.group_name === selectedGroup.group_name),
        ];
        tempArr1 = tempArr1.filter(
          (obj) => obj.group_name !== selectedGroup.group_name
        );
        newArr = [...newArr, ...tempArr1];
        console.log(newArr);
        setgroupReadUnreadList([...newArr]);
      }
    });
    setSelectedReplyMessage(null);
  };

  const uploadGroupReplyImage = async () => {
    // var file = new File([clipImage], Date.now().toString().png);
    const base64Data = new Buffer.from(
      clipImage.replace(/^data:image\/\w+;base64,/, ""),
      "base64"
    );
    const file = new File([base64Data], "noname", {type: "image/png"});
    console.log("myfile", base64Data);

    const S3Client = new S3(config);
    let uploaded_data;
    try {
      uploaded_data = await S3Client.uploadFile(file, Date.now().toString());
      console.log(uploaded_data);
      message.destroy();
      message.success("File Upload Success", 2);
      setVisiblePasteImage(false);
    } catch (e) {
      console.log(e);
      message.destroy();
      message.error("File Upload Failed", 2);
    }

    let message_data = {
      message: Date.now().toString(),
      replied_msg: true,
      replied_to_data: {
        message: selectedReplyMessage.message,
        threadId: selectedReplyMessage.threadId,
        sender: selectedReplyMessage.sender,
        timestamp: selectedReplyMessage.timestamp,
        type: selectedReplyMessage.type ? selectedReplyMessage.type : null,
        location: selectedReplyMessage.location
          ? selectedReplyMessage.location
          : null,
      },
      thread_id: selectedGroup.thread_id,
      sender: userObj,
      timestamp: Date.now(),
      filename: Date.now().toString(),
      type: "file",
      location: uploaded_data.location,
    };

    let notification_data = {
      sender: userObj.username,
      receiver: selectedGroup.group_name,
      group: true,
      group_type: "group",
    };

    socket.emit("new_message", notification_data);

    socket.emit("new_group_message", message_data, app_id, (response) => {
      if (response !== "success") {
        message.error("Error in sending message");
      } else {
        setmessagearray([...messagearray, {...message_data}]);
        // setInputText("");

        let tempArr1 = [...groupReadUnreadList];

        const objIndex = tempArr1.findIndex(
          (obj) => obj.group_name === selectedGroup.group_name
        );
        tempArr1[objIndex].sender = userObj.username;

        let newArr = [
          ...tempArr1.filter((o) => o.group_name === selectedGroup.group_name),
        ];
        tempArr1 = tempArr1.filter(
          (obj) => obj.group_name !== selectedGroup.group_name
        );
        newArr = [...newArr, ...tempArr1];
        console.log(newArr);
        setgroupReadUnreadList([...newArr]);
      }
    });
    setSelectedReplyMessage(null);
  };

  const conditionalImageUpload = () => {
    if (selectedGroup !== null) {
      if (selectedReplyMessage) {
        uploadGroupReplyImage();
      } else {
        uploadGroupImage();
      }
    }
  };

  const handleShiftEnter = (e) => {
    var x = document.getElementById("demo");
    if (e.keyCode == 13 && e.shiftKey) {
      // x.innerHTML = "SHIFT + ENTER key was pressed!";
      // alert("add new div");
    } else if (e.keyCode == 13) {
      e.preventDefault();
      conditionalSubmit();
      // x.innerHTML = "ENTER key pressed!";
    } else {
      // alert("")
      // x.innerHTML = "other text";
    }
  };

  const addEmoji = (e) => {
    //console.log(e.native)
    let emoji = e.native;
    bodyRef.current.innerHTML = bodyRef.current.innerHTML + " " + emoji;
    setemojiShow(false);
    // setInputText(inputText + " " + emoji);
  };

  return (
    <>
      <div className={"myeditor"}>
        {selectedReplyMessage !== null ? (
          <div
            className="shadow"
            style={{
              padding: "10px",
              margin: "10px",
              // backgroundColor: "",
              background:
                localStorage.getItem("mode") === "light"
                  ? "#EEEEEE"
                  : localStorage.getItem("mode") === "dark"
                  ? "#121212"
                  : "",
              color:
                localStorage.getItem("mode") === "light"
                  ? "black"
                  : localStorage.getItem("mode") === "dark"
                  ? "lightgray"
                  : "",

              borderRadius: "10px",
            }}
          >
            <div
              style={{
                fontWeight: "bold",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <div style={{paddingBottom: "5px"}}>
                {selectedReplyMessage.sender.username}
              </div>
              <CloseOutlined
                type="close"
                style={{cursor: "pointer"}}
                onClick={(e) => setSelectedReplyMessage(null)}
              />
            </div>
            {/* <div>{renderMessage(selectedReplyMessage)}</div> */}
          </div>
        ) : (
          ""
        )}
        <div
          style={{
            display: "flex",
            width: "100%",
            alignItems: "center",
            background:
              localStorage.getItem("mode") === "light"
                ? "#EDEDED"
                : localStorage.getItem("mode") === "dark"
                ? "#121212"
                : "",
            padding: "10px",
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              paddingLeft: "5px",
              paddingRight: "10px",
              color: "#8D8D8D",
            }}
          >
            <Popover
              placement="topLeft"
              content={<Picker onSelect={addEmoji} skin={2} title="Nvest" />}
              trigger="click"
            >
              <SmileOutlined
                type="smile"
                style={{
                  fontSize: "18px",
                }}
                onClick={(e) => setemojiShow(!emojiShow)}
              />
            </Popover>
          </div>

          <div
            onKeyDown={(e) => {
              handleShiftEnter(e);
              console.log("changing");
              socket.emit("typing", userObj.username, "thread_id");
            }}
            onPaste={pasteImageHandler}
            ref={bodyRef}
            className={
              localStorage.getItem("mode") === "light"
                ? "chat-input"
                : localStorage.getItem("mode") === "dark"
                ? "chat-input-dark"
                : "chat-input"
            }
            contentEditable="true"
            placeholder="Write Something..."
          ></div>

          <div
            style={{
              display: "flex",
              alignItems: "center",
              paddingLeft: "10px",
              paddingRight: "5px",
              color: "#8D8D8D",
            }}
          >
            <img
              style={{cursor: "pointer"}}
              src={send}
              alt=""
              width="20px"
              onClick={conditionalSubmit}
            />
          </div>
        </div>
      </div>

      {/* On Paste Image this appears */}
      <Modal
        width={300}
        bodyStyle={{padding: "0px"}}
        title="Upload Image?"
        visible={visiblePasteImage}
        onOk={conditionalImageUpload}
        onCancel={(e) => setVisiblePasteImage(false)}
      >
        <img src={clipImage} alt="" style={{width: "100%"}} />
      </Modal>
    </>
  );
};

export default StreamChatEditor;
