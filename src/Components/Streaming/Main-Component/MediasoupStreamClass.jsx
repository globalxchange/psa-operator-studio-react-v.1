export class MediasoupStreamClass {
  constructor(
    setFlags,
    setReloadFlags,
    stream_hosts,
    setReccordState,
    recordState,
    streamType,
    coHost,
    finalAudioType,
    finalVideoType,
    profiledata,
    exitCall,
    host,
    mediasoupClient,
    sockets,
    roomID,
    Name,
    successCallback,
    bodyConnections,
    userReferencef,
    screenReferencef,
    Recorders,
    redirectFunction,
    setCameras,
    setFirsts,
    firsts
  ) {
    console.log("-------------------constructor-------------");
    console.log(sockets);
    this.setReloadFlag = setReloadFlags;
    this.setFlag = setFlags;
    this.stream_hosts = stream_hosts;
    this.setReccordState = setReccordState;
    this.coHosts = coHost;
    this.recordState = recordState;
    this.streamType = streamType;
    this.finalAudioType = finalAudioType;
    this.finalVideoType = finalVideoType;
    this.MeetingList = [];
    this.producer = "";
    this.first = firsts;
    this.userReference = userReferencef;
    this.screenReference = screenReferencef;
    this.profileData = profiledata;
    this.setFirst = setFirsts;
    this.setCamera = setCameras;
    this.name = Name;
    this.exitCall = exitCall;
    this.Recorder = Recorders;
    this.redirectFunction = redirectFunction;
    this.mediasoupClient = mediasoupClient;
    this.bodyConnections = bodyConnections;
    this.socket = sockets;
    this.producerTransport = null;
    this.consumerTransport = null;
    this.device = null;
    this.room_id = roomID;
    console.log(this.room_id);
    console.log(profiledata);
    this.consumers = new Map();
    this.producers = new Map();
    this.producerLabel = new Map();
    this.tempReferenceObj = {};
    this.currentType = "";
    console.log("-------------------constructor------------");

    if (this.coHosts === "viewer") {
      const asyncFunction = async () => {
        await this.join(this.name, this.room_id);
        this.initSockets();
        successCallback(true);
      };
      asyncFunction();
    } else {
      this.createRoom(this.room_id).then(
        async function () {
          await this.join(this.name, this.room_id);
          this.initSockets();
          successCallback(true);
        }.bind(this)
      );
    }
  }

  ////////// INIT /////////

  async createRoom(room_id) {
    console.log(this.socket);
    let data = {
      room_id,
      user_limit: 100,
      ...this.profileData,
    };
    console.log("----data----");
    console.log(this.profileData);
    let res = await this.socket.request("createRoom", data).catch((err) => {
      console.log(err);
    });
    console.log("---------------resullt-------------------" + res);
  }

  async join(name, room_id) {
    console.log(room_id);
    try {
      let e = await this.socket.request("join", {
        room_id,
        ...this.profileData,
      });
      console.log(e);
      if (e.viewer_id) {
        this.profileData.user_psa_id = e.viewer_id;
        localStorage.setItem("viewer_psa_id", {
          meeting_id: room_id,
          id: e.viewer_id,
        });
      }
      const data = await this.socket.request("getRouterRtpCapabilities");
      console.log(data);
      let device = await this.loadDevice(data);
      this.device = device;
      console.log(device);
      await this.initTransports(device);
      console.log(this.producerTransport);
      this.socket.emit("getProducers");
    } catch (e) {
      console.log(e);
      if (e === "room does not exist") {
        this.setReloadFlag(true);
        this.setFlag(true);
      }
    }
    // .then(
    //   async function (e) {

    //   }.bind(this)
    // )
    // .catch((e) => {
    //   console.log(e);
    //   if (e === "room does not exist") {
    //     this.setReloadFlag(true);
    //     this.setFlag(true);
    //   }
    // });
  }

  async loadDevice(routerRtpCapabilities) {
    console.log(this.mediasoupClient);
    let device;
    try {
      device = new this.mediasoupClient.Device();
    } catch (error) {
      if (error.name === "UnsupportedError") {
        console.error("browser not supported");
      }
      console.error(error);
    }
    await device.load({
      routerRtpCapabilities,
    });
    return device;
  }

  async initTransports(device) {
    console.log("in init " + this.coHosts);
    // init producerTransport
    if (this.coHosts === "host" || this.coHosts === "co-host") {
      const data = await this.socket.request("createWebRtcTransport", {
        forceTcp: false,
        rtpCapabilities: device.rtpCapabilities,
      });
      if (data.error) {
        console.error(data.error);
        return;
      }

      this.producerTransport = device.createSendTransport(data);
      console.log(this.producerTransport);
      this.producerTransport.on(
        "connect",
        async function ({dtlsParameters}, callback, errback) {
          this.socket
            .request("connectTransport", {
              dtlsParameters,
              transport_id: data.id,
            })
            .then(callback)
            .catch(errback);
        }.bind(this)
      );

      this.producerTransport.on(
        "produce",
        async function ({kind, rtpParameters}, callback, errback) {
          try {
            const {producer_id} = await this.socket.request("produce", {
              producerTransportId: this.producerTransport.id,
              kind,
              rtpParameters,
              dontAdd: this.Recorder ? true : false,
              ...this.profileData,
              producer_type: this.currentType,
            });
            callback({
              id: producer_id,
            });
          } catch (err) {
            errback(err);
          }
        }.bind(this)
      );

      this.producerTransport.on(
        "connectionstatechange",
        function (state) {
          switch (state) {
            case "connecting":
              console.log("connecting producer");
              break;
            case "connected":
              console.log("connected producer transport");
              break;
            case "failed":
              this.producerTransport.close();
              break;
            default:
              break;
          }
        }.bind(this)
      );
    }

    // init consumerTransport
    {
      const data = await this.socket.request("createWebRtcTransport", {
        forceTcp: false,
      });
      if (data.error) {
        console.error(data.error);
        return;
      }

      // only one needed
      this.consumerTransport = device.createRecvTransport(data);
      this.consumerTransport.on(
        "connect",
        function ({dtlsParameters}, callback, errback) {
          this.socket
            .request("connectTransport", {
              transport_id: this.consumerTransport.id,
              dtlsParameters,
            })
            .then(callback)
            .catch(errback);
        }.bind(this)
      );

      this.consumerTransport.on(
        "connectionstatechange",
        async function (state) {
          switch (state) {
            case "connecting":
              break;

            case "connected":
              console.log("connected consumer transport");
              break;

            case "failed":
              this.consumerTransport.close();
              break;

            default:
              break;
          }
        }.bind(this)
      );
    }
  }

  initSockets() {
    this.socket.on(
      "consumerClosed",
      function ({consumer_id}) {
        console.log("closing consumer:", consumer_id);
        this.removeConsumer(consumer_id, "through init close");
      }.bind(this)
    );

    this.socket.on(
      "newProducers",
      async function (data) {
        console.log("new producers", data);
        let sendData = {
          psa_app_id: this.profileData.psa_app_id,
          type: this.profileData.type,
          room_id: this.room_id,
          user_psa_id: this.profileData.user_psa_id,
        };
        console.log(sendData);
        this.socket.emit("stream_viewer_list", sendData, (data) => {
          console.log("-----new participants list----");
          console.log(data);
          this.MeetingList = data.payload;
        });

        for (let {producer_id, dontAddProducer} of data) {
          if (this.producers.get(producer_id)) {
            console.log(
              "-------------returning due to produce found-----------------"
            );
            return;
          } else {
            if (dontAddProducer) {
              return;
            } else {
              console.log("into producer found");
              setTimeout(async () => {
                await this.consume(producer_id);
              }, 1000);
            }
          }
        }
      }.bind(this)
    );

    this.socket.on(`activeChange_${this.room_id}`, async (data) => {
      console.log(data);
      console.log(
        `---------------change in active stream with producer-id + ${data.data.producer_id}--------------`
      );
      if (this.coHosts === "co-host" && data.data.type === "co-host") {
        console.log("--internal producer change");
        return;
      }
      if (this.coHosts === "host" && data.data.type === "host") {
        console.log("--internal producer change--");
        return;
      }
      console.log(this.consumers);
      let array = Array.from(this.consumers, ([name, value]) => ({
        name,
        value,
      }));
      console.log(array);
      let requiredConsumer = array.find(
        (item) => item.value.producerId === data.data.producer_id
      );
      let secondaryConsumer = "";
      if (array.length > 2) {
        secondaryConsumer = array.find(
          (item) =>
            item.value.producerId !== data.data.producer_id &&
            item.value.consumer.kind !== "audio"
        );
      }
      console.log(requiredConsumer);
      console.log(secondaryConsumer);
      if (this.coHosts === "co-host" && data.data.type !== "co-host") {
        let obj = {
          source: this.userReference.current.srcObject,
          id: this.userReference.current.id,
        };
        this.tempReferenceObj = obj;
        // this.tempReference = this.userReference.current.srcObject;
        // this.tempReference = this.userReference.current.producerId;
        this.userReference.current.srcObject =
          requiredConsumer.value.stream.stream;
      }
      if (this.coHosts === "host" && data.data.type !== "host") {
        let obj = {
          source: this.screenReference.current.srcObject,
          id: this.screenReference.current.id,
        };
        this.tempReferenceObj = obj;
        // this.tempReference = this.screenReference.current.srcObject;
        this.screenReference.current.srcObject =
          requiredConsumer.value.stream.stream;
      }
      if (this.coHosts === "viewer") {
        if (data.data.type === "co-host") {
          let obj = {
            source: this.screenReference.current.srcObject,
            id: this.screenReference.current.id,
          };
          this.tempReferenceObj = obj;
          // this.tempReference = this.screenReference.current.srcObject;
          this.screenReference.current.srcObject =
            requiredConsumer.value.stream.stream;
        } else if (data.data.type === "host") {
          // let obj = {
          //   source: this.userReference.current.srcObject,
          //   id: this.userReference.current.id,
          // };
          // this.tempReferenceObj = obj;
          console.log(requiredConsumer.value.stream);
          console.log(this.userReference.current);
          // this.tempReference = this.userReference.current.srcObject;
          this.userReference.current.srcObject =
            requiredConsumer.value.stream.stream;

          console.log(this.userReference.current);
        }
      }
    });
  }

  //////// MAIN FUNCTIONS /////////////

  async produce(
    type,
    reference,
    deviceId = null,
    producer_type,
    finalAudioType,
    finalVideoType
  ) {
    console.log(producer_type);
    this.currentType = producer_type;
    let record = this.Recorder;
    console.log(
      "-----------------------in produce----------------------------"
    );
    console.log(record);
    let mediaConstraints = {};
    console.log(this.producerTransport);
    let audio = false;
    let screen = false;
    switch (type) {
      case mediaType.audio:
        mediaConstraints = {
          audio: {
            autoGainControl: false,
            echoCancellation: true,
            latency: 0,
            noiseSuppression: false,
            sampleRate: 48000,
            sampleSize: 16,
            volume: 1.0,
          },
          video: false,
        };
        audio = true;
        break;
      case mediaType.video:
        mediaConstraints = {
          video: {
            width: {
              min: 640,
              ideal: 1920,
              max: 3840,
            },
            height: {
              min: 400,
              ideal: 1080,
              max: 2160,
            },
            frameRate: {
              min: 15,
              max: 120,
            },
            deviceId: deviceId,
          },
        };
        break;
      case mediaType.screen:
        mediaConstraints = false;
        screen = true;
        audio = false;
        break;
      default:
        return;
    }
    console.log(this.device);
    if (!this.device.canProduce("video") && !audio) {
      console.error("cannot produce video");
      return;
    }
    if (this.producerLabel.has(type)) {
      console.log("producer already exists for this type " + type);
      return;
    }
    console.log("mediacontraints:", mediaConstraints);

    let stream;
    try {
      stream = screen
        ? await navigator.mediaDevices.getDisplayMedia({
            video: true,
            audio: {
              autoGainControl: false,
              echoCancellation: false,
              googAutoGainControl: false,
              noiseSuppression: false,
            },
          })
        : await navigator.mediaDevices.getUserMedia(mediaConstraints);
      // prompt();
      let track;

      if (audio) {
        console.log("-------in producer audio---" + finalAudioType);
        track = stream.getAudioTracks()[finalAudioType];
      } else {
        console.log("-------in producer audio---" + finalVideoType);
        track = stream.getVideoTracks()[finalVideoType];
      }

      let params = {
        track,
      };
      if (screen) {
        console.log("-------------coz im recorder----------------");
        params = {track: stream.getVideoTracks()[0]};
      }
      if (!audio && !screen) {
        params.encodings = [
          {
            rid: "r0",
            maxBitrate: 100000,
            //scaleResolutionDownBy: 10.0,
            scalabilityMode: "S1T3",
          },
          {
            rid: "r1",
            maxBitrate: 300000,
            scalabilityMode: "S1T3",
          },
          {
            rid: "r2",
            maxBitrate: 900000,
            scalabilityMode: "S1T3",
          },
        ];
        params.codecOptions = {
          videoGoogleStartBitrate: 1000,
        };
      }
      let producer = "";
      let audioProducer = "";
      console.log(this.producerTransport);
      producer = await this.producerTransport.produce(params);
      this.socket.request("set_producer_type", {
        producer_id: producer.id,
        producer_type,
      });
      let audioRestricted = false;
      if (screen) {
        if (stream.getAudioTracks()[0]) {
          console.log("stream audios", stream.getAudioTracks());
          // this.setCurrent('screen_audio');
          // this.currentType = 'screen_audio';
          let params = {track: stream.getAudioTracks()[0]};
          audioProducer = await this.producerTransport.produce(params);
          this.socket.request("set_producer_type", {
            producer_id: audioProducer.id,
            producer_type: "screen_audio",
          });
        } else {
          audioRestricted = true;
        }
      }
      this.producers.set(producer.id, producer);

      let elem;
      console.log(stream);

      if (!audio) {
        if (screen) {
          if (record) {
            // this.screenReference.current.id = producer.id;
          } else {
            console.log("in screen block");
            switch (this.streamType) {
              case "Streaming_SS":
                if (this.coHosts === "co-host") {
                  this.screenReference.current.srcObject = stream;
                  this.screenReference.current.id = producer.id;
                } else if (this.coHosts === "host") {
                  this.userReference.current.srcObject = stream;
                  this.userReference.current.id = producer.id;
                }
                break;
              case "Streaming_SC":
                this.screenReference.current.srcObject = stream;
                this.screenReference.current.id = producer.id;
                break;
              case "Streaming_CC":
                break;
              case "Streaming_HH":
                if (this.coHosts === "co-host") {
                  // this.tempReference = this.screenReference;
                  let obj = {
                    source: this.screenReference.current.srcObject,
                    id: this.screenReference.current.id,
                  };
                  this.tempReferenceObj = obj;
                  this.screenReference.current.srcObject = stream;
                  this.screenReference.current.id = producer.id;
                  this.screenReference.current.playsinline = false;
                  this.screenReference.current.autoplay = true;
                } else if (this.coHosts === "host") {
                  let obj = {
                    source: this.userReference.current.srcObject,
                    id: this.userReference.current.id,
                  };
                  this.tempReferenceObj = obj;
                  // let temp = this.userReference;
                  this.userReference.current.srcObject = stream;
                  this.userReference.current.id = producer.id;
                  this.userReference.current.playsinline = false;
                  this.userReference.current.autoplay = true;
                  // this.tempReference = temp;
                }
                break;
              default:
                break;
            }
          }
        } else {
          if (!screen) {
            if (this.coHosts === "host") {
              this.userReference.current.srcObject = stream;
              this.userReference.current.playsinline = false;
              this.userReference.current.autoplay = true;
              this.userReference.current.id = producer.id;
            } else if (this.coHosts === "co-host") {
              this.screenReference.current.srcObject = stream;
              this.screenReference.current.playsinline = false;
              this.screenReference.current.autoplay = true;
              this.screenReference.current.id = producer.id;
            }
          }
        }
      }
      if (screen) {
        this.producerLabel.set(type, producer.id);
        if (!audioRestricted) {
          this.producerLabel.set("screenAudio", audioProducer.id);
        }
      } else {
        console.log("--------------- general producer----------------------");
        this.producerLabel.set(type, producer.id);
      }
      console.log(this.producerLabel);
      producer.on("trackended", () => {
        this.closeProducer(type);
      });

      producer.on("transportclose", () => {
        console.log("producer transport close");
        if (!audio) {
          elem.srcObject.getTracks().forEach(function (track) {
            track.stop();
          });
          elem.parentNode.removeChild(elem);
        }
        this.producers.delete(producer.id);
      });

      producer.on("close", () => {
        console.log("closing producer");
        if (!audio) {
          elem.srcObject.getTracks().forEach(function (track) {
            track.stop();
          });
          elem.parentNode.removeChild(elem);
        }
        this.producers.delete(producer.id);
      });

      if (screen && !audioRestricted) {
        audioProducer.on("trackended", () => {
          this.closeProducer(type);
        });
        audioProducer.on("transportclose", () => {
          console.log("producer transport close");
          if (!audio) {
            elem.srcObject.getTracks().forEach(function (track) {
              track.stop();
            });
            elem.parentNode.removeChild(elem);
          }
          this.producers.delete(producer.id);
        });
        audioProducer.on("close", () => {
          console.log("closing producer");
          if (!audio) {
            elem.srcObject.getTracks().forEach(function (track) {
              track.stop();
            });
            elem.parentNode.removeChild(elem);
          }
          this.producers.delete(producer.id);
        });
      }
    } catch (err) {
      console.log(err);
      if (err.message.includes("Permission denied")) {
        console.log(this.first);
        this.setCamera(false);
        this.setFirst(false);
        console.log(this.first);
        if (this.Recorder) {
          this.produce(type, reference, deviceId);
        } else {
          throw new Error("ulala");
        }
      }
    }
  }
  async produceForScreenRecord(deviceId = null) {
    this.Recorder = true;
    this.currentType = "screen_record_video";
    console.log(
      "-----------------------in screen record produce----------------------------"
    );
    // this.Recorder = true
    let new_stream;
    try {
      let stream = await navigator.mediaDevices.getDisplayMedia({
        video: true,
        audio: {
          autoGainControl: false,
          echoCancellation: false,
          googAutoGainControl: false,
          noiseSuppression: false,
        },
      });
      let audio_stream = await navigator.mediaDevices.getUserMedia({
        audio: {
          autoGainControl: false,
          echoCancellation: true,
          latency: 0,
          noiseSuppression: false,
          sampleRate: 48000,
          sampleSize: 16,
          volume: 1.0,
        },
      });

      new_stream = new MediaStream();
      new_stream.addTrack(stream.getVideoTracks()[0]);

      const audioContext = new AudioContext();
      if (stream.getAudioTracks()[0]) {
        const voice = audioContext.createMediaStreamSource(audio_stream);
        const desktop = audioContext.createMediaStreamSource(stream);
        const destination = audioContext.createMediaStreamDestination();

        const desktopGain = audioContext.createGain();
        const voiceGain = audioContext.createGain();

        desktopGain.gain.value = 0.7;
        voiceGain.gain.value = 0.9;

        voice.connect(voiceGain).connect(destination);
        desktop.connect(desktopGain).connect(destination);

        new_stream.addTrack(destination.stream.getAudioTracks()[0]);
      } else {
        new_stream.addTrack(audio_stream.getAudioTracks()[0]);
      }

      let producer = "";
      let audioProducer = "";

      console.log(this.producerTransport);
      producer = await this.producerTransport.produce({
        track: stream.getVideoTracks()[0],
      });
      this.socket.request("set_producer_type", {
        producer_id: producer.id,
        producer_type: "screen_record_video",
      });
      audioProducer = await this.producerTransport.produce({
        track: stream.getAudioTracks()[0],
      });
      this.socket.request("set_producer_type", {
        producer_id: audioProducer.id,
        producer_type: "screen_record_audio",
      });

      this.producers.set(producer.id, producer);
      this.producers.set(audioProducer.id, producer);
      this.producerLabel.set("screen_record_audio", audioProducer.id);
      this.producerLabel.set("screen_record_video", producer.id);

      console.log(this.producerLabel);
      producer.on("trackended", () => {
        this.closeProducer("screen_record_video");
      });
      producer.on("transportclose", () => {
        this.producers.delete(producer.id);
      });
      producer.on("close", () => {
        console.log("closing producer");
        this.producers.delete(producer.id);
      });

      audioProducer.on("trackended", () => {
        this.closeProducer("screen_record_video");
      });
      audioProducer.on("transportclose", () => {
        this.producers.delete(producer.id);
      });
      audioProducer.on("close", () => {
        console.log("closing producer");
        this.producers.delete(producer.id);
      });
      this.setReccordState(!this.recordState);
    } catch (err) {
      console.log(err);
      if (err.message.includes("Permission denied")) {
        console.log(this.first);
        this.setCamera(false);
        this.setFirst(false);
        console.log(this.first);
        if (this.Recorder) {
          this.produceForScreenRecord(deviceId);
        } else {
          throw new Error("ulala");
        }
      }
    }
  }
  async recordFunction(data) {
    console.log(this.producerLabel);

    let producer_id1 = this.producerLabel.get("screen_record_video");
    let producer_id2 = this.producerLabel.get("screen_record_audio");
    console.log(producer_id1 + "---" + producer_id2);
    let producers = [producer_id1, producer_id2];
    this.socket.request("start_record", {data, producers}, (result) => {
      console.log(result);
    });
  }
  async stopRecording() {
    this.socket.request("stop_record");
    this.closeProducer("screen_record_video");
    this.closeProducer("screen_record_audio");
  }
  async consume(producer_id) {
    this.getConsumeStream(producer_id).then(
      function ({consumer, stream, kind}) {
        let consumerObj = {consumer};
        consumerObj.stream = {stream};
        consumerObj.kind = kind;
        consumerObj.producerId = producer_id;
        this.consumers.set(consumer.id, consumerObj);
        let selected_item;
        console.log(producer_id);
        console.log(this.MeetingList);
        for (let item of this.MeetingList) {
          if (item.producers === undefined) {
          } else {
            console.log("-----in producers exits------");
            console.log(producer_id);
            console.log(item);
            let res = item.producers.find(
              (e) => e.producer_id === producer_id && e.end_ts === null
            );
            console.log(res);
            if (res) {
              selected_item = item;
            } else {
              console.log("=======in item but producer not found-----------");
            }
          }
        }
        if (!selected_item) {
          return;
        }
        console.log(selected_item);
        console.log(
          "---consumer-id---" +
            consumer.id +
            "--------producer_id---" +
            producer_id
        );
        if (kind === "video") {
          console.log("------------------stream video--------------");
          if (this.coHosts === "host") {
            this.screenReference.current.srcObject = stream;
            this.screenReference.current.id = consumer.id;
            this.screenReference.current.playsinline = false;
            this.screenReference.current.autoplay = true;
          } else if (this.coHosts === "co-host") {
            this.userReference.current.srcObject = stream;
            this.userReference.current.playsinline = false;
            this.userReference.current.autoplay = true;
            this.userReference.current.id = consumer.id;
          } else {
            let e = this.stream_hosts.find(
              (item) => item.id === selected_item.userid
            );
            console.log(e);
            if (e.type === "host") {
              console.log("in viewer consumer-----" + producer_id);
              // this.tempReference = this.userReference;
              // this.tempReference.current.id = this.userReference.current.id;
              this.userReference.current.srcObject = stream;
              this.userReference.current.playsinline = false;
              this.userReference.current.autoplay = true;
              this.userReference.current.id = consumer.id;
            } else if (e.type === "co-host") {
              console.log("in viewer consumer-----" + producer_id);

              // this.tempReference = this.screenReference;
              this.screenReference.current.srcObject = stream;
              this.screenReference.current.id = consumer.id;
              this.screenReference.current.playsinline = false;
              this.screenReference.current.autoplay = true;
            }
          }
          console.log(this.userReference.current.srcObject);
        } else {
          let elem = document.createElement("div");
          let video = document.createElement("video");
          video.srcObject = stream;
          video.src = stream;
          elem.id = consumer.id + "one";
          video.producer_id = producer_id;
          video.id = consumer.id;
          video.playsinline = false;
          video.autoplay = true;
          video.className = "none";
          elem.appendChild(video);
          this.bodyConnections.current.appendChild(elem);
        }
        consumer.on(
          "trackended",
          function () {
            this.removeConsumer(consumer.id, "through track ended");
          }.bind(this)
        );
        consumer.on(
          "transportclose",
          function () {
            this.removeConsumer(consumer.id, "through transaport close");
          }.bind(this)
        );
      }.bind(this)
    );
  }

  async getConsumeStream(producerId) {
    const {rtpCapabilities} = this.device;
    const data = await this.socket.request("consume", {
      rtpCapabilities,
      consumerTransportId: this.consumerTransport.id, // might be
      producerId,
    });
    const {id, kind, rtpParameters} = data;

    let codecOptions = {};
    const consumer = await this.consumerTransport.consume({
      id,
      producerId,
      kind,
      rtpParameters,
      codecOptions,
    });
    const stream = new MediaStream();
    stream.addTrack(consumer.track);
    return {
      consumer,
      stream,
      kind,
    };
  }

  closeProducer(type) {
    if (!this.producerLabel.has(type)) {
      console.log("there is no producer for this type " + type);
      return;
    }
    let producer_id = this.producerLabel.get(type);
    console.log(producer_id);
    this.socket.emit("producerClosed", {
      producer_id,
      ...this.profileData,
      room_id: this.room_id,
    });

    if (type === "screenType" && this.streamType === "Streaming_HH") {
      if (this.coHosts === "host") {
        this.userReference.current.srcObject = this.tempReferenceObj.source;
        this.userReference.current.id = this.tempReferenceObj.id;
        let data = {producer_id: this.tempReferenceObj.id, type: "host"};
        this.socket.request(`active`, {data}, (data) => {
          console.log("socket request" + data);
        });
        console.log("host -end");
      } else if (this.coHosts === "co-host") {
        this.screenReference.current.srcObject = this.tempReferenceObj.source;
        this.screenReference.current.id = this.tempReferenceObj.id;
        let data = {producer_id: this.tempReferenceObj.id, type: "co-host"};
        this.socket.request(`active`, {data}, (data) => {
          console.log("socket request" + data);
        });
        console.log("cohost -end");
      }
      this.setFirst(false);
    } else {
      if (type === "screenType") {
        if (this.coHosts === "host") {
          this.userReference.current.srcObject
            .getTracks()
            .forEach(function (track) {
              track.stop();
            });
          this.setFirst(false);
        } else {
          this.screenReference.current.srcObject
            .getTracks()
            .forEach(function (track) {
              track.stop();
            });
          this.setFirst(false);
        }
      }
    }
    this.producers.get(producer_id).close();
    this.producers.delete(producer_id);
    this.producerLabel.delete(type);
    console.log("-------------closed producer----------------");
  }

  pauseProducer(type) {
    console.log(this.producerLabel);
    if (!this.producerLabel.has(type)) {
      console.log("there is no producer for this type " + type);
      return;
    }
    let producer_id = this.producerLabel.get(type);
    let data = {
      ...this.profileData,
      room_id: this.room_id,
      producer_id: producer_id,
      producer_type: type,
    };
    console.log(data);
    this.socket.request("paused_producer", data, (data) => {
      console.log("-----in pause producer function------" + data);
    });
    this.producers.get(producer_id).pause();
  }

  resumeProducer(type) {
    console.log(this.producerLabel);

    if (!this.producerLabel.has(type)) {
      console.log("there is no producer for this type " + type);
      return;
    }
    let producer_id = this.producerLabel.get(type);
    this.socket.request(
      "resume_producer",
      {
        ...this.profileData,
        room_id: this.room_id,
        producer_id: producer_id,
        producer_type: type,
      },
      (data) => {
        console.log("-----in resume producer function------" + data);
      }
    );
    this.producers.get(producer_id).resume();
  }

  removeConsumer(consumer_id) {
    let elem = document.getElementById(consumer_id);
    if (elem === null) {
      return;
    }
    elem.srcObject.getTracks().forEach(function (track) {
      track.stop();
    });
    this.consumers.delete(consumer_id);
  }

  exit(offline = false) {
    console.log("---in exit---");
    this.socket.request("user_left", {
      ...this.profileData,
    });
    if (this.coHosts === "host" || this.coHost === "co-host") {
      this.exitCall();
    }
    let clean = function () {
      console.log("---clean---");
      if (this.coHosts === "viewer" && this.consumerTransport !== null) {
        this.consumerTransport.close();
      } else {
        if (
          this.producerTransport !== null &&
          this.consumerTransport !== null
        ) {
          this.producerTransport.close();
          this.consumerTransport.close();
        }
      }
      this.socket.off("disconnect");
      this.socket.off("newProducers");
      this.socket.off("consumerClosed");
      this.redirectFunction();
    }.bind(this);

    if (!offline) {
      this.socket
        .request("exitRoom")
        .then((e) => console.log(e))
        .catch((e) => console.warn(e))
        .finally(
          function () {
            clean();
          }.bind(this)
        );
    } else {
      clean();
    }

    // this.event(_EVENTS.exitRoom);
  }

  static get mediaType() {
    return mediaType;
  }
}

const mediaType = {
  audio: "audioType",
  video: "videoType",
  screen: "screenType",
};
