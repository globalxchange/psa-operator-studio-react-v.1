import React, {useEffect, useState} from "react";
const Timing = () => {
  let timer;
  const [timingArray, setTimingArray] = useState("");

  const timerFunction = () => {
    timer = setInterval(() => {
      let now = new Date().getTime();
      let countDownDate = new Date("Feb 10, 2021 15:37:25").getTime();
      let count = countDownDate - now;
      let days = Math.floor(count / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (count % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((count % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((count % (1000 * 60)) / 1000);
      let milliseconds = Math.floor(count % 1000);
      setTimingArray([days, hours, minutes, seconds, milliseconds]); 
      if (count < 0) {
        clearInterval(timer); 
      }
    }, 1000);
  };

  useEffect(() => {
    console.log("interval");
    timerFunction();
    return () => clearInterval(timer);
  }, []);

  return (
    <div className="div-three-two">
      <div className="div-three-two-inner">
        <h3>{timingArray[0]}</h3>
        <p>Days</p>
      </div>
      <div className="div-three-two-inner">
        <h3>{timingArray[1]}</h3>
        <p>Hours</p>
      </div>
      <div className="div-three-two-inner">
        <h3>{timingArray[2]}</h3>
        <p>Minutes</p>
      </div>
      <div className="div-three-two-inner">
        <h3>{timingArray[3]}</h3>
        <p>Seconds</p>
      </div>
      <div className="div-three-two-inner">
        <h3>{timingArray[4]}</h3>
        <p>Milli-seconds</p>
      </div>
    </div>
  );
};
export default Timing;
