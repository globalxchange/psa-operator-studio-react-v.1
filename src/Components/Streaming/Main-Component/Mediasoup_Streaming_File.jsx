import React, {useEffect, useContext, useState, useRef} from "react";
import io from "socket.io-client";
import * as mediasoupClient from "mediasoup-client";
import "./stream.style.scss";
import {CounselAppContext} from "../../../Context_Api/Context";
import VideoOptionComponent from "./video-options/stream-video-option.component";
import {message} from "antd";
import Axios from "axios";
import {Redirect} from "react-router-dom";
import {MediasoupStreamClass} from "./MediasoupStreamClass";

const Streaming = React.memo(({socket, setFlag, setReloadFlag}) => {
  const {
    host,
    meetingRoom,
    Name,
    profileData,
    token,
    email,
    userReference,
    brainDetails,
    screenReference,
    setHostName,
    finalAudioType,
    finalVideoType,
    coHosts,
    presentMeeting,
    rc,
    setRC,
  } = useContext(CounselAppContext);
  const endpoint = "https://prstnview.pulse.stream";
  // const endpoint = 'msbackend.pulse.stream';
  // const endpoint = "https://localhost:3342/";

  // -------------------------------------------------------states----------------------------------------------------------------//
  const [recordState, setReccordState] = useState(false);
  const [options, setOptions] = useState(true);
  const [camera, setCamera] = useState(true);
  const [cameraOn, setCameraOn] = useState(true);
  const [mic, setMic] = useState(true);
  const bodyConnections = useRef(null);
  const [first, setFirst] = useState(false);
  const [roomID, setRoomID] = React.useState(meetingRoom);
  const [optionRecord, setOptionRecord] = useState(false);

  const Recorder = false;
  let user_psa_id = "";

  let profiledata = {};
  const vc = useRef();
  //-------------------------------------------------  -----------------------------------------------------------------------------//

  //-----------------------------------------Functions----------------------------------------------------------------------------//

  const exitCall = async () => {
    let data = {
      room_id: roomID,
      type: "grvc",
    };
    let res = Axios.post(
      "https://ms.apimachine.com/api/getcallreceipts",
      data,
      {
        headers: {
          token: token,
          email: email,
        },
      }
    );
    console.log(res.data);
  };
  const startRecordHit = () => {
    let data = {
      psa_app_id: profiledata.psa_app_id,
      brain_user_id: brainDetails,
      type: "grvc",
    };
    rc.recordFunction(data); // mediaRecorder.start(5500);
    message.success("Recording started");
    setOptionRecord(true);
  };
  const successCallback = () => {
    console.log("success Callback");
  };
  const redirectFunction = () => {
    console.log("----in redirect-----");
    window.close();
    if (host) {
      return <Redirect to="/appconfig" />;
    } else {
      window.location.href = "https://studio.pulse.stream/landing";
    }
  };
  const useEffectFunction = () => {
    console.log("-----" + host + "----" + coHosts);
    if (coHosts === "viewer") {
      if (localStorage.getItem("viewer_psa_id")) {
        console.log("viewer  uuid present");
        let meeting = localStorage.getItem("viewer_psa_id");
        if (meeting.meeting_id === meetingRoom) {
          user_psa_id = meeting.id;
        } else {
          user_psa_id = "8e8eefe346fe8cf6e993d3";
          localStorage.setItem("viewer_psa_id", {
            id: user_psa_id,
            meeting_id: meetingRoom,
          });
        }
      } else {
        user_psa_id = "8e8eefe346fe8cf6e993d3";
        localStorage.setItem("viewer_psa_id", {
          id: "8e8eefe346fe8cf6e993d3",
          meeting_id: meetingRoom,
        });
      }
    }
    profiledata = {
      user_psa_id:
        coHosts === "viewer" ? user_psa_id : profileData.psa_app_userId,
      psa_app_id: "ea422e04-d561-4f3f-951a-db183b699bf0",
      type: "omvs",
      username: Name,
    };
    console.log(profiledata);
    socket = io(endpoint);
    console.log(socket);
    socket.request = function request(type, data = {}) {
      return new Promise((resolve, reject) => {
        socket.emit(type, data, (data) => {
          if (data.error) {
            reject(data.error);
          } else {
            resolve(data);
          }
        });
      });
    };
    setTimeout(() => {
      ModalCall();
    }, 1000);
  };
  const ModalCall = () => {
    console.log("in Modal call");
    console.log(socket);
    console.log(profiledata);
    // console.log(finalVideoType + "--------" + finalAudioType);
    const streamType = presentMeeting.stream_type;
    let stream_hosts = presentMeeting.stream_hosts;
    console.log(presentMeeting);
    let room = new MediasoupStreamClass(
      setFlag,
      setReloadFlag,
      stream_hosts,
      setReccordState,
      recordState,
      streamType,
      coHosts,
      finalAudioType,
      finalVideoType,
      profiledata,
      exitCall,
      host,
      mediasoupClient,
      socket,
      roomID,
      Name,
      successCallback,
      bodyConnections,
      userReference,
      screenReference,
      Recorder,
      redirectFunction,
      setCamera,
      setFirst,
      first
    );
    setRC(room);
    socket.on(
      `joined_user_data${roomID}^^^^${profiledata.psa_app_id}`,
      (data) => {
        if (coHosts !== "viewer") {
          if (profiledata.user_psa_id === data.userid) {
          } else {
            setTimeout(() => {
              message.success(`${data.username} joined`);
            }, 2000);
          }
        } else {
          setTimeout(() => {
            message.success(`${data.username} joined`);
          }, 2000);
        }
      }
    );
    socket.on(
      `left_user_data${roomID}^^^^${profiledata.psa_app_id}`,
      (data) => {
        message.info(`${data.username} Left`);
      }
    );

    setTimeout(() => {
      switch (presentMeeting.stream_type) {
        case "Streaming_SS":
          console.log("----------------in Streaming_Ss-----------------");

          if (coHosts === "host" || coHosts === "co-host") {
            room.produce(
              "audioType",
              userReference,
              rc.device,
              "mic",
              finalVideoType,
              finalAudioType
            );
            room.produce(
              "screenType",
              userReference,
              rc.device,
              "screen",
              finalVideoType,
              finalAudioType
            );
          }

          break;
        case "Streaming_CC":
          console.log("----------------in Streaming_CC-----------------");

          if (coHosts === "host" || coHosts === "co-host") {
            room.produce(
              "audioType",
              userReference,
              rc.device,
              "mic",
              finalVideoType,
              finalAudioType
            );
            room.produce(
              "videoType",
              userReference,
              rc.device,
              "camera",
              finalVideoType,
              finalAudioType
            );
          }
          break;
        case "Streaming_SC":
          console.log("----------------in Streaming_SC-----------------");
          if (coHosts === "host" || coHosts === "co-host") {
            room.produce(
              "audioType",
              userReference,
              rc.device,
              "mic",
              finalVideoType,
              finalAudioType
            );
            room.produce(
              "videoType",
              userReference,
              rc.device,
              "camera",
              finalVideoType,
              finalAudioType
            );
          }
          break;
        case "Streaming_HH":
          console.log("----------------in Streaming_HH-----------------");
          if (coHosts === "host" || coHosts === "co-host") {
            room.produce(
              "audioType",
              userReference,
              rc.device,
              "mic",
              finalVideoType,
              finalAudioType
            );
            room.produce(
              "videoType",
              userReference,
              rc.device,
              "camera",
              finalVideoType,
              finalAudioType
            );
          }
          break;
        default:
          break;
      }
      let conf_details = {
        psa_app_id: profiledata.psa_app_id,
        type: profiledata.type,
        room_id: meetingRoom,
      };

      socket.emit("stream_details", conf_details, async (data) => {
        console.log("------------conference details-------------");
        console.log(data.payload);
        setHostName(data.payload.username);
      });
    }, 3500);
  };

  const record = (recordType) => {
    console.log(recordType);
    console.log("in record funtion");
    switch (recordType) {
      case "start_recording":
        console.log("in start" + brainDetails);
        rc.produceForScreenRecord(rc.device);

        break;
      case "stop_recording":
        console.log("in stop");
        // mediaRecorder.stop();
        rc.stopRecording();
        message.success("Recording stoped");
        setReccordState(false);
        setOptionRecord(false);

        break;
      default:
        break;
    }
  };

  //----------------------------------------------------------------------------------------------------------------------------//

  //-------------------------------------use Effects----------------------------------------------------------------------------//

  useEffect(() => {
    if (recordState) {
      startRecordHit();
    }
  }, [recordState]);
  useEffect(() => {
    useEffectFunction();
    return () => {
      socket.request("disconnect");
      socket.off();
    };
  }, [endpoint, Name, meetingRoom]);

  useEffect(() => {
    const handleBeforeUnload = (event) => event.preventDefault();
    window.addEventListener(" rocket", handleBeforeUnload);
    return () => window.removeEventListener("beforeunload", handleBeforeUnload);
  }, []);
  //---------------------------------------------------------------------------------------------------------------------------//

  return (
    <React.Fragment>
      <div ref={vc} id="video-container" className="terminal-video">
        <VideoOptionComponent
          host={host}
          first={first}
          setFirst={setFirst}
          setCameraOn={setCameraOn}
          socket={socket}
          roomReference={rc}
          cameraOn={cameraOn}
          camera={camera}
          setCamera={setCamera}
          reference={screenReference}
          mic={mic}
          setOptions={setOptions}
          options={options}
          setMic={setMic}
          finalVideoType={finalVideoType}
          finalAudioType={finalAudioType}
          record={record}
          optionRecord={optionRecord}
          setOptionRecord={setOptionRecord}
        />

        <div ref={bodyConnections} style={{display: "none"}}></div>
      </div>
    </React.Fragment>
  );
});

export default Streaming;
