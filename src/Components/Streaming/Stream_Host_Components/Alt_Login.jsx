import React, {useState, useContext, useEffect} from "react";
import "../../../pages/Login/Login.scss";
import message from "antd/lib/message";
import Axios from "axios";
import Fade from "react-reveal/Fade";
import {CounselAppContext} from "../../../Context_Api/Context";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import PulseImage from "../../../static/images/PulseLoginImage.png";

function Alt_Login(props) {
  const {
    login,
    email,
    setPresentMeeting,
    meetingRoom,
    setMeetingRoom,
    setProfileData,
    Name,
  } = useContext(CounselAppContext);
  const [emailId, setemailId] = useState(email);
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const [mfaEnabled, setMfaEnabled] = useState(false);
  const [googlePin, setGooglePin] = useState("");
  const [profilePic, setProfilePic] = useState("a");
  const [profileName, setProfileName] = useState("");
  const userLogin = (e) => {
    e.preventDefault();
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailId)) {
      setLoading(true);
      if (mfaEnabled) {
        Axios.post("https://gxauth.apimachine.com/gx/user/login", {
          email: emailId,
          password: password,
          totp_code: googlePin,
        })
          .then(async (response) => {
            const {data} = response;
            if (data.status) {
              await login(emailId, data.accessToken, data.idToken);

              if (props.history) {
                switch (props.match.params.type) {
                  case "stream":
                    props.history.push(
                      `/streamroomLink/${meetingRoom}/${Name}/${email}/logged`
                    );
                    break;

                  case "groupcall":
                    props.history.push(
                      `/meetingroomLink/${meetingRoom}/${Name}/${email}/logged`
                    );
                    break;
                  default:
                    break;
                }
              }
            } else {
              message.error(data.message);
            }
          })
          .finally(() => {
            setLoading(false);
          });
      } else {
        Axios.post("https://gxauth.apimachine.com/gx/user/login", {
          email: emailId,
          password: password,
        })
          .then((response) => {
            const {data} = response;
            if (data.status) {
              login(emailId, data.accessToken, data.idToken);
              Axios.get(
                `https://comms.globalxchange.com/user/details/get?email=${emailId}`
              ).then(async (res) => {
                console.log("data :>> ", res.data.status);
                if (res.data.status) {
                  setProfileName(res.data.user.name);
                  setProfilePic(res.data.user.profile_img);
                  console.log(props.match.params.type);
                  let res2 = await Axios.get(
                    `https://ms.apimachine.com/api/get_by_email/${emailId}`
                  );
                  console.log(res2);
                  let decodedString = atob(
                    localStorage.getItem("current_meeting")
                  );
                  setProfileData(res2.data.thefetch);
                  let decodedObj = JSON.parse(decodedString);
                  setPresentMeeting(decodedObj.meeting);
                  setemailId(decodedObj.email);
                  setMeetingRoom(decodedObj.room);

                  decodedObj.profileData = res2.data.thefetch;
                  decodedObj.timestamp = Date.now();
                  let objectString = JSON.stringify(decodedObj);
                  let encodedString = btoa(objectString);
                  localStorage.setItem("CounselAppLoginAccount", emailId);
                  console.log("token" + data.idToken);
                  localStorage.setItem("CounselAppToken", data.idToken);
                  localStorage.setItem(
                    "CounselAppAccessToken",
                    data.accessToken
                  );
                  localStorage.setItem("current_meeting", encodedString);

                  switch (props.match.params.type) {
                    case "streaming":
                      props.history.push(
                        `/streamroomLink/${decodedObj.room}/${decodedObj.Name}/${decodedObj.email}/logged`
                      );
                      break;

                    case "groupcall":
                      props.history.push(
                        `/meetingroomLink/${decodedObj.room}/${decodedObj.Name}/${decodedObj.email}/logged`
                      );
                      break;
                    default:
                      break;
                  }
                }
              });
            } else if (data.mfa) {
              setMfaEnabled(true);
            } else {
              setLoading(false);
              message.error(data.message);
            }
          })
          .catch((error) => {
            message.error("Some Thing Went Wrong!");
            setLoading(false);
          });
      }
    } else {
      message.error("Enter Valid Email");
    }
  };
  useEffect(() => {
    if (localStorage.getItem("current_meeting") === "") {
      console.log("current meeting");
      window.location.href = "https://studio.pulse.stream/landing";
    }
  }, []);
  return (
    <div className="login-ag">
      <div className="form-wrapper">
        <img src={PulseImage} style={{width: "14rem"}} alt="no_img" />
        <form className="login-form mx-auto" onSubmit={userLogin}>
          <Fade bottom>
            <div className="group">
              <input
                type="text"
                name="email"
                value={emailId}
                onChange={(e) => setemailId(e.target.value)}
                required="required"
              />
              <span className="highlight" />
              <span className="bar" />
              <label>Email</label>
            </div>
          </Fade>
          <Fade bottom>
            <div className="group">
              <input
                type="password"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required="required"
              />
              <span className="highlight" />
              <span className="bar" />
              <label>Password</label>
            </div>
          </Fade>
          <Fade bottom>
            <button
              type="submit"
              disabled={loading}
              className="btn btn-darkblue mb-5"
              style={{fontFamily: "Montserrat"}}
            >
              {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "UNLOCK"}
            </button>
          </Fade>
        </form>
      </div>
    </div>
  );
}

export default Alt_Login;
