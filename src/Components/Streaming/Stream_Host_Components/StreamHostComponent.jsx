import React, {useContext, useEffect} from "react";
import {CounselAppContext} from "../../../Context_Api/Context";
// import Main from './Main-Component/Main';
import "../Main-Component/streamview.scss";
import queryString from "query-string";
import StreamView from "../Main-Component/StreamView";
import {loginLoader} from "../../../static/images/images";

const StreamHostCompoenent = (props) => {
  const context = useContext(CounselAppContext);
  const [flag, setFlag] = React.useState(true);
  useEffect(() => {
    getDetails();
  }, []);
  const getDetails = async () => {
    setFlag(true);
    try {
      let params = queryString.parse(props.location.search);
      await context.getBrain();
      let decodedString = atob(params.id);
      let decodedObj = JSON.parse(decodedString);
      console.log(decodedObj);
      context.setMeetingRoom(decodedObj.id);
      context.setName(decodedObj.name);
      context.setToken(decodedObj.token);
      context.setEmail(decodedObj.email);
      console.log(decodedObj.email);
      console.log(decodedObj.token);

      localStorage.setItem("CounselAppLoginAccount", decodedObj.email);
      localStorage.setItem("CounselAppToken", decodedObj.token);
      context.setProfileData(decodedObj.profileData);
      context.setSubDomainData(decodedObj.subDomainData);
      context.setPresentMeeting(decodedObj.presentMeeting);
      let obj = {
        Name: decodedObj.name,
        email: decodedObj.email,
        meeting: decodedObj.presentMeeting,
        room: decodedObj.id,
      };
      let objectString = JSON.stringify(obj);
      let encodedString = btoa(objectString);

      localStorage.setItem("current_meeting", encodedString);
      context.setMeetingLink(decodedObj.meetingLink);
      context.setHost(decodedObj.host);
      context.setCohosts("host");
      setTimeout(() => {
        setFlag(false);
      }, 1000);
    } catch (E) {
      console.log(E.message);
    }
  };
  return (
    <>
      {flag ? (
        <div
          style={{
            height: "100vh",
            width: "100vw",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={loginLoader}
            alt="Loader"
            style={{alignSelf: "center", height: "10vh"}}
          />
        </div>
      ) : (
        <div className="meeting-Main">
          <StreamView />
        </div>
      )}
    </>
  );
};

export default StreamHostCompoenent;
