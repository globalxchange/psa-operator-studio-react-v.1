import {message} from "antd";
import Axios from "axios";
import React, {useEffect} from "react";
import {CounselAppContext} from "../../../Context_Api/Context";
import {loginLoader} from "../../../static/images/images";
import StreamingView from "../Main-Component/StreamView";
import "../Main-Component/streamview.scss";

const StreamViewerComponent = (props) => {
  const context = React.useContext(CounselAppContext);
  const [flag, setFlag] = React.useState(true);
  useEffect(() => {
    functionCall();
  }, []);

  const pushFunction = async () => {
    setFlag(true);
    try {
      let res = await Axios.get(
        `https://ms.apimachine.com/api/getmeetings/${props.match.params.roomID}`
      );
      if (res.data.success) {
        context.setPresentMeeting(res.data.data);
        let obj = {
          Name: props.match.params.Name,
          email: props.match.params.emailID,
          room: props.match.params.roomID,
          presentMeeting: res.data.data,
        };

        let hosts = res.data.data.stream_hosts;
        console.log("hosts array- line 35" + res);

        let res2 = await Axios.get(
          `https://ms.apimachine.com/api/get_by_email/${props.match.params.emailID}`
        );
        if (res2.data.success) {
          let result = hosts.filter(
            (item) => item.id === res2.data.thefetch.psa_app_userId
          );

          if (result.length !== 0) {
            await context.getBrain();
            if (result[0].type === "host") {
              context.setHost(true);
              obj.coHosts = "host";
              context.setCohosts("host");
            } else {
              context.setHost(false);
              obj.coHosts = "co-host";
              obj.coHosts = "co-host";
              context.setCohosts("co-host");
            }
            let objectString = JSON.stringify(obj);
            let encodedString = btoa(objectString);
            localStorage.setItem("current_meeting", encodedString);
            localStorage.setItem(
              "CounselAppLoginAccount",
              props.match.params.emailID
            );
            props.history.push(`/hostlogin/${"streaming"}`);
            //redirect to login that guy
          } else {
            context.setCohosts("viewer");
            context.setHost(false);
            setFlag(false);
          }
        } else {
          let objectString = JSON.stringify(obj);
          let encodedString = btoa(objectString);
          localStorage.setItem("current_meeting", encodedString);

          context.setCohosts("viewer");
          context.setHost(false);
          setFlag(false);
        }
      } else {
        console.log("Could not find the Meeting");
        message.error("Could not find the Meeting");
      }
    } catch (E) {
      message.error(E.message);
      console.log(E);
    }
  };
  const functionCall = async () => {
    setFlag(true);
    if (props.match.params.logged === "logged") {
      let decodedString = atob(localStorage.getItem("current_meeting"));
      let decodedObj = JSON.parse(decodedString);
      console.log(decodedObj);
      if (decodedObj.room === props.match.params.roomID) {
        context.setPresentMeeting(decodedObj.meeting);
        context.setMeetingRoom(props.match.params.roomID);
        context.setName(props.match.params.Name);
        context.setEmail(props.match.params.emailID);
        context.setCohosts(decodedObj.coHosts);
        localStorage.setItem(
          "CounselAppLoginAccount",
          props.match.params.emailID
        );

        context.setProfileData(decodedObj.profileData);
      }
      console.log("came from login");
      setFlag(false);
      return;
    } else {
      console.log("in meeting room link");
      context.setMeetingRoom(props.match.params.roomID);
      context.setName(props.match.params.Name);
      localStorage.setItem(
        "CounselAppLoginAccount",
        props.match.params.emailID
      );
      context.setEmail(props.match.params.emailID);
      await pushFunction();
    }
  };
  return (
    <>
      {flag ? (
        <div
          style={{
            height: "100vh",
            width: "100vw",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={loginLoader}
            alt="Loader"
            style={{alignSelf: "center", height: "10vw"}}
          />
        </div>
      ) : (
        <div className="meeting-Main" style={{width: "100%"}}>
          <StreamingView />
        </div>
      )}
    </>
  );
};
export default StreamViewerComponent;
