import React, {useContext} from "react";
import {CounselAppContext} from "../../../../Context_Api/Context";
import {
  ellipsePink2,
  people,
  smiley,
  system,
  video,
} from "../../../../static/images/images";
import "../Streaming-Meeting.scss";

const StreamLiveMeetingFirst = () => {
  const context = useContext(CounselAppContext);
  return (
    <div className="div-1">
      <div className="div-top">
        <h3>
          <img src={smiley} alt="" />
          Suprise Stream
        </h3>
        <button
          onClick={(e) => {
            context.setStepStream("stream-live");
            context.setCurrentStreamDrawer("stream-live");
          }}
        >
          <img src={ellipsePink2} alt="" />
          Go Live now
        </button>
      </div>{" "}
      <p>Need to have a quick stream. No problem. Just call a #SupriseStream</p>
      <div>
        <button>
          {" "}
          <img src={video} alt="" />
          Video
        </button>
        <button>
          {" "}
          <img src={people} alt="" />
          Multi Participant
        </button>
        <button>
          {" "}
          <img src={system} alt="" />
          Auto Record
        </button>
      </div>
    </div>
  );
};
export default StreamLiveMeetingFirst;
