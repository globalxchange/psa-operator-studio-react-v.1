import React, {useContext, useState, useEffect} from "react";
import {
  calender,
  copySVG,
  editRoom,
  ellipse,
  ellipsePink2,
  insta,
  linkedin,
  loginLoader,
  logo,
  people,
  smiley,
  snap,
  system,
  twitter,
  video,
  youtube,
} from "../../../../static/images/images";
import "../Streaming-Meeting.scss";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {CounselAppContext} from "../../../../Context_Api/Context";
import Axios from "axios";
import {message, notification, Select} from "antd";
import {Link} from "react-router-dom";
import {Radio} from "antd";
import ioClient from "socket.io-client";

const {Option} = Select;

const StreamLiveMeetingSecond = (props) => {
  const context = useContext(CounselAppContext);
  const [flag, setFlag] = useState(true);
  const [streamType, setStreamType] = React.useState("Streaming_SC");

  const [name, setName] = useState("");
  const [diffRoom, setDiffRoom] = useState("");
  const [subDomain, setSubDomain] = useState(null);
  const [pulbicURL, setpulbicURL] = useState("");
  const [hostinfo, setHostInfo] = useState("");
  const newTab = {
    tabName: "Profile",
    detail: "new-tab",
    img: context.profilePic,
    dashboard: "New-Tab",
    type: "New-Tab",
    imgTab: context.profilePic,
  };
  const getDetails = async () => {
    setFlag(true);
    console.log("in get details");
    if (name === "") {
      setFlag(false);
      return;
    }
    try {
      console.log("in axios");
      let body = {
        meetingName: name,
        meetingCreatedBy: context.profileData.psa_app_userId,
        subdomain: context.subDomainData[0].subdomain,
        stream_type: streamType,
      };
      if (streamType !== "Streaming_SC") {
        if (hostinfo === "") {
          message.error("Please enter a valid host id");
          setFlag(false);
          return;
        } else {
          let res = await Axios.get(
            `https://ms.apimachine.com/api/get_by_email/${hostinfo}`
          );
          if (res.data.success) {
            let stream_hosts = [
              {type: "host", id: context.profileData.psa_app_userId},
              {type: "co-host", id: res.data.thefetch.psa_app_userId},
            ];
            let hostnames = [
              {
                id: context.profileData.psa_app_userId,
                name:
                  context.profileData.firstName + context.profileData.lastName,
              },
              {
                id: res.data.thefetch.psa_app_userId,
                name: res.data.thefetch.firstName + res.data.thefetch.lastName,
              },
            ];
            body.stream_hosts = stream_hosts;
            body.hostnames = hostnames;
          } else {
            message.error("Please Enter a Valid PSA Email ID");
          }
        }
      } else if (streamType === "Streaming_SC") {
        let stream_hosts = [
          {type: "host", id: context.profileData.psa_app_userId},
        ];
        let hostnames = [
          {
            id: context.profileData.psa_app_userId,
            name: context.profileData.firstName + context.profileData.lastName,
          },
        ];
        body.stream_hosts = stream_hosts;
        body.hostnames = hostnames;
      }
      let res = await Axios.post(
        "https://ms.apimachine.com/api/createmeeting",
        body,
        {
          headers: {
            email: context.email,
            token: context.token,
          },
        }
      );

      console.log("call complete");
      if (res.data.success) {
        context.setPresentMeeting(res.data.createdata);
        setDiffRoom(res.data.createdata.meetingId);
        setpulbicURL(
          `https://${context.subDomainData[0].subdomain}.pulse.stream/joinStreamingLink/?id=${res.data.createdata.meetingId}`
        );

        let commentSocket = ioClient(
          "https://testsockchatsio.globalxchange.io",
          {
            reconnection: false,
            query: {
              email: localStorage.getItem("CounselAppLoginAccount"),
              token: localStorage.getItem("CounselAppToken"),
              tokfortest: "nvestisgx",
            },
          },
          (res) => {
            console.log(res);
          }
        );
        console.log(commentSocket);

        commentSocket.emit(
          "create_stream_group",
          {
            group_name: res.data.createdata.meetingId,
            id: res.data.createdata.meetingId,
            timestamp: Date.now(),
          },
          {
            username: context.profileData.firstName,
            email: localStorage.getItem("CounselAppLoginAccount"),
          },
          "559af22d-5e79-4948-acf6-e48733ee1daa",
          (response) => {
            console.log(response);
            if (response.status === false) {
              message.error(response.payload);
            }
          }
        );

        context.setSecondStreamStep(1);
        setFlag(false);
      } else {
        console.log("somthing went wrong");
        message.error(res.data.message.checksubdomain.body.message);
        setFlag(false);
      }
    } catch (e) {
      console.log(e.message);
      message.error(e.message);
      setFlag(false);
    }
  };
  const updateDetails = async () => {
    setFlag(true);
    console.log("in update details");
    try {
      console.log("in axios");
      let body = {
        updatedMeetingId: diffRoom,
      };

      let res = await Axios.put(
        `https://ms.apimachine.com/api/updatemeeting/${context.presentMeeting.meetingId}`,
        body,
        {
          headers: {
            email: context.email,
            token: context.token,
          },
        }
      );
      console.log("call complete");
      if (res.data.success) {
        context.setPresentMeeting(res.data.data.updateMeeting);
        setDiffRoom(res.data.data.meetings.optionalMeetingId.updateMeeting);
        setpulbicURL(
          `https://${context.subDomainData[0].subdomain}.pulse.stream/joinStreamingLink/?id=${res.data.data.meetings.updateMeeting.optionalMeetingId}&roomID=${res.data.data.updateMeeting.meetings.meetingId}`
        );
        context.setSecondStreamStep(1);
        setFlag(false);
      } else {
        console.log("somthing went wrong");
        setFlag(false);
      }
    } catch (e) {
      console.log(e.message);
      message.error(e.message);
      setFlag(false);
    }
  };
  const mainFunction = () => {
    switch (context.secondtreamStep) {
      case 0:
        return (
          <>
            <h2>Name Your Stream</h2>
            <div className="copy-1">
              <img src={logo} alt="copybtn" />
              <input
                style={{
                  width: "100%",
                  background: "white",
                  border: "none",
                  borderLeft: "0.5px solid #dfdde2",
                  fontSize: "20px",
                  padding: "10px",
                  borderRadius: "0px",
                  outline: "none",
                  textAlign: "initial",
                }}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              ></input>
            </div>

            <br></br>
            <h2> Select Type of Stream</h2>
            <div>
              <Radio.Group
                onChange={(e) => {
                  setStreamType(e.target.value);
                }}
                value={streamType}
              >
                <Radio value={"Streaming_SS"}>Two Hosts (ScreenShares)</Radio>
                <Radio value={"Streaming_SC"}>Single Host</Radio>
                <Radio value={"Streaming_CC"}>Two Hosts (Cameras) </Radio>
                <Radio value={"Streaming_HH"}>Two Hosts (All) </Radio>
              </Radio.Group>
              {streamType !== "Streaming_SC" ? (
                <input
                  id="idInput"
                  placeholder="Please Enter Another Host email ID"
                  onChange={(e) => {
                    setHostInfo(e.target.value);
                  }}
                ></input>
              ) : (
                ""
              )}
            </div>
            <div className="buttons">
              <button
                style={{
                  backgroundColor: "#FE5777",
                  color: "white",
                  width: "40%",
                }}
                onClick={(e) => {
                  getDetails();
                }}
              >
                <img alt="" src={ellipse} />
                Create Stream
              </button>
            </div>
          </>
        );
      case 1:
        return (
          <>
            <h2>Share Your Stream Link With The World</h2>
            <div className="copy-1">
              <img src={logo} alt="" />
              <CopyToClipboard
                text={pulbicURL}
                onCopy={() => {
                  message.success("copied");
                }}
              >
                <button
                  style={{
                    border: "none",
                    color: "grey",
                    marginTop: "0px",
                    borderLeft: "0.5px solid #dfdde2",
                    fontSize: "0.7vw",
                    outline: "none",
                  }}
                  className="thelinkgen"
                >
                  <p>{pulbicURL}</p>
                  <img
                    src={copySVG}
                    alt="copybtn"
                    className="thecpybtn"
                    style={{marginRight: "0px", marginTop: "auto"}}
                  />
                </button>
              </CopyToClipboard>
            </div>
            <div className="buttons">
              <button
                onClick={(e) => {
                  context.setSecondStreamStep(2);
                }}
              >
                <img src={editRoom} alt="" />
                Edit
              </button>
              <Link tp="/main">
                <button
                  style={{backgroundColor: "#FE5777", color: "white"}}
                  onClick={(e) => {
                    let obj = {
                      id: context.presentMeeting.meetingId,
                      token: context.token,
                      email: context.email,
                      profileData: context.profileData,
                      name: context.profileName,
                      subDomainData: context.subDomainData,
                      presentMeeting: context.presentMeeting,
                      host: true,
                      meetingLink: pulbicURL,
                    };
                    let objectString = JSON.stringify(obj);
                    let encodedString = btoa(objectString);

                    window.open(
                      `https://${context.subDomainData[0].subdomain}.pulse.stream/joinStream/?id=${encodedString}`,
                      "_blank"
                    ); //to open new page
                  }}
                >
                  <img alt="" src={ellipse} />
                  Start Stream
                </button>
              </Link>
            </div>
          </>
        );
      case 2:
        return (
          <>
            <h2>Edit Your Stream Link </h2>
            <div className="copy-1">
              <Select
                style={{height: "100%"}}
                placeholder="Select "
                onChange={(e) => {
                  setSubDomain(e);
                }}
              >
                <Option value="sk" key="sk">
                  {context.presentMeeting.subdomain}
                </Option>
              </Select>
              <input readOnly disabled={true} value={"pulse.stream"}></input>

              <input
                value={diffRoom}
                onChange={(e) => {
                  setDiffRoom(e.target.value);
                }}
              ></input>
            </div>
            <div className="buttons">
              <button
                onClick={(e) => {
                  if (subDomain === "" || diffRoom === "") {
                    message.error(
                      "Please Make Sure You Enter Room ID and Select Sub Domain"
                    );
                  } else {
                    updateDetails();
                    context.setSecondStreamStep(1);
                  }
                }}
              >
                <img src={calender} alt="" />
                Save Link
              </button>
            </div>
          </>
        );
      default:
        break;
    }
  };
  useEffect(() => {
    getsubDomains();
  }, []);
  const getsubDomains = async () => {
    try {
      let res = await context.getSubDomain();
      if (res.data.success) {
        console.log(res.data.payload);
        context.setSubDomainData(res.data.payload);
      } else {
        if (res.data.message === "SubDomain List is unavalilable") {
          message.error("Please Register a Sub Domain");
          context.handleAddTab(newTab);
          context.setSecondStreamStep(0);
          context.setStepStream("base");
          context.setDrawerOpen(false);
          console.log("yes");
        } else {
          message.error(res.data.message);
        }
      }
      setFlag(false);
    } catch (e) {
      if (e.message === "SubDomain List is unavalilable") {
        setFlag(false);
      }
    }
  };
  return (
    <>
      <div className="div-1">
        <div className="div-top">
          <h3>
            <img src={smiley} alt="" />
            Suprise Stream
          </h3>
          <button>
            <img src={ellipsePink2} alt="" />
            Go Live now
          </button>
        </div>
        <p>
          Need to have a quick stream. No problem. Just call a #SupriseStream
        </p>
        <div>
          <button>
            <img src={video} alt="" />
            Video
          </button>
          <button>
            <img src={people} alt="" />
            Single Stream{" "}
          </button>
          <button>
            <img src={system} alt="" />
            Auto Record
          </button>
        </div>
      </div>
      <div className="div-2">
        {flag ? (
          <div
            style={{
              height: "5vh",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img
              src={loginLoader}
              alt="Loader"
              style={{alignSelf: "center", height: "5vh"}}
            />
          </div>
        ) : (
          <>
            <h3>#TheTimeIsNow</h3>
            {mainFunction()}
          </>
        )}
      </div>
      <div className="div-3">
        <p>Invite Your Guests Via These Platforms</p>
        <div>
          {buttons.map((item, i) => {
            return (
              <img style={{height: "50px"}} src={item} alt={item} key={i} />
            );
          })}
        </div>
      </div>
    </>
  );
};

const buttons = [insta, youtube, snap, linkedin, twitter];

export default StreamLiveMeetingSecond;
