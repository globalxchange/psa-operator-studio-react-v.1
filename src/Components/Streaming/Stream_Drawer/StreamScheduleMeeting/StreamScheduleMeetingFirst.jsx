import React from 'react';
import {
  calender,
  people,
  smiley,
  system,
  video,
} from '../../../../static/images/images';
import '../Streaming-Meeting.scss';
const StreamScheduleMeetingFirst = () => {
  return (
    <div className='div-1'>
      <div className='div-top'>
        <h3>
          <img src={smiley} alt='' />
          Arrange Meeting
        </h3>
        <button>
          <img src={calender} alt='' />
          Schedule now
        </button>
      </div>
      <p>
        Got some important matters to discuss? Great, let us get your meeting
        room ready.
      </p>
      <div>
        <button>
          <img src={video} alt='' />
          Video
        </button>
        <button>
          <img src={people} alt='' />
          Multi Participant
        </button>
        <button>
          <img src={system} alt='' />
          Auto Record
        </button>
      </div>
    </div>
  );
};
export default StreamScheduleMeetingFirst;
