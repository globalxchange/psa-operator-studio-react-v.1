import React, { useContext, useState } from "react";
import { CounselAppContext } from "../../../Context_Api/Context";
import { arrow } from "../../../static/images/images";
import "./Streaming-Meeting.scss";
import StreamLiveMeetingFirst from "./StreamLiveMeeting/StreamLiveMeetingFirst";
import StreamLiveMeetingSecond from "./StreamLiveMeeting/StreamLiveMeetingSecond";
import StreamScheduleMeetingFirst from "./StreamScheduleMeeting/StreamScheduleMeetingFirst";
const StreamDrawer = () => {
  const context = useContext(CounselAppContext);
  const mainfunction = () => {
    switch (context.stepStream) {
      case "stream-base":
        return (
          <>
            <StreamLiveMeetingFirst />
            <br />
            <StreamScheduleMeetingFirst />
          </>
        );
      case "stream-live":
        return <StreamLiveMeetingSecond />;
      case "stream-schedule":
        return <StreamScheduleMeetingFirst />;
      case "multiple":
        break;
      // return <CustomStreamDrawer />;
      default:
        break;
    }
  };
  return (
    <div className="meetings-drawer-configure">
      <div className="meetings-heading-configure">
        <img
          src={arrow}
          alt=""
          onClick={(e) => {
            if (context.currentStreamDrawer === "stream-live") {
              if (context.secondtreamStep === 1) {
                context.setSecondStreamStep(0);
              } else if (context.secondtreamStep === 2) {
                context.setSecondStreamStep(1);
              } else {
                context.setStepStream("stream-base");
                context.setCurrentStreamDrawer("stream-base");
              }
            } else if (context.currentStreamDrawer === "stream-schedule") {
              context.setCurrentStreamDrawer("stream-schedule");
              context.setStepStream("stream-base");
            } else if (context.currentStreamDrawer === "stream-base") {
              context.setDrawerOpen(false);
            }
          }}
        />
        <h3>Stream Configuration</h3>
      </div>
      <div className="meetings-body-configure">{mainfunction()}</div>
    </div>
  );
};

export default StreamDrawer;
