import {Radio} from "antd";
import {Modal} from "antd";
import React, {useContext} from "react";
import {CounselAppContext} from "../../../Context_Api/Context";
const StreamModalSingle = React.memo(
  ({visible, setVisible, setModalClick, type}) => {
    const {
      audioTypes,
      finalVideoType,
      finalAudioType,
      setFinalAudioType,
      videoTypes,
      setFinalVideoType,
    } = useContext(CounselAppContext);
    return (
      <Modal
        title="Device Selection"
        visible={visible}
        onOk={(e) => {
          if (finalAudioType === null || finalVideoType === null) {
            return;
          } else {
            setVisible(false);
            setModalClick(true);
          }
        }}
      >
        {type === "Streaming_SS" ? (
          ""
        ) : (
          <div>
            <p>Select Video Source</p>
            <div>
              <Radio.Group
                onChange={(e) => {
                  console.log(
                    "---------------video source------------------" +
                      e.target.value
                  );
                  setFinalVideoType(e.target.value);
                }}
              >
                {videoTypes.map((item) => {
                  return (
                    <Radio value={item.key} key={item.key}>
                      {item.name}
                    </Radio>
                  );
                })}
              </Radio.Group>
            </div>
          </div>
        )}
        <br></br>
        <div>
          <p>Select Audio Source</p>
          <div>
            <Radio.Group
              onChange={(e) => {
                console.log(
                  "---------------audio source------------------" +
                    e.target.value
                );

                setFinalAudioType(e.target.value);
              }}
            >
              {audioTypes.map((item) => {
                return (
                  <Radio key={item} value={item.key}>
                    {item.name}
                  </Radio>
                );
              })}
            </Radio.Group>
          </div>
        </div>
      </Modal>
    );
  }
);
export default StreamModalSingle;
