import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './pages/Login/Login';
import Home from './pages/Home/Home';
import Landing from './pages/Landing/Landing';
import MarketPlaceOne from './pages/Home/MarketPlaceOne';
import ManagementOne from './pages/Home/ManagementOne';
import MyAppsOne from './pages/Home/MyAppsOne';
import Marketing from "./pages/Home/Marketing";
import CRM from "./pages/Home/CRM";
import ITndData from "./pages/Home/ITndData";
import Admin from "./pages/Home/Admin";
import CryptoLaw from "./pages/Home/CryptoLaw";
import MyCounsel from "./pages/Home/MyCounsel";
import ProCounsel from "./pages/Home/ProCounsel";
import Regulator from "./pages/Home/Regulator";
import CustomerRelations from "./pages/Home/CustomerRelations";
import Management from "./pages/Home/Managment";
import AddOns from './pages/Home/AddOns';
import MediaSoup from "./pages/Home/MediaSoup";
// import MeetingRoom from "./pages/MeetingRoom/MeetingRoom"
import EnterEmail from "./pages/Register/EmailEnter";
import EnterUsername from "./pages/Register/EnterUsername"
// import GenerateLink from "./pages/MeetingRoom/GenerateLink"
// import MeetingLink from "./pages/MeetingRoom/MeetingLink";
import UserAccept from "./pages/Register/UserAccept"
import ImageUpload from "./pages/Register/ImageUpload"
import SelectApp from "./pages/Apps/selectApp"
import Trade from "./pages/Apps/trade"
import Gamer from "./pages/Apps/gamer"

function App() {
  return (
    <Switch>
      <Route exact path="/home" component={Home} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/landing" component={Landing} />
      <Route exact path="/appconfig" component={MarketPlaceOne} />
      <Route exact path="/streamacademy" component={ManagementOne} />
      <Route exact path="/myapps1" component={MyAppsOne} />
      <Route exact path="/analytics" component={Marketing}/>
      <Route exact path="/vaults" component={CRM}/>
      <Route exact path = "/documentation" component = {ITndData}/>
      <Route exact path = "/support" component = {Admin}/>
      <Route exact path = "/cryptolaw" component = {CryptoLaw}/>
      <Route exact path = "/mycounsel" component = {MyCounsel}/>
      <Route exact path = "/procounsel" component = {ProCounsel}/>
      <Route exact path = "/regulator" component = {Regulator}/>
      <Route exact path = "/manage" component = {CustomerRelations}/>
      <Route exact path = "/devtools" component = {Management}/>
      <Route exact path = "/pulseapps" component = {AddOns}/>
      <Route exact path = "/mediasoup" component = {MediaSoup}/>
      {/* <Route exact path = "/meetingroom" component = {MeetingRoom}/> */}
      <Route exact path = "/enteremail" component = {EnterEmail}/>
      <Route exact path = "/enterusername" component = {EnterUsername}/>
      <Route exact path = "/useraccept" component = {UserAccept}/>
      <Route exact path = "/imageupload" component = {ImageUpload}/>
      <Route exact path = "/" component = {SelectApp}/>
      <Route exact path = "/tradestream" component = {Trade}/>
      <Route exact path = "/gamerstream" component = {Gamer}/>
      {/* <Route exact path = "/generatelink" component = {GenerateLink}/> */}
      {/* <Route exact path = "/meetinglink" component = {MeetingLink}/> */}
    </Switch>
  );
}

export default App;
