import addPeople from "./Add.svg";
import cameraOff from "./CameraOff.svg";
import chatPeople from "./Chat.svg";
import copy from "./copy.svg";
import exit from "./out.svg";
import gx from "./Gx_Nvest.svg";
import micOff from "./Union.svg";
import screenShare from "./ScreenShare.svg";
import loadingGIF from "./6.gif";
import slabOne from "./slab-one.svg";
import slabSmall from "./small-slab.svg";
import slabTwo from "./slab-two.svg";
import arrow from "./arrow.svg";
import youtube from "./social/youtube.svg";
import snap from "./social/snap.svg";
import linkedin from "./social/linkedin.svg";
import twitter from "./social/twitter.svg";
import insta from "./social/insta.svg";
import copySVG from "./copysvg.svg";
import ellipse from "./ellipse 5.svg";
import editRoom from "./editRoom.svg";
import smiley from "./smiley.svg";
import rocket from "./rocket.svg";
import dots from "./navbar-icons/more.svg";
import system from "./system.svg";
import people from "./people.svg";
import video from "./video.svg";
import calender from "./calender2.svg";
import ellipsePink2 from "./Ellipsepink2.png";
import greenPlus from "./greenPlus.svg";
import send from "./send.svg";
import recordIcon from "./button.svg";
import loginLoader from "./LoginLoader.gif";
import logo from "../images/sidebar-icons/meetingstrmlogo.svg";
import micOn from "./video-icons/microphone (1).svg";
import MicOff from "./video-icons/microphone.svg";
import VideoOn from "./video-icons/video-camera.svg";
import VideoOff from "./video-icons/no-video.svg";
import CopyLoad from "./video-icons/copy.svg";
import chatBubbleOn from "./video-icons/speech-bubble.svg";
import chatBubbleOff from "./video-icons/no-chatting.svg";
import ScreenShare from "./video-icons/monitor.svg";
import LogOut from "./video-icons/logout.svg";
import People from "./video-icons/customer.svg";
import Vector from "./Vector.svg";
import stop from "./stop.svg";
import PulseImage from "./PulseLoginImage.png";
import BlackPlus from "./blackPlus.svg";
import deskStreamlogo from "./deskstream.svg";
import deskstreamBlock from "./Shape - Rectangle (Top Part Of Header).svg";
import streamTriangle from "./streamtriangle.svg";
import folder from "./folder.svg";

import videoImage from "./brainImages/video-camera.svg";
import audioImage from "./brainImages/audiobook.svg";
import pdfImage from "./brainImages/pdf.svg";
import powerpoint from "./brainImages/powerpoint.svg";
import unknown from "./brainImages/unknown.svg";
import excel from "./brainImages/excel.svg";
import compressed from "./brainImages/zip.svg";
import document from "./brainImages/document.svg";
import imageImage from "./brainImages/image.svg";
import userposter from "./userposter.png";
import screenposter from "./screenposter.png";

export {
  userposter,
  screenposter,
  videoImage,
  audioImage,
  pdfImage,
  powerpoint,
  unknown,
  excel,
  compressed,
  document,
  imageImage,
  micOn,
  folder,
  deskstreamBlock,
  deskStreamlogo,
  PulseImage,
  MicOff,
  stop,
  VideoOff,
  VideoOn,
  streamTriangle,
  CopyLoad,
  chatBubbleOff,
  chatBubbleOn,
  ScreenShare,
  LogOut,
  People,
  BlackPlus,
  logo,
  ellipsePink2,
  greenPlus,
  calender,
  snap,
  video,
  dots,
  Vector,
  people,
  loginLoader,
  system,
  gx,
  ellipse,
  send,
  smiley,
  editRoom,
  rocket,
  copySVG,
  insta,
  recordIcon,
  youtube,
  twitter,
  linkedin,
  addPeople,
  cameraOff,
  chatPeople,
  screenShare,
  arrow,
  micOff,
  exit,
  copy,
  loadingGIF,
  slabOne,
  slabSmall,
  slabTwo,
};
