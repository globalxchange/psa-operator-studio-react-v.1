import LxSVG from './LxSVG.svg';
import bankerSVG from './bankerSVG.svg';
import tellerSVG from './tellerSVG.svg';
import atmSVG from "./atmSVG.svg"
export {
    LxSVG,
    bankerSVG,
    tellerSVG,
    atmSVG
}


