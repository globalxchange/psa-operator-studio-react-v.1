import React from "react";
import "srcstaticimagesCounselLogo.svg";
// import CounselLogo from '../../static/images/CounselLogo.svg';
import PulseImage from "../../static/images/pulseMain.png";
import {Link} from "react-router-dom";
import "./Landing.scss";

function Landing() {
  return (
    <div className="login-ag">
      <div className="wrapper">
        <img src={PulseImage} alt="no_img" />
        <Link to="/login" className="login-btn-ag">
          Access Studios
        </Link>
        <Link to="/enteremail" className="get-srt-btn">
          Create Account
        </Link>
      </div>
      {/* <div className="powered-by">
        <h5>P O W E R E D &nbsp;&nbsp; B Y</h5>
        <img src={GxNvest} alt="no_img" />
      </div> */}
    </div>
  );
}

export default Landing;
