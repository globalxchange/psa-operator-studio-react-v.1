import React, { useState, useEffect, useContext } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import axios from "axios"
import "../Home/pages.scss"
import { message, Modal, Button } from 'antd';
import { Row, Col } from 'reactstrap';
import { faCross, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CounselAppContext } from '../../Context_Api/Context';


function FetchPersonalities(props) {
    const context = useContext(CounselAppContext);
    const [getpersonalities, setpersonalities] = useState([])

    // const theget = () => {
      
    // }
    useEffect(() => {
        const personalities = message.loading("getting personalities")
        axios.get('https://ms.apimachine.com/api/getallpersonalitiesbyuser?email=' + context.email, {
            headers: {
                email: context.email,
                token: context.token,
            }
        }).then(response => {
            if (response.data.success) {
                setTimeout(personalities, 100);
                setpersonalities(response.data.fetch)
            } else {
                message.error(response.data.message)
            }
        }).catch(err => console.log(err))
    }, [])
    console.log(JSON.stringify(getpersonalities))
    if(getpersonalities.length === 0){
       return <div>
           <img src={Arrow} alt="arrow" style={{ cursor: "pointer", marginTop: "-6px" }} onClick={() => props.onClose11()} />
        &nbsp;&nbsp;&nbsp;<span className="add-style" >View Personalities</span><br />
        <h1>Personality not found</h1>
       </div>
    }
    return <div>
        <img src={Arrow} alt="arrow" style={{ cursor: "pointer", marginTop: "-6px" }} onClick={() => props.onClose11()} />
                    &nbsp;&nbsp;&nbsp;<span className="add-style" >View Personalities</span><br />
        {getpersonalities.map(element1 => {

            return <div className="crd-style" style={{ marginTop: "10px" }}>
                <img className="the-img-sty" style = {{marginTop:"10px"}} src={element1.icon} alt="icon" />
                <Row>
                    <Col sm="5">
                        <p className="thekeysty1 ">Name Of Personality</p>
                    </Col>
                    <Col sm="1" style={{ marginLeft: "25px" }}>:</Col>
                    <Col sm="5">
                        <p>{element1.nameofpersonality}</p>
                    </Col>
                </Row>
                <Row>
                    <Col sm="5">
                        <p className="thekeysty1 ">Description</p>
                    </Col>
                    <Col sm="1" style={{ marginLeft: "25px" }}>:</Col>
                    <Col sm="5">
                        <p>{element1.description}</p>
                    </Col>
                </Row>
                {element1.personalities.map(element11=>{
                 return <Row>
                    <Col sm="5">
                        <p className="thekeysty1 ">{element11.key}</p>
                    </Col>
                    <Col sm="1" style={{ marginLeft: "25px" }}>:</Col>
                    <Col sm="5">
                        <p>{element11.value}</p>
                    </Col>
                </Row>
                })}
            </div>
        })}
    </div>
}

export default FetchPersonalities;

