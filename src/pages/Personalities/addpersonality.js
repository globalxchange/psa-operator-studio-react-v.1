import React, { useState, useEffect,useContext } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import axios from "axios"
import message from 'antd/lib/message';
import jwt from "jsonwebtoken";
import TextArea from 'antd/lib/input/TextArea';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Form, Input, Button, Space } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { CounselAppContext } from '../../Context_Api/Context';




function AddPersonality(props) {
    const context = useContext(CounselAppContext);
    const [personality, setpersonality] = useState("")
    const [profilephoto, setprofilephoto] = useState("")
    const [description, setdescription] = useState("")
    const [thefile, setfile] = useState("none")
    const [loading, setloading] = useState(false)




    const inputchange = (e) => {
        if (e.target.name === "personality") {
            setpersonality(e.target.value)
        } else if (e.target.name === "description") {
            setdescription(e.target.value)
        } else {
            message.error("invalid input value")
        }
    }

    let handleChange = async (e) => {
        e.preventDefault()
        if (e.target.files[0] === undefined) {
            message.error("Please upload files")
        } else {
            let filename = e.target.files[0].name
            console.log(e.target.files[0])
            console.log("name-file " + filename)
            let name = e.target.name
            console.log("name " + name)

            let selected_file = e.target.files[0]
            // let fileName = e.target.files[0].name
            let fileext = e.target.files[0].type
            console.log("file-type " + fileext)
            //code starts here
            if (fileext.substring(0, 5) === "image") {
                const formData = new FormData();
                formData.append("files", selected_file)
                const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
                const email = "ram@nvestbank.com"; //email of the developer.
                const path_inside_brain = "root/CounselAppImages/";
                let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
                console.log(fileName)
                const token = jwt.sign({ name: fileName, email: email }, secret, {
                    algorithm: "HS512",
                    expiresIn: 240,
                    issuer: "gxjwtenchs512",
                });

                console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
                let response = await axios.post(
                    `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                    formData,
                    {
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                        },
                    }
                );
                if (response.data.status) {
                    setfile("block")
                    setprofilephoto(response.data.payload.url)
                } else {
                    message.error(response.data.payload)
                }
                console.log(JSON.stringify(response.data) + " image-upload");
                console.log(selected_file.name + " imagename")

            } else {
                message.error("please upload only images")
            }
        }
    }

    const onFinish = values => {
        console.log('personality:', values.personality);
        console.log('array of persoanlities:', values.users);
        console.log('profile photo:', profilephoto)
        console.log('description: ', values.description)
        



        let body = {
            nameofpersonality: values.personality,
            icon:profilephoto,
            description: values.description,
            personalities: values.users
        }
        console.log(JSON.stringify(body) + "thebody")
        axios.post('https://ms.apimachine.com/api/createpersonality',body,  {
            headers: {
                email: context.email,
                token: context.token,
            }}).then(res=>{
                if(res.data.success){
                    message.success("Personality added")
                }else{
                    message.error(res.data.message)
                }
    
        })
    };

        // console.log(personality, description, profilephoto)
        // let body = {
        //     "NameOfUserType": personality,
        //     "Icon": profilephoto,
        //     "Description": description,
        // }
        // axios.post('https://ms.apimachine.com/api/createusertype/5fa42a94b872e706309874a7', body).then(res => {
        //     if (res.data.success) {
        //         message.success("User Type added")
        //         setusertype("")
        //         setprofilephoto("")
        //         setdescription("")
        //         setPrerequsiteUserType("")
        //         theget()
        //     }else{
        //         message.error(res.data.message)
        //     }
        // }).catch(err => console.log(err))

    return <div>
      <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose11()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Add Personality</span>
        </div>
        <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
            <div className="crd-style">
                <p className="adjust-title">Name Of Personality</p>
                <Form.Item
                    name="personality"
                    rules={[{ required: true, message: 'Please enter the personality name!' }]}
                    className="thetext-styy1 namewidth"
                >
                    <input type="text" name="personality" value={personality} onChange={inputchange} className="thetext-styy1 namewidth" placeholder="Ex: business" required />
                </Form.Item>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Upload Icon</p><br />
                <Form.Item
                    name="profile"
                    className="thetext-styy1 namewidth"
                >
                    <input type="file" className="theimage-styy" name="profile" onChange={handleChange} required/>
                    <img src={profilephoto} style={{ display: thefile }} className=" img-styeee" alt="icon" />                    </Form.Item>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Description</p>
                <Form.Item
                    name="description"
                    rules={[{ required: true, message: 'Please enter description!' }]}
                    className="thetext-styy1 textarea-width">
                    <TextArea style={{ marginLeft: "25px", marginTop: "10px" }} id="address" name="description" value={description} onChange={inputchange} rows="4" cols="50" required></TextArea>
                </Form.Item>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Personalities</p>
                <Form.List name="users">
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(field => (
                                <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item style={{ width: "90%", marginLeft: "25px", marginTop: "10px" }}
                                        {...field}
                                        name={[field.name, 'key']}
                                        fieldKey={[field.fieldKey, 'key']}
                                        rules={[{ required: true, message: 'Missing key' }]}
                                    >
                                        <Input placeholder="Key" />
                                    </Form.Item>
                                    <Form.Item
                                        {...field}
                                        name={[field.name, 'value']}
                                        fieldKey={[field.fieldKey, 'value']}
                                        rules={[{ required: true, message: 'Missing value' }]}
                                    >
                                        <Input placeholder="Value" />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button style={{ width: "90%", marginLeft: "25px", marginTop: "10px" }} type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add field
              </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
            </div>
            <Form.Item>
                <button type="submit"
                    className="footer"
                    disabled={loading}>
                    {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Submit"}
                </button>
            </Form.Item>
        </Form>
  );
      {/* <button type="submit"
            className="footer"
            disabled={loading}
        >
        </button> */}

    </div>
}

export default AddPersonality;
