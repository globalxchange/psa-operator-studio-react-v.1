import React from 'react';
import 'srcstaticimagesCounselLogo.svg';
import MeetingImage from '../../static/images/MainImage.svg'
import { Link } from 'react-router-dom';
import "./meeting.scss"

function GenerateLink() {
    return <div className="meeting-ag">
        <div className="wrapper">
            <img src={MeetingImage} alt="no_img" />
            <p className="titletext">What Password Do You Want To Set? </p>
            <input type="text" className="theinput" placeholder="Ex. Board Meeting With Alex" />
            <Link to="/meetinglink" className="join-srt-btn">
            Generate Link 
        </Link>
        </div>
    </div>
}

export default GenerateLink;
