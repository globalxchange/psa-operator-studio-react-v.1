import React, { useState } from 'react';
import 'srcstaticimagesCounselLogo.svg';
import PulseImage from '../../static/images/PulseLoginImage.png'
import PulseTitleImg from "../../static/images/PulseTabIcon.png"
import { Link } from 'react-router-dom';
import "./meeting.scss"
import uploadImageIcon from "../../static/images/uploadImageIcon.png"
import message from 'antd/lib/message';
import axios from "axios";
import jwt from "jsonwebtoken";
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function ImageUpload(props) {
    const [profilephoto, setprofilephoto] = useState(uploadImageIcon)
    const [thedisplay, setdisplay] = useState('none')
    const [loading,setLoading] = useState(false)

    // Create a reference to the hidden file input element
    const hiddenFileInput = React.useRef(null);

    // Programatically click the hidden file input element
    // when the Button component is clicked
    const handleClick = () => {
        hiddenFileInput.current.click();
        console.log(props.history)
    };

    const handleChange = async (event) => {
        event.preventDefault()
        const fileUploaded = event.target.files[0];
        console.log(fileUploaded)
        if (event.target.files[0] === undefined) {
            message.success("Image not changed")
        } else {
            let filename = event.target.files[0].name
            const formData = new FormData();
            let selected_file = event.target.files[0]
            formData.append("files", selected_file)
            const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
            const email = "ram@nvestbank.com"; //email of the developer.
            const path_inside_brain = "root/LxOperatorImages/";
            let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
            console.log(fileName)
            const token = jwt.sign({ name: fileName, email: email }, secret, {
                algorithm: "HS512",
                expiresIn: 240,
                issuer: "gxjwtenchs512",
            });

            console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
            let response = await axios.post(
                `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                formData,
                {
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                    },
                }
            );
            console.log(JSON.stringify(response.data) + " image-upload");
            if (response.data.status) {
                setprofilephoto(response.data.payload.url)
                setdisplay('block')
            }
        }
    }
    const submitdata = (e)=>{
        e.preventDefault()
        setLoading(true)
        let body = {
            // "email":props.history.location.data[1],
            // "image_url":profilephoto,
            // "username":props.history.location.data[0]
            "email":localStorage.getItem('themailenter'),
            "image_url":profilephoto,
            "username":localStorage.getItem('theuser')
        }
        // localStorage.clear()
        console.log(JSON.stringify(body))
        axios.post('https://testpsabackend.globalxchange.io/register', body)
        .then((response) => {
            const { data } = response;
            if (data.status) {
                setLoading(false)
                message.success("Operator Registration Success! Please Login")
                localStorage.clear()
                props.history.push('/landing')
            }else{
                setLoading(false)
                message.error(data.payload)
            }
        })
    }
    return (
        <div className="meeting-ag">
            <div className="center-align">
                <img src={PulseImage} alt="no_img" />
                <p className="titletext">Upload A Profile Picture</p>
                <div className = "thebtuns">
                <button onClick={handleClick} className="btnstyle2">
                    <img className="imgsty2" src={profilephoto} alt="uploadimage" />
                </button>
                <form onSubmit = {submitdata}>
                <input type="file" ref={hiddenFileInput}
                    onChange={handleChange} style={{ display: "none" }}></input>
                <button className="meeting-btn-ag2 nextbtnalig" style = {{display:thedisplay}}>
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'Next'}

        </button>
        </form>
        </div>
                {/* <input type="text" className="theinput1 alig12" placeholder=" Ex. shorupan123 " value = "Shorupan123" />
                <Link to="/meetinglink" className="meeting-btn-ag2">
                    Check Availability
        </Link>
                 */}
            </div>
        </div>
    );
}
export default ImageUpload;
