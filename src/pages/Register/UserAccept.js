import React from 'react';
import 'srcstaticimagesCounselLogo.svg';
import PulseImage from '../../static/images/PulseLoginImage.png'
import { Link } from 'react-router-dom';
import "./meeting.scss"


function UserAccept(props) {

    const nextbtn = (e)=>{
        e.preventDefault()
        props.history.push({pathname:"/imageupload",data:props.history.location.data})
        console.log(props.history)
    }
    let thename
    if(props.history.location.data === undefined){
        thename = localStorage.getItem('theuser')
    }else{
        thename = props.history.location.data[0]
    }
    return (
        <div className="meeting-ag">
            <div className="center-align">
                <img src={PulseImage} alt="no_img" />
                <form onSubmit = {nextbtn}>
                <p className="titletext">Great Your Username Is Available</p>
                <input type="text" className="theinput1 alig12" placeholder=" Ex. shorupan123 " value = {thename} />
                <button className="meeting-btn-ag2">
                Next
        </button>
        </form>              
            </div>
        </div>
    );
}

export default UserAccept;
