import React from 'react';
import 'srcstaticimagesCounselLogo.svg';
import MeetingImage from '../../static/images/MainImage.svg'
import { Link } from 'react-router-dom';


function MeetingRoom() {
    return (
        <div className="login-ag">
            <div className="wrapper">
                <img src={MeetingImage} alt="no_img" />
                <Link to="/startmeeting" className="login-btn-ag">
                    Start A Meeting
        </Link>
                <Link to="/meetingroom" className="get-srt-btn">
                    Join A Meeting
        </Link>
            </div>
        </div>
    );
}

export default MeetingRoom;
