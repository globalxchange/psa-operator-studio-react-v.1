import React, { useState } from 'react';
import 'srcstaticimagesCounselLogo.svg';
import PulseImage from '../../static/images/PulseLoginImage.png'
import { Link } from 'react-router-dom';
import "./meeting.scss"
import message from 'antd/lib/message';
import axios from "axios"
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';



function EmailEnter(props) {
  const [email, setemail] = useState(localStorage.getItem('themailenter') || '')
  const [loading, setLoading] = useState(false);

  const nextpage = (e) => {
    e.preventDefault()
    setLoading(true)
    console.log(email)
    localStorage.setItem('themailenter', email)
    if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email)) {
      axios.get(
        `https://comms.globalxchange.com/user/details/get?email=${email}`
      ).then((res) => {
        if (res.data.status) {
          setLoading(false)
          props.history.push({ pathname: '/enterusername', data: email })
        } else {
          setLoading(false)
          message.error(res.data.message)
        }
      })
    } else {
      setLoading(false)
      message.error('please enter valid email')
    }
  }
  return (
    <div className="meeting-ag">
      <div className="center-align">
        <img src={PulseImage} alt="no_img" />
        <p className="titletext"> Enter A GX Account Email</p>
        <form onSubmit={nextpage}>
          <input type="text" className="theinput1 alig12" value={email} onChange={(e) => setemail(e.target.value)} placeholder=" Ex. shorupan@gmail.com" required />
          <button className="meeting-btn-ag2">
            {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'Next'}
          </button>
        </form>

      </div>
      <div className="customize-link1">
        <h5> Don’t Have GXAccount?&nbsp;<a href="https://globalxchange.com/" rel="noopener noreferrer" target="_blank" className="under-line">Click Here</a></h5>
      </div>
    </div>
  );
}

export default EmailEnter;
