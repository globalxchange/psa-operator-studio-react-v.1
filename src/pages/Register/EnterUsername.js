import React, { useState } from 'react';
import 'srcstaticimagesCounselLogo.svg';
import PulseImage from '../../static/images/PulseLoginImage.png'
import { Link } from 'react-router-dom';
import "./meeting.scss";
import axios from "axios"
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';



function EnterUsername(props) {
    const [name, setname] = useState( localStorage.getItem('theuser') || '')
    const [loading, setLoading] = useState(false);
    // const [email, setemail] = useState('')
    const [titletext, settitletext] = useState('Please Select A Pulse Operator Username')

    const thevalue = (e)=>{
        e.preventDefault()
        setLoading(true)
        console.log(name)
        localStorage.setItem('theuser',name)
        console.log(props.history.location.data)
        axios.post('https://testpsabackend.globalxchange.io/register', {
          username: name,
          email: props.history.location.data,
          username_check: true,
        })
        .then((response) => {
            const { data } = response;
            if (data.status) {
                setLoading(false)
                setTimeout(() => {
                    props.history.push({ pathname: '/useraccept',data:[name,props.history.location.data]})
                  }, 1000);           
            }else{
                setLoading(false)
                settitletext(data.payload) 
                console.log(localStorage.getItem('themailenter') + "thelocal")
            }
        })
        // if(name === "Shorupan123"){
        //     props.history.push({ pathname: '/useraccept',data:[name,email]})
        // }else{
        //     settitletext('Sorry Your Username Is Already Taken. Please Try Another One ')
        //     console.log(props.history.location.data)
        // }
    }
    return (
        <div className="meeting-ag">
            <div className="center-align">
                <img src={PulseImage} alt="no_img" />
                <p className="titletext">{titletext}</p>
                <form onSubmit = {thevalue}>
                <input type="text" className="theinput1 alig12" placeholder=" Ex. shorupan123 " value = {name} onChange={(e) => setname(e.target.value)}/>
                <button className="meeting-btn-ag2">
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'Check Availability'}
        </button>
                </form>
            </div>
        </div>
    );
}

export default EnterUsername;
