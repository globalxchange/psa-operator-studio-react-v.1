import React from 'react';
import 'srcstaticimagesCounselLogo.svg';
import MeetingImage from '../../static/images/MainImage.svg'
import { Link } from 'react-router-dom';
import "./meeting.scss"
import CopyBtn from "../../static/images/copybtn.png"
import message from 'antd/lib/message';


const cpylink = (e)=>{
    message.success('Link copied to clipboard')
}
function MeetingLink() {
    return <div className="meeting-ag">
        <div className="wrapper">
            <img src={MeetingImage} alt="no_img" />
            <p className="titletext">Here Is Your Meeting Room</p>
            <button className="thelinkgen" onClick = {cpylink}>meetings.stream/123456 <img src={CopyBtn} alt="copybtn" className="thecpybtn" /></button>

            <Link to="/meetinglink" className="join-srt-btn">
                Enter Meeting
  </Link>
            <div className="customize-link">
                <h5><Link className="under-line">Click Here</Link> To Customize Your Link</h5>
            </div>
        </div>
    </div>
}

export default MeetingLink;
