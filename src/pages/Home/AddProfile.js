import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faBowlingBall } from '@fortawesome/free-solid-svg-icons'
import axios from "axios";
import jwt from "jsonwebtoken";
import message from 'antd/lib/message';
import { Select } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
const { Option } = Select;


function AddProfile(props) {
    const [profilephoto, setprofilephoto] = useState("")
    const [coverphoto, setcoverphoto] = useState("")
    const [thefile, setfile] = useState("none")
    const [coverfile, setcoverfile] = useState("none")
    const [countries, setcountries] = useState([])
    const [states, setstates] = useState([])
    const [passport, setpassport] = useState("")
    const [loading, setloading] = useState(false)
    const [birthcertificate, setbirthcertificate] = useState("")
    const [license, setlicense] = useState("")
    const [socialinsurancenumber, setsocialinsurancenumber] = useState("")
    const [criminalbackgroundcheck, setcriminalbackgroundcheck] = useState("")
    const [firstname, setfirstname] = useState("")
    const [lastname, setlastname] = useState("")
    const [address, setaddress] = useState("")
    const [countryname, setcountryname] = useState("")
    const [statename, setstatename] = useState("")
    const [phonenumber, setphonenumber] = useState("")
    const [occupation, setoccupation] = useState("")
    const [companyname, setcompanyname] = useState("")
    const [companynumber, setcompanynumber] = useState("")
    const [companyaddress, setcompanyaddress] = useState("")
    const [workexperience, setworkexperience] = useState("")




    useEffect(() => {
        axios.get('https://counsel.apimachine.com/api/getallcountries')
            .then(response => {
                if (response.data.success) {
                    setcountries(response.data.data)
                } else {
                    message.error(response.data.message)
                }
            }).catch(err => console.log(err))
    }, [])

    // console.log(JSON.stringify(countries) + " thecountries")
    let handleChange = async (e) => {
        e.preventDefault()
        if(e.target.files[0] === undefined){
            message.error("Please upload files")
        }else{
        let filename = e.target.files[0].name
        console.log(e.target.files[0])    
        console.log("name-file " + filename)
        let name = e.target.name
        console.log("name " + name)
        
        let selected_file = e.target.files[0]
        // let fileName = e.target.files[0].name
        let fileext = e.target.files[0].type
        console.log("file-type " + fileext)
        //code starts here
        if (name === "profile" || name === "cover") {
            if (fileext.substring(0, 5) === "image") {
                const formData = new FormData();
                formData.append("files", selected_file)
                const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
                const email = "ram@nvestbank.com"; //email of the developer.
                const path_inside_brain = "root/CounselAppImages/";
                let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
                console.log(fileName)
                const token = jwt.sign({ name: fileName, email: email }, secret, {
                    algorithm: "HS512",
                    expiresIn: 240,
                    issuer: "gxjwtenchs512",
                });

                console.log(/[^.]+$/.exec(filename)+ " name-of-the-file")
                let response = await axios.post(
                    `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                    formData,
                    {
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                        },
                    }
                );

                console.log(JSON.stringify(response.data) + " image-upload");
                console.log(selected_file.name + " imagename")

                if (response.data.status) {
                    if (name === "profile") {
                        setfile("block")
                        setprofilephoto(response.data.payload.url)
                    } else if (name === "cover") {
                        setcoverfile("block")
                        setcoverphoto(response.data.payload.url)
                    }
                } else {
                    message.error(response.data.payload)
                }
            } else {
                message.error("please upload only images")
            }
        } else {
            if (fileext.substring(0, 5) === "image") {
                message.error("Please upload only files")
            }else{
            let fileName = "Document" + Math.random() + "." + /[^.]+$/.exec(filename);
            const formData = new FormData();
            formData.append("files", selected_file)

            const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
            const email = "ram@nvestbank.com"; //email of the developer.
            const path_inside_brain = "root/CounselAppImages/";


            const token = jwt.sign({ name: fileName, email: email }, secret, {
                algorithm: "HS512",
                expiresIn: 240,
                issuer: "gxjwtenchs512",
            });


            let response = await axios.post(
                `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                formData,
                {
                    headers: {
                        "Access-Control-Allow-Origin": "*",
                    },
                }
            );

            console.log(JSON.stringify(response.data) + " document-upload");
            if (response.data.status) {
                if (name === "passport") {
                    setpassport(response.data.payload.url)
                } else if (name === "birthcertificate") {
                    setbirthcertificate(response.data.payload.url)
                } else if (name === "license") {
                    setlicense(response.data.payload.url)
                } else if (name === "socialinsurancenumber") {
                    setsocialinsurancenumber(response.data.payload.url)
                } else if (name === "criminalbackgroundcheck") {
                    setcriminalbackgroundcheck(response.data.payload.url)
                } else {
                    message.error("invalid upload")
                }
            }
        }
    }
}
}

    function onChange(value) {
        console.log(`selected ${value}`);
        setstatename(value)
    }

    function onBlur() {
        console.log('blur');
    }

    function onFocus() {
        console.log('focus');
    }
    function onSearch(val) {
        console.log('search:', val);
    }
    function onChange1(value) {
        console.log(`selected ${value}`);
        setcountryname(value.substring(2))
        console.log("country-name " + countryname)
        axios.get('https://counsel.apimachine.com/api/getstatesbycountry/' + `${value.substring(0,2)}`)
            .then(response => {
                if (response.data.success) {
                    setstates(response.data.data)
                    console.log(JSON.stringify(states) + " states")
                } else {
                    setstates([])
                    message.error(response.data.message)
                }
            }).catch(err => console.log(err))
    }

    function onBlur1() {
        console.log('blur');
    }

    function onFocus1() {
        console.log('focus');
    }
    function onSearch1(val) {
        console.log('search:', val);
    }
    const inputchange = (e) => {
        if (e.target.name === "firstname") {
            setfirstname(e.target.value)
        } else if (e.target.name === "lastname") {
            setlastname(e.target.value)
        } else if (e.target.name === "address") {
            setaddress(e.target.value)
        } else if (e.target.name === "phonenumber") {
            setphonenumber(e.target.value)
        } else if (e.target.name === "occupation") {
            setoccupation(e.target.value)
        } else if(e.target.name === "companyname"){
            setcompanyname(e.target.value)
        } else if(e.target.name === "companynumber"){
            setcompanynumber(e.target.value)
        } else if(e.target.name === "companyaddress"){
            setcompanyaddress(e.target.value)
        } else if(e.target.name === "workexperience"){
            setworkexperience(e.target.value)
        } else {
            message.error("invalid input value")
        }
    }

    const submitprofile = (e)=>{
        e.preventDefault()
        if(countryname === "" || statename === ""){
            message.error("Please select country/state")
        }else{
        let body = {
            "FirstName":firstname,
            "LastName":lastname,
            "ProfileImage":profilephoto,
            "CoverPhoto":coverphoto,
            "Country": countryname,
            "State":statename,
            "Address":address,
            "Passport":passport,
            "BirthCertificate":birthcertificate,
            "License":license,
            "SocialInsuranceNumber":socialinsurancenumber,
            "PhoneNumber":phonenumber,
            "Occupation":occupation,
            "EmployerInformation":{
                "CompanyName":companyname,
                "CompanyNumber":companynumber,
                "CompanyAddress":companyaddress,
                "YearsWorkingForTheCompany":workexperience,
            },
            "CriminalBackgroundCheck":criminalbackgroundcheck
        }
    
        console.log("profile-details " + JSON.stringify(body))
        axios.post('https://counsel.apimachine.com/api/adduserdetails/' + props.getid, body).then(res => {
            console.log(res.data)
            setloading(false)
            if (res.data.success) {
                message.success("Profile details added")
                setfirstname("")
                setlastname("")
                // setprofilephoto("")
                // setcoverphoto("")
                // setcountryname("")
                // setstatename("")
                setaddress("")
                // setpassport("")
                // setbirthcertificate("")
                // setlicense("")
                // setsocialinsurancenumber("")
                setphonenumber("")
                setoccupation("")
                setcompanyname("")
                setcompanynumber("")
                setcompanyaddress("")
                setworkexperience("")
                // setcriminalbackgroundcheck("")
            } else {
                message.error(res.data.message)
            }
        }).catch(err => console.log(err))
    }
}
    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
      &nbsp;&nbsp;&nbsp;<span className="add-style">Add Profile Details</span>
        </div>
        <form onSubmit = {submitprofile}>
        <div style={{ display: "flex" }}>
            <div className="crd-style widthcard">
                <p className="adjust-title">First Name</p>
                <input type="text" name="firstname" className="thetext-styy1 namewidth" value = {firstname} placeholder="Ex: shorupan" onChange={inputchange} required />
            </div>
            <div className="crd-style widthcard adj">
                <p className="adjust-title">Last Name</p>
                <input type="text" name="lastname" className="thetext-styy1 namewidth" value = {lastname} placeholder="Ex: pirakaspathy" onChange={inputchange} required />
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Upload Profile Photo</p><br />
            <div style={{ display: "flex" }}>
                <input type="file" className="theimage-styy" name="profile" onChange={handleChange} required/>
                <img src={profilephoto} style={{ display: thefile }} className=" img-stye" alt="profilephoto" />
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Upload Cover Photo</p><br />
            <input type="file" className="theimage-styy" name="cover" onChange={handleChange} required/>
            <img src={coverphoto} style={{ display: coverfile }} className="img-stye1" alt="coverphoto" />
        </div>
        <div style={{ display: "flex" }}>
            <div className="crd-style widthcard">
                <p className="adjust-title">Country</p><br />
                <div className="thedrop-stye1">
                    <Select
                        showSearch
                        onChange={onChange1}
                        onFocus={onFocus1}
                        onBlur={onBlur1}
                        style={{ width: 200 }}
                        placeholder="Select Country"
                        optionFilterProp="children"
                        onSearch={onSearch}
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        {countries.map(element => {
                            return <Option key={element.name} value={element.code + element.name}>{element.name}</Option>
                        })}
                    </Select>

                </div>
            </div>
            <div className="crd-style widthcard adj">
                <p className="adjust-title">State</p><br />
                <div className="thedrop-stye1">
                    <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select State"
                        optionFilterProp="children"
                        onChange={onChange}
                        onFocus={onFocus}
                        onBlur={onBlur}
                        onSearch={onSearch1}
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        {states.map(element => {
                            return <Option value={element.region}>{element.region}</Option>
                        })}
                    </Select>
                </div>
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Enter Address</p><br />
            <div className="thetext-styy1 textarea-width">
                <TextArea id="address" name="address" value = {address} onChange={inputchange} rows="4" cols="50" required></TextArea>
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Upload Passport</p><br />
            <div style={{ display: "flex" }}>
                <input type="file" className="theimage-styy" name="passport" onChange={handleChange} required/>
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Birth Certificate</p><br />
            <div style={{ display: "flex" }}>
                <input type="file" className="theimage-styy" name="birthcertificate" onChange={handleChange} required/>
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">License</p><br />
            <div style={{ display: "flex" }}>
                <input type="file" className="theimage-styy" name="license" onChange={handleChange} required/>
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Social Insurance Number</p><br />
            <div style={{ display: "flex" }}>
                <input type="file" className="theimage-styy" name="socialinsurancenumber" onChange={handleChange} required/>
            </div>
        </div>
        <div style={{ display: "flex" }}>
            <div className="crd-style widthcard">
                <p className="adjust-title">Phone Number</p>
                <input type="number" name="phonenumber" value = {phonenumber} className="thetext-styy1 namewidth" onChange={inputchange} placeholder="Ex: 1234567890" required />
            </div>
            <div className="crd-style widthcard adj">
                <p className="adjust-title">Occupation</p>
                <input type="text" name="occupation" value = {occupation} className="thetext-styy1 namewidth" onChange={inputchange} placeholder="Ex: employee" required />
            </div>
        </div>
        <div style={{ display: "flex" }}>
            <div className="crd-style widthcard">
                <p className="adjust-title">Company Name</p>
                <input type="text" name="companyname" value = {companyname} className="thetext-styy1 namewidth" onChange={inputchange} placeholder="Ex: nvest" required />
            </div>
            <div className="crd-style widthcard adj">
                <p className="adjust-title">Company Number</p>
                <input type="number" name="companynumber" value = {companynumber} className="thetext-styy1 namewidth" onChange={inputchange} placeholder="Ex: 1234567890" required />
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Company Address</p><br />
            <div className="thetext-styy1 textarea-width">
                <TextArea id="companyaddress" value = {companyaddress} onChange={inputchange} name="companyaddress" rows="4" cols="50" required></TextArea>
            </div>
        </div>
        <div className="crd-style">
            <p className="adjust-title">Years Working for the Company</p>
            <input type="number" name="workexperience" value = {workexperience} onChange={inputchange} className="thetext-styy1 namewidth" placeholder="Ex: 2" required />
        </div>
        <div className="crd-style">
            <p className="adjust-title">Criminal Background Check</p><br />
            <div style={{ display: "flex" }}>
                <input type="file" className="theimage-styy" name="criminalbackgroundcheck" onChange={handleChange} required/>
            </div>
        </div>
        <button type="submit"
            className="footer addprofilesubmit"
            disabled={loading}
        >
            {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Submit"}
        </button>
        </form>
    </div>
}

export default AddProfile;
