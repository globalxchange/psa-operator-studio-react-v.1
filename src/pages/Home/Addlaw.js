import React, { useState } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Form, Input, InputNumber, Button, Card, Space, Select, message } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import Axios from 'axios';


function Addlaw(props) {
    const [title, settitle] = useState("")
    const [loading, setloading] = useState(false)
    const [body, setbody] = useState("")
    const [inputList, setInputList] = useState([{ Clause: "" }]);
    const handleChange = (e) => {
        if (e.target.name === "title") {
            settitle(e.target.value)
        } else if (e.target.name === "body") {
            setbody(e.target.value)
        } else {
            message.error("error while adding input")
        }
    }
    const handleInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...inputList];
        list[index][name] = value;
        setInputList(list);
    };
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };
    const handleAddClick = () => {
        setInputList([...inputList, { Clause: "" }]);
    };
    // const handleSubmit = (e) => {
    //     e.preventDefault()
    //     console.log(title, body)
    // }
    const onFinish = values => {
        console.log(values);
        console.log(title, body)
        console.log(JSON.stringify(inputList))
        let Tags = []
        for(let i=0;i<values.users.length;i++){
            Tags.push({Tag:[values.users[i]]})
        }
       console.log( JSON.stringify(values.users))
       console.log(JSON.stringify(Tags))
        let body1 = {
            "Title":title,
            "Body": body,
            "Clauses":inputList,
            "Tags":Tags
        }
        Axios.post("https://counsel.apimachine.com/api/proposelaw/" + props.getlegid, body1).then(res=>{
            if(res.data.success){
                message.success("Law Created Successfully")
            }else{
                message.error(res.data.message)
            }
        })

        console.log(JSON.stringify(body1))
    };
    // const onFinish1 = values => {
    //     console.log(values);
    // };

    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Propose law</span>
        </div>
        {/* <p>{props.getlegid}</p> */}


        <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
            <Form.List name="users">
                {(fields, { add, remove }) => {
                    return (
                        <div>
                            <div className="crd-style">
                                <p className="adjust-title">Title</p>
                                <input type="text" name="title" className="inputtitle" placeholder="Ex: law firm" value={title} onChange={handleChange} required />
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Body</p>
                                <textarea style={{ marginLeft: "25px", border: "none", width: "480px" }} placeholder="Enter description..." className="inputtxtarea" rows="4" cols="50" value={body} name="body" onChange={handleChange} required></textarea>
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Clauses</p>
                                {inputList.map((x, i) => {
                                    return (
                                        <div>
                                            <div style={{ display: "flex" }}>
                                                <p className="theclausesty">Clause</p>
                                                <input
                                                    name="Clause"
                                                    placeholder="Clause"
                                                    className="theinpclausesty"
                                                    value={x.Clause}
                                                    onChange={e => handleInputChange(e, i)}
                                                />
                                                {inputList.length !== 1 && <MinusCircleOutlined
                                                    className="mr10 theminusbtn"
                                                    onClick={() => handleRemoveClick(i)} />}
                                                <div>
                                                </div>
                                            </div>
                                            {inputList.length - 1 === i && <Button className="addclausebtn" type="dashed" onClick={handleAddClick}><PlusOutlined /> Add Clause</Button>}
                                        </div>
                                    );
                                })}
                            </div>
                            <div className="crd-style">
                                <p className="adjust-title">Tags</p><br />
                                {fields.map(field => (
                                    <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="start">
                                        <Form.Item
                                            {...field}
                                            name={[field.name, 'TagType']}
                                            fieldKey={[field.fieldKey, 'TagType']}
                                            rules={[{ required: true, message: 'Missing tag type' }]}
                                        >
                                            <Input placeholder="Tag type" style={{ marginLeft: "25px" }} />
                                        </Form.Item>
                                        <Form.Item
                                            {...field}
                                            name={[field.name, 'TagName']} 
                                            fieldKey={[field.fieldKey, 'TagName']}
                                            rules={[{ required: true, message: 'Missing tag name' }]} 
                                        >
                                            <Input placeholder="Tag Name" style={{ marginLeft: "25px" }} />
                                        </Form.Item>
                                        {/* {fields.length > 1 ? ( */}
                                        <MinusCircleOutlined className="marginsty"
                                            onClick={() => {
                                                remove(field.name);
                                            }}
                                        />
                                        {/* ): null} */}
                                    </Space>
                                ))}
                                <Form.Item>
                                        <Button className="addclausebtn thebtm"
                                            type="dashed"
                                            onClick={() => {
                                                add();
                                            }}

                                        >
                                            <PlusOutlined /> Add field
                                        </Button>
                                </Form.Item>
                            </div>
                        </div>
                    );
                }}
            </Form.List>

            <button type="submit" style={{ position: "initial" }}
                className="footer btmste"
                disabled={loading}
            >
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Submit"}
            </button>
        </Form>
    </div>;
}

export default Addlaw;
