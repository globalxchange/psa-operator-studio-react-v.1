import React, { useState, useEffect, useContext } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import SwagImg from "../../static/images/sidebar-icons/logo/swag.png"
import "./pages.scss";
import { CounselAppContext } from '../../Context_Api/Context';


function WorkSpaceTwo(props) {
    const context = useContext(CounselAppContext);

    const openview = (e)=>{
        e.preventDefault()
        context.setVisible3(true)
        context.setVisible1(false)

    }
    return (
        <div>
            <div className="main-style">
                <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose11()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style" >What Do You Want To Fetch</span>
            </div>
            <div className="crd-style" style = {{cursor:"pointer"}} onClick = {openview}>
                <div className="titleimg-sty">
                    <img src={SwagImg} alt="swag" className="img-sty" />
                    <span className="maintitle-sty">User Type</span>
                </div>
                <p className="body-sty">An accounting professional is a user who provides a service to AccountingTool Users</p>
                </div>
        </div>
    )
}

export default WorkSpaceTwo