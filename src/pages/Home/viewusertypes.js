import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import axios from "axios"
import "./pages.scss"
import { message, Modal, Button } from 'antd';
import { Row, Col } from 'reactstrap';
import { faCross, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


function Viewusertype(props) {
    const [usertype, setusertype] = useState([])
    const [theid, setid] = useState("")
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [thename, setname] = useState("")

    const theget = () => {
        const getusers = message.loading("getting user types")
        axios.get('https://ms.apimachine.com/api/getusertype')
            .then(response => {
                if (response.data.success) {
                    setTimeout(getusers, 100);
                    setusertype(response.data.data)
                } else {
                    message.error(response.data.message)
                }
            }).catch(err => console.log(err))
    }
    useEffect(() => {
        theget()

    }, [])
    console.log(usertype)

    const thechangeid =  (e) => {
        e.preventDefault()
        console.log("the-id" + theid)
        setIsModalVisible(true);
    }
    const handleOk = () => {
        setIsModalVisible(false);
        let body = {
            _id: theid
        }
        const hide2 = message.loading("deleting user type")
        axios.delete("https://ms.apimachine.com/api/deleteusertype/5fa42a94b872e706309874a7", { data: body }).then(res => {
            if (res.data.success) {
                setTimeout(hide2, 100);
                message.success(res.data.message)
                theget()
            } else {
                console.log(JSON.stringify(body) + "the-body")
                setTimeout(hide2, 100);
                message.error(res.data.message)
            }

        })
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return (
        <div>
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer", marginTop: "-6px" }} onClick={() => props.onClose11()} />
  &nbsp;&nbsp;&nbsp;<span className="add-style" >View User Types</span><br />
            {usertype.map(element1 => {
                return <div className="crd-style" style={{ marginTop: "10px" }}>
                    <button className="thebtn-idsty" onClick={thechangeid}>
                        <FontAwesomeIcon className="theclose-ico" icon={faWindowClose} onClick={() => { setid(element1._id); setname(element1.NameOfUserType) }} />
                    </button>
                    <br />
                    <img className="the-img-sty" src={element1.Icon} alt="icon" />
                    <br />
                    <Row>
                        <Col sm="5">
                            <p className="thekeysty1 ">Name Of User Type</p>
                        </Col>
                        <Col sm="1" style={{ marginLeft: "25px" }}>:</Col>
                        <Col sm="5">
                            <p>{element1.NameOfUserType}</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="5">
                            <p className="thekeysty1 ">Description</p>
                        </Col>
                        <Col sm="1" style={{ marginLeft: "25px" }}>:</Col>
                        <Col sm="5">
                            <p>{element1.Description}</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="5">
                            <p className="thekeysty1 ">PrerequsiteUserType</p>
                        </Col>
                        <Col sm="1" style={{ marginLeft: "25px" }}>:</Col>
                        <Col sm="5">
                            <p>{element1.PrerequsiteUserType}</p>
                        </Col>
                    </Row>
                    <Modal
                        title="Delete User Type"
                        visible={isModalVisible}
                        onOk={handleOk}
                        onCancel={handleCancel}
                    >
                        <p>Do you want to delete user_type "<b>{thename}</b>" ?</p>
                    </Modal>
                </div>
            })}

        </div>
    )
}
export default Viewusertype;
