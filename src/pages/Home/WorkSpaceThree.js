import React, { useState, useEffect } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import axios from "axios"
import message from 'antd/lib/message';
import jwt from "jsonwebtoken";
import TextArea from 'antd/lib/input/TextArea';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';





function WorkSpaceThree(props) {
    const [profilephoto, setprofilephoto] = useState("")
    const [usertype, setusertype] = useState("")
    const [thefile, setfile] = useState("none")
    const [description, setdescription] = useState("")
    const [PrerequsiteUserType, setPrerequsiteUserType] = useState("")
    const [loading, setloading] = useState(false)
    const [usertype1, setusertype1] = useState([])

    const inputchange = (e) => {
        if (e.target.name === "usertype") {
            setusertype(e.target.value)
        } else if (e.target.name === "description") {
            setdescription(e.target.value)
        } else if (e.target.name === "PrerequsiteUserType") {
            setPrerequsiteUserType(e.target.value)
        } else {
            message.error("invalid input value")
        }
    }
    const theget = () => {
        const getusers = message.loading("getting user types")
        axios.get('https://ms.apimachine.com/api/getusertype')
            .then(response => {
                if (response.data.success) {
                    setTimeout(getusers, 100);
                    setusertype1(response.data.data)
                } else {
                    message.error(response.data.message)
                }
            }).catch(err => console.log(err))
    }
    const submitprofile = (e) => {
        e.preventDefault()
        console.log(usertype, description, PrerequsiteUserType, profilephoto)
        let body = {
            "NameOfUserType": usertype,
            "Icon": profilephoto,
            "Description": description,
            "PrerequsiteUserType": PrerequsiteUserType,
        }
        axios.post('https://ms.apimachine.com/api/createusertype/5fa42a94b872e706309874a7', body).then(res => {
            if (res.data.success) {
                message.success("User Type added")
                setusertype("")
                setprofilephoto("")
                setdescription("")
                setPrerequsiteUserType("")
                theget()
            }else{
                message.error(res.data.message)
            }
        }).catch(err => console.log(err))

    }
    let handleChange = async (e) => {
        e.preventDefault()
        if (e.target.files[0] === undefined) {
            message.error("Please upload files")
        } else {
            let filename = e.target.files[0].name
            console.log(e.target.files[0])
            console.log("name-file " + filename)
            let name = e.target.name
            console.log("name " + name)

            let selected_file = e.target.files[0]
            // let fileName = e.target.files[0].name
            let fileext = e.target.files[0].type
            console.log("file-type " + fileext)
            //code starts here
            if (fileext.substring(0, 5) === "image") {
                const formData = new FormData();
                formData.append("files", selected_file)
                const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
                const email = "ram@nvestbank.com"; //email of the developer.
                const path_inside_brain = "root/CounselAppImages/";
                let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
                console.log(fileName)
                const token = jwt.sign({ name: fileName, email: email }, secret, {
                    algorithm: "HS512",
                    expiresIn: 240,
                    issuer: "gxjwtenchs512",
                });

                console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
                let response = await axios.post(
                    `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                    formData,
                    {
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                        },
                    }
                );
                if (response.data.status) {
                    setfile("block")
                    setprofilephoto(response.data.payload.url)
                } else {
                    message.error(response.data.payload)
                }
                console.log(JSON.stringify(response.data) + " image-upload");
                console.log(selected_file.name + " imagename")

            } else {
                message.error("please upload only images")
            }
        }
    }
    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose11()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Add User Type</span>
        </div>
        <form onSubmit={submitprofile}>
            <div className="crd-style">
                <p className="adjust-title">Name Of User Type</p>
                <input type="text" name="usertype" value={usertype} onChange={inputchange} className="thetext-styy1 namewidth" placeholder="Ex: general" required />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Upload Icon</p><br />
                <div style={{ display: "flex" }}>
                    <input type="file" className="theimage-styy" name="profile" onChange={handleChange} required />
                    <img src={profilephoto} style={{ display: thefile }} className=" img-stye" alt="profilephoto" />
                </div>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Description</p><br />
                <div className="thetext-styy1 textarea-width">
                    <TextArea id="address" name="description" value={description} onChange={inputchange} rows="4" cols="50" required></TextArea>
                </div>
            </div>
            <div className="crd-style">
                <p className="adjust-title">PrerequsiteUserType</p>
                <input type="text" name="PrerequsiteUserType" value={PrerequsiteUserType} onChange={inputchange} className="thetext-styy1 namewidth" placeholder="Ex: general" />
            </div>
            <button type="submit"
                className="footer"
                disabled={loading}
            >
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Submit"}
            </button>
        </form>

    </div>;
}

export default WorkSpaceThree;
