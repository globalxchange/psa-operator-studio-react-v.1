import React from "react";
import {Tabs, Button} from "antd";
import {ReactComponent as IconBoards} from "../../static/images/navbar-icons/boards.svg";
import {ReactComponent as IconList} from "../../static/images/navbar-icons/list.svg";

const {TabPane} = Tabs;

const operations = (
  <div style={{display: "flex"}}>
    <div className="ml-auto boards active">
      <IconBoards />
      <div className="itm my-auto ml-2">View 1</div>
    </div>
    <div className="divider" />
    <div className="boards">
      <IconList />
      <div className="itm my-auto ml-2" style={{color: "black"}}>
        View 2
      </div>
    </div>
  </div>
);

const OperationsSlot = {
  left: <Button className="tabs-extra-demo-button">Left Extra Action</Button>,
  right: <Button>Right Extra Action</Button>,
};

const Tabsss = () => {
  const [position] = React.useState(["left", "right"]);

  const slot = React.useMemo(() => {
    if (position.length === 0) return null;

    return position.reduce(
      (acc, direction) => ({...acc, [direction]: OperationsSlot[direction]}),
      {}
    );
  }, [position]);

  return (
    <Tabs tabBarExtraContent={operations}>
      <TabPane tab="User Management" key="1" />
    </Tabs>
  );
};

export default Tabsss;
