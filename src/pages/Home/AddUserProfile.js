import React from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import { Form, Input, InputNumber, Button, Card, Space, Select, message } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import axios from "axios";

// import MainLayout from '../../Layouts/MainLayout';

function Admin(props) {
    // const formItemLayout = {
    //     labelCol: {
    //       xs: { span: 24 },
    //       sm: { span: 4 },
    //     },
    //     wrapperCol: {
    //       xs: { span: 24 },
    //       sm: { span: 20 },
    //     },
    //   };

    const formItemLayoutWithOutLabel = {
        wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 20, offset: 2 },
        },
    };

    const onFinish = values => {
        console.log('Received values of form:', values);
        console.log(JSON.stringify(values.names))
        let theprofiles = []
        let body
        if (props.getkey === "User") {
            values.names.forEach(element => {
                theprofiles.push({ CorporateName: element })
            })
            body = {
                "profile_id": props.setId,
                "CorporateProfile": theprofiles
            }
            axios.post('https://counsel.apimachine.com/api/addcorporateprofile', body).then(res => {
                console.log(res.data)
                if (res.data.success) {
                    message.success(res.data.message)
                } else {
                    message.error(res.data.message)
                }
            }).catch(err => console.log(err))
        } else if (props.getkey === "Legal Professional") {
            for (let i = 0; i < values.names.length; i++) {
                let num = values.names[i].first
                theprofiles.push({ [num]: values.names[i].last })
            }
            body = {
                "profile_id": props.setId,
                "LegalProfile": theprofiles
            }
            axios.post('https://counsel.apimachine.com/api/addlegalprofile', body).then(res => {
                console.log(res.data)
                if (res.data.success) {
                    message.success(res.data.message)
                } else {
                    message.error(res.data.message)
                }
            }).catch(err => console.log(err))
        } else {
            message.error("Invalid profile")
        }
        console.log("the-profiles " + JSON.stringify(theprofiles))
    }
    let theprofile
    if (props.getkey === "User") {
        theprofile = "Corporate"
    } else if (props.getkey === "Legal Professional") {
        theprofile = "Legal"
    }
    return <div>
        <div className="main-style">
            <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Add&nbsp;{theprofile}&nbsp;Profile</span>
            {/* <p>{props.setId}</p> */}
        </div>

        <Form name="dynamic_form_item" {...formItemLayoutWithOutLabel} onFinish={onFinish}>
            <Form.List name="names">
                {(fields, { add, remove }) => {
                    if (props.getkey === "User" || props.getkey === "Admin") {
                        return (
                            <div>
                                {fields.map((field, index) => (
                                    <Form.Item
                                        required={false}
                                        key={field.key}
                                    >
                                        <Form.Item
                                            {...field}
                                            validateTrigger={['onChange', 'onBlur']}
                                            rules={[
                                                {
                                                    required: true,
                                                    whitespace: true,
                                                    message: "Profile name is required",
                                                },
                                            ]}
                                            noStyle
                                        >
                                            <Input placeholder="profile" style={{ width: '90%' }} />
                                        </Form.Item>
                                        <MinusCircleOutlined
                                            className="dynamic-delete-button"
                                            style={{ margin: '0 8px' }}
                                            onClick={() => {
                                                remove(field.name);
                                            }}
                                        />
                                    </Form.Item>
                                ))}
                                <Form.Item>
                                    <Button
                                        type="dashed"
                                        onClick={() => {
                                            add();
                                        }}
                                        style={{ width: '90%' }}
                                    >
                                        <PlusOutlined /> Add field
                </Button>
                                </Form.Item>
                            </div>
                        );
                    } else if (props.getkey === "Legal Professional") {
                        return (
                            <div>
                                {fields.map(field => (
                                    <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="start">
                                        <Form.Item
                                            {...field}
                                            name={[field.name, 'first']}
                                            fieldKey={[field.fieldKey, 'first']}
                                            rules={[{ required: true, message: 'Missing Legal Profile Type' }]}
                                        >
                                            <Input placeholder="Legal Profile Type" style={{ marginLeft: "27px", width: "102%" }} />
                                        </Form.Item>
                                        <Form.Item
                                            {...field}
                                            name={[field.name, 'last']}
                                            fieldKey={[field.fieldKey, 'last']}
                                            rules={[{ required: true, message: 'Missing Legal Profile Name' }]}
                                        >
                                            <Input placeholder="Legal Profile Name" style={{ width: "174%" }} />
                                        </Form.Item>
                                        <MinusCircleOutlined
                                            style={{ margin: '0 88px' }}
                                            onClick={() => {
                                                remove(field.name);
                                            }}
                                        />
                                    </Space>
                                ))}

                                <Form.Item>
                                    <Button
                                        type="dashed"
                                        onClick={() => {
                                            add();
                                        }}
                                        style={{ width: '90%' }}
                                        block
                                    >
                                        <PlusOutlined /> Add field
                      </Button>
                                </Form.Item>
                            </div>
                        );
                    } else {
                        return <p>Error adding profiles</p>
                    }
                }}
            </Form.List>
            <Form.Item>
                <Button type="primary" htmlType="submit" style={{ width: '90%' }}>
                    Submit
        </Button>
            </Form.Item>
        </Form>
    </div>
}

export default Admin;
