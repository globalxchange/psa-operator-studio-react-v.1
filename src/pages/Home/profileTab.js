import React, {useContext, useState, useEffect} from "react";
import {CounselAppContext} from "../../Context_Api/Context";
import MainLayout from "../../Layouts/MainLayout";
import MeetingsDrawers from "../Dashboard-Components/Dasboard-Meetings-Drawer/MeetingsDrawers";
import "./pages.scss";
import logo from "../../static/images/sidebar-icons/meetingstrmlogo.svg";
import {Row, Col} from "antd";
import axios from "axios";
import message from "antd/lib/message";
import {Tabs, Button, Divider, Checkbox} from "antd";
import {ReactComponent as IconBoards} from "../../static/images/navbar-icons/boards.svg";
import {ReactComponent as IconList} from "../../static/images/navbar-icons/list.svg";

function ProfileTab() {
  const context = useContext(CounselAppContext);
  const [subdomain, setsubdomain] = useState("");
  const [displaymsg, setdisplaymsg] = useState("");
  const [displaybtn, setdisplaybtn] = useState("none");
  const [office, setOffice] = useState("");
  const [endselected, setEndselected] = useState("Toggle 1");
  const [tabSelected, setTabSelected] = useState("sub1");
  const [email, setEmail] = useState(
    localStorage.getItem("CounselAppLoginAccount") || ""
  );
  const [userId, setId] = useState("");
  const [subtoken, setToken] = useState(
    localStorage.getItem("CounselAppToken") || ""
  );
  const apiUrl = "https://ms.apimachine.com";
  const authAxios = axios.create({
    baseURL: apiUrl,
    headers: {
      email: email,
      token: `${subtoken}`,
    },
  });
  // useEffect(() => {
  //   localStorage.setItem("CounselAppLoginAccount", email);
  // }, [email]);

  useEffect(() => {
    localStorage.setItem("CounselAppToken", subtoken);
    axios
      .get("https://ms.apimachine.com/api/get_by_email/" + email)
      .then((res) => {
        const hide2 = message.loading("Getting Office List");
        if (res.data.success) {
          console.log(res.data.thefetch + "the-fetch");
          setTimeout(hide2, 100);

          console.log(res.data.thefetch.psa_app_userId);
          setId(res.data.thefetch.psa_app_userId);
          setOffice(res.data.thefetch.subdomain);
        } else {
          message.error("Subdomains not available");
        }
      });
  }, [subtoken]);
  const registerdomain = async (e) => {
    e.preventDefault();
    const hide1 = message.loading("Registering Office");
    var body1 = {
      subdomain: subdomain,
    };
    await authAxios.post("/api/registersubdomain", body1).then((res) => {
      console.log(res.data);
      if (res.data.success) {
        setOffice(subdomain);
      } else {
        message.error("Subdomain exists");
      }
      // axios.get("https://ms.apimachine.com/api/get_by_email/" + email).then(res12 => {
      //     if (res12.data.success) {
      //         console.log(res12.data)
      //         setTimeout(hide1, 100);
      //             setOffice(res.data.thefetch.subdomain)
      //     } else {
      //         setTimeout(hide1, 100);
      //         message.error("Subdomain exists")
      //     }
      // })
    });
  };
  const handleChange = (e) => {
    e.preventDefault();
    setsubdomain(e.target.value);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const hide = message.loading("Checking Availability of subdomain", 0);
    setdisplaymsg("");
    console.log(subdomain);
    console.log(localStorage.getItem("CounselAppToken"));
    console.log(context.thebilltab);
    // const authAxios = axios.create({
    //   baseURL: apiUrl,
    //   headers: {
    //     email: 'ram@nvestbank.com',
    //     token: `${subtoken}`,
    //   },
    // });
    // const result = await authAxios.get('/meetings/subdomain/list')
    var body = {
      subdomain: subdomain,
    };
    await authAxios.post("/api/checksubdomain", body).then((res) => {
      if (res.data.checksubdomain.body.status) {
        console.log(res.data);
        setTimeout(hide, 100);
        setdisplaymsg(
          <p className="themsg-display" style={{color: "#186AB4"}}>
            The Name You Prosed Is Available. Do You Want To Add This Office To
            Your Profile?
          </p>
        );
        authAxios.get("/api/subdomainlist").then((res1) => {
          if (res1.data.success) {
            setdisplaybtn("none");
          } else {
            setdisplaybtn("block");
          }
        });
        // setdisplaybtn('block')
        //  message.success('subdomain available')
      } else {
        setTimeout(hide, 100);
        console.log(JSON.stringify(res.data) + "the-error");
        setdisplaymsg(
          <p className="themsg-display" style={{color: "#FE5777"}}>
            The Name You Prosed Is Not Available. Please Try Another One
          </p>
        );
      }
    });
    // console.log(result.data)
  };
  const {TabPane} = Tabs;
  // const CheckboxGroup = Checkbox.Group;
  const operations = (
    <div style={{display: "flex"}}>
      <div className="ml-auto boards active">
        <IconBoards />
        <div className="itm my-auto ml-2">View 1</div>
      </div>
      <div className="divider" />
      <div className="boards">
        <IconList />
        <div className="itm my-auto ml-2" style={{color: "black"}}>
          View 2
        </div>
      </div>
    </div>
  );
  // const OperationsSlot = {
  //     left: <Button className="tabs-extra-demo-button">Left Extra Action</Button>,
  //     right: <Button>Right Extra Action</Button>,
  // };
  // const options = ['left', 'right'];
  // const Demo = () => {
  //     const [position, setPosition] = React.useState(['left', 'right']);

  //     const slot = React.useMemo(() => {
  //         if (position.length === 0) return null;

  //         return position.reduce(
  //             (acc, direction) => ({ ...acc, [direction]: OperationsSlot[direction] }),
  //             {},
  //         );
  //     }, [position]);
  // }
  function callback(key) {
    console.log(key);
  }
  return (
    <div>
      {/* <div className='market-place-dashboard'>{mainFunction()}</div> */}
      <Tabs
        defaultActiveKey="1"
        onChange={callback}
        tabBarExtraContent={operations}
      >
        <TabPane tab="Offices" key="1" style={{marginLeft: "45px"}}>
          <Row>
            <Col lg={14}>
              <p className="theoffice">Add a new office</p>
              <p className="thevail">
                Check The Availability Of The Office Link
              </p>
              <table>
                <tr>
                  <td>
                    <img src={logo} style={{height: "30px"}} />
                  </td>
                  <td>Enter Proposed Name</td>
                  <td>
                    <form onSubmit={handleSubmit}>
                      <input
                        type="text"
                        value={subdomain}
                        onChange={handleChange}
                        className="thein-sty"
                        required
                      />
                      <button type="submit" className="thebt-sty">
                        Check Availability
                      </button>
                    </form>
                  </td>
                </tr>
              </table>
              {displaymsg}
              <button
                className="thebt-dis"
                style={{display: displaybtn}}
                onClick={registerdomain}
              >
                Reserve Office
              </button>
            </Col>
            <Col lg={10}>
              <div className="theofc-sty">
                <p className="titleofc">Your Offices</p>
                <p className="dataofc">
                  An Office Is A Customizable Environment Which Is Hosted For
                  You At A Unique Sub-Domain
                </p>
                <u className="subdomain-titlesty">sub-domain List</u>
                {/* <ol className = "subdomain-liststy"> */}
                <p className="subdomain-liststy">{office}</p>
                {/* </ol> */}
              </div>
            </Col>
          </Row>
        </TabPane>
        <TabPane tab="Pending" key="2"></TabPane>
        <TabPane tab="Ideas" key="3"></TabPane>
      </Tabs>
    </div>
  );
}

export default ProfileTab;
