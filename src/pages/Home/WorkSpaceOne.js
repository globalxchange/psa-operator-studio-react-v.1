import React, { useState, useEffect,useContext } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg"
import SwagImg from "../../static/images/sidebar-icons/logo/swag.png"
import "./pages.scss"
import { CounselAppContext } from '../../Context_Api/Context';
import axios from "axios"

function WorkSpaceOne(props) {
  const context = useContext(CounselAppContext);

  const opencard = (e)=>{
    e.preventDefault()
    context.setVisible(false)
    context.setVisible2(true)

  }
  return (
    <div>
      <div className="main-style">
        <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onCloseall()} />
          &nbsp;&nbsp;&nbsp;<span className="add-style">Add New</span>
      </div>
      <div className="crd-style" style = {{cursor:"pointer"}} onClick = {opencard}>
        <div className="titleimg-sty">
          <img src={SwagImg} alt="swag" className="img-sty" />
          <span className="maintitle-sty">User Type</span>
          {/* <span className="thecount">{normalusers}</span> */}
        </div>
        <p className="body-sty">An accounting professional is a user who provides a service to AccountingTool Users</p>
        {/* <div>
          <button type="button" className="button-sty sty1">Invite</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose(); props.setVariable1("User") }} >Manual</button>
          <button type="button" className="button-sty" onClick={() => { props.onClose13(); props.setVariable2("User") }}>See User List</button>
        </div> */}
      </div>
    </div>
  )
}
export default WorkSpaceOne;
