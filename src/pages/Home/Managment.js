import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function Management() {
  return <MainLayout active="management"></MainLayout>;
}

export default Management;
