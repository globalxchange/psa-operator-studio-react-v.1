import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function CustomerRelations() {
  return <MainLayout active="customer-relations"></MainLayout>;
}

export default CustomerRelations;
