import React, { useState } from 'react';
import whitearrow from "../../static/images/sidebar-icons/logo/arrow-white.png"
import message from 'antd/lib/message';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
// import ReactTooltip from "react-tooltip";


function PasswordPage(props) {
  const [password, setPassword] = useState('')
  const [hidden, sethidden] = useState(true)
  const [thedisplay, setdisplay] = useState(false)
  // const [line1, setline1] = useState("black")
  // const [line2, setline2] = useState("black")
  // const [line3, setline3] = useState("black")
  // const [line4, setline4] = useState("black")
  console.log("user-name " + props.name[0])
  console.log("email " + props.name[1])
  // let line1
  // let line2
  // let line3
  // let line4
  const handleChange = async (e) => {
    e.preventDefault()
    await setPassword(e.target.value)
    console.log("input-pass" + password)

    // if (/.{8,}/.test(password)) {
    //   line1 = "green"
    // } else {
    //   line1 = "red"
    // }
    // if (/(?=.*?[A-Z])/.test(password)) {
    //   line2 = "green"
    // } else {
    //   line2 = "red"
    // }
    // if (/(?=.*?[0-9])/.test(password)) {
    //   line3 = "green"
    // } else {
    //   line3 = "red"
    // }
    // if (/(?=.*?[#?!@$%^&*-])/.test(password)) {
    //   line4 = "green"
    // } else {
    //   line4 = "red"
    // }
  }
  const handleSubmit = async (e) => {
    e.preventDefault()
    if (/^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(password)) {
      await props.CnfrmPasswordPage()
      await props.sendData([props.name[0], props.name[1], password, props.name[2], props.name[3]])
    } else {
      message.error("Doesn't match all requirements")
    }
  }
  const clickeye = (e) => {
    e.preventDefault()
    sethidden(!hidden)
    setdisplay(!thedisplay)
  }
  return <div className="outer">
    <div className="middle">
      <div className="inner">
        <h4 className="usrnm-sty">Create Password</h4>
        <p className="gxtitle-sty">GX Registration</p>
        <form onSubmit={handleSubmit}>
          <div className="crd-style">
            <p className="adjust-title">Create Password</p>
            <div style={{ display: "flex" }}>
              <input name="password" type={hidden ? "password" : "text"} className="thetext-styy1" value={password} onChange={handleChange} placeholder="***********" required />
              <p style={{ cursor: "pointer" }} onClick={clickeye}>{thedisplay ? <FontAwesomeIcon icon={faEye} /> : <FontAwesomeIcon icon={faEyeSlash} />}</p>
            </div>
          </div>
          <div style={{ textAlign: "initial" }}>
            <p className="validation-chk">Password should contain atleast</p>
            <ul>
              <li>Eight Characters</li>
              <li>One Upper Case letter</li>
              <li>One Number (0-9)</li>
              <li>One Special Character</li>
            </ul>
          </div>
          <div className="thebtmbtn-sty">
            <img src={whitearrow} alt="backarrow" className="whitearrow-sty" onClick={() => props.onClose()} />
            <button type="submit" className="centerbtn-sty">Continue</button>
          </div>
        </form>
      </div>
    </div>
  </div>
}

export default PasswordPage;
