import React, { useContext, useState } from "react";
import MainLayout from "../../Layouts/MainLayout";
import { CounselAppContext } from "../../Context_Api/Context";
import Tabs11 from "./Tabs12";
import { Drawer } from "antd";
import WorkSpaceOne from "../../pages/Home/WorkSpaceOne";
import WorkSpaceTwo from "../../pages/Home/WorkSpaceTwo";
import WorkSpaceThree from "../../pages/Home/WorkSpaceThree";
import Viewusertype from "../../pages/Home/viewusertypes";
// import MeetingsDrawers from '../Dashboard-Components/Dasboard-Meetings-Drawer/MeetingsDrawers';
import MainMeetingsDrawers from "../Dashboard-Components/Dasboard-Meetings-Drawer/MainMeetingsDrawer";
import "../../pages/Home/pages.scss";
import ProfileTab from "./profileTab";

function MarketPlaceOne() {
  const context = useContext(CounselAppContext);
  const {
    visible,
    visible1,
    visible2,
    visible3,
    selectedTab,
    drawer,
    drawerOpen,
    setVisible,
    setVisible1,
    setVisible2,
    setVisible3,
    thebilltab,
  } = useContext(CounselAppContext);
  const drawerFunction = () => {
    switch (drawer) {
      case "meetings-start":
        return <MainMeetingsDrawers />;
      default:
        break;
    }
  };
  const onClose = async () => {
    setVisible(false);
    setVisible1(false);
    setVisible2(false);
    setVisible3(false);
  };
  return (
    <MainLayout active="appconfig">
      {thebilltab.map((x) => {
        return (
          <div
            key={x.key}
            className={x.key === selectedTab.key ? context.view : "d-none"}
          >
            {x.tabName === "Profile" ? (
              <ProfileTab />
            ) : x.tabName === "deskstream" ? (
              " "
            ) : (
              <Tabs11 />
            )}
            {drawerOpen ? (
              <div className="market-place-drawer">{drawerFunction()}</div>
            ) : (
              ""
            )}
            <Drawer
              width={560}
              placement="right"
              closable={false}
              onClose={onClose}
              visible={visible}
              maskStyle={{ opacity: "0" }}
              bodyStyle={{ marginBottom: "0px" }}
              style={{ marginTop: "-5px" }}
            >
              <WorkSpaceOne onCloseall={onClose} />
            </Drawer>
            <Drawer
              width={560}
              placement="right"
              closable={false}
              onClose={onClose}
              visible={visible1}
              maskStyle={{ opacity: "0" }}
              bodyStyle={{ marginBottom: "0px" }}
              style={{ marginTop: "-5px" }}
            >
              <WorkSpaceTwo onClose11={onClose} />
            </Drawer>
            <Drawer
              width={560}
              placement="right"
              closable={false}
              onClose={onClose}
              visible={visible2}
              maskStyle={{ opacity: "0" }}
              bodyStyle={{ marginBottom: "0px" }}
              style={{ marginTop: "-5px" }}
            >
              <WorkSpaceThree onClose11={onClose} />
            </Drawer>
            <Drawer
              width={560}
              placement="right"
              closable={false}
              onClose={onClose}
              visible={visible3}
              maskStyle={{ opacity: "0" }}
              bodyStyle={{ marginBottom: "0px" }}
              style={{ marginTop: "-5px" }}
            >
              <Viewusertype onClose11={onClose} />
            </Drawer>
          </div>
        );
      })}
    </MainLayout>
  );
}

export default MarketPlaceOne;
