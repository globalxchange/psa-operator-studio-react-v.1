import React from 'react';
import MainLayout from '../../Layouts/MainLayout';

function MyAppsOne() {
  return <MainLayout active="myapps1"></MainLayout>;
}

export default MyAppsOne;
