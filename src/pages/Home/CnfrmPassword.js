import React, { useState } from 'react';
import whitearrow from "../../static/images/sidebar-icons/logo/arrow-white.png"
import message from 'antd/lib/message';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash, faSpinner } from '@fortawesome/free-solid-svg-icons';
import Axios from 'axios';

function CnfrmPassword(props) {
  const [password, setpassword] = useState("")
  const [hidden, sethidden] = useState(true)
  const [thedisplay, setdisplay] = useState(false)
  const [loading, setloading] = useState(false)

  const handleChange = (e) => {
    e.preventDefault()
    setpassword(e.target.value)
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    setloading(true)
    console.log("thepropsdata " + JSON.stringify(props.theformdata))
    if (props.theformdata[2] === password) {
      let body = {
        "username": props.theformdata[0],
        "email": props.theformdata[1][0],
        "password": props.theformdata[2],
        "ref_affiliate": props.theformdata[4],
        "account_type": props.theformdata[3],
        "signedup_app": "counsel"
      }
      console.log("form-data " + JSON.stringify(body))
      Axios.post("https://gxauth.apimachine.com/gx/user/signup", body).then(res => {
        console.log(res.data)
        if (res.data.status) {
          setloading(false)
          props.emailpage()
          props.sendEmail([props.theformdata[1][0],props.theformdata[1][1]])
        } else {
          setloading(false)
          message.error(res.data.message)
        }
      }).catch(err => console.log(err))
    } else {
      setloading(false)
      message.error("password mismatch")
    }
  }
  const clickeye = (e) => {
    e.preventDefault()
    sethidden(!hidden)
    setdisplay(!thedisplay)
  }
  return <div className="outer">
    <div className="middle">
      <div className="inner">
        <h4 className="usrnm-sty">Confirm Password</h4>
        <p className="gxtitle-sty">GX Registration</p>
        <form onSubmit={handleSubmit}>
          <div className="crd-style">
            <p className="adjust-title">Confirm Password</p>
            <div style={{ display: "flex" }}>
              <input type={hidden ? "password" : "text"} name="password" className="thetext-styy1" value={password} onChange={handleChange} placeholder="*********" required />
              <p onClick={clickeye}>{thedisplay ? <FontAwesomeIcon icon={faEye} /> : <FontAwesomeIcon icon={faEyeSlash} />}</p>
            </div>
          </div>
          <div className="thebtmbtn-sty">
            <img src={whitearrow} alt="backarrow" className="whitearrow-sty" onClick={() => props.onClose()} />
            <button type="submit" className="centerbtn-sty" disabled={loading}> {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Continue"}</button>
          </div>
        </form>
      </div>
    </div>
  </div>
}

export default CnfrmPassword;
