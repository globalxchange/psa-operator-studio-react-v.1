import React, { useState } from 'react';
import Arrow from "../../static/images/sidebar-icons/logo/arrow.svg";
import message from 'antd/lib/message';
import axios from "axios";
import jwt from "jsonwebtoken";
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';




function AddOns(props) {
    const [name1, setname1] = useState("")
    const [address, setaddress] = useState("")
    const [profilephoto, setprofilephoto] = useState("")
    const [coverphoto, setcoverphoto] = useState("")
    const [thefile, setfile] = useState("none")
    const [coverfile, setcoverfile] = useState("none")
    const [email1, setemail1] = useState("")
    const [website, setwebsite] = useState("")
    const [fax, setfax] = useState("")
    const [phonenumber, setphonenumber] = useState("")
    const [loading, setloading] = useState(false)
    const [inputList, setInputList] = useState([{ Link: "" }]);
    let handleChange1 = async (e) => {
        e.preventDefault()
        if (e.target.files[0] === undefined) {
            message.error("Please upload image")
        } else {
            let filename = e.target.files[0].name
            console.log(e.target.files[0])
            console.log("name-file " + filename)
            let name = e.target.name
            console.log("name " + name)

            let selected_file = e.target.files[0]
            let fileext = e.target.files[0].type
            console.log("file-type " + fileext)

            if (fileext.substring(0, 5) === "image") {
                const formData = new FormData();
                formData.append("files", selected_file)
                const secret = "uyrw7826^&(896GYUFWE&*#GBjkbuaf"; //secret not to be disclosed anywhere.
                const email = "ram@nvestbank.com"; //email of the developer.
                const path_inside_brain = "root/CounselAppImages/";
                let fileName = "image" + Math.random() + "." + /[^.]+$/.exec(filename);
                console.log(fileName)
                const token = jwt.sign({ name: fileName, email: email }, secret, {
                    algorithm: "HS512",
                    expiresIn: 240,
                    issuer: "gxjwtenchs512",
                });

                console.log(/[^.]+$/.exec(filename) + " name-of-the-file")
                let response = await axios.post(
                    `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
                    formData,
                    {
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                        },
                    }
                );
                console.log(JSON.stringify(response.data) + " image-upload");
                console.log(selected_file.name + " imagename")
                if (response.data.status) {
                    if (name === "profile") {
                        setfile("block")
                        setprofilephoto(response.data.payload.url)
                    } else if (name === "cover") {
                        setcoverfile("block")
                        setcoverphoto(response.data.payload.url)
                    }
                }
            } else {
                message.error("Please upload only images")
            }
        }
    }
    const handleInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...inputList];
        list[index][name] = value;
        setInputList(list);
    };
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };
    const handleAddClick = () => {
        setInputList([...inputList, { Link: "" }]);
    };
    const handleChange = (e) => {
        if (e.target.name === "name") {
            setname1(e.target.value)
        } else if (e.target.name === "address") {
            setaddress(e.target.value)
        } else if (e.target.name === "email") {
            setemail1(e.target.value)
        } else if (e.target.name === "website") {
            setwebsite(e.target.value)
        } else if (e.target.name === "fax") {
            setfax(e.target.value)
        } else if (e.target.name === "phonenumber") {
            setphonenumber(e.target.value)
        } else {
            message.error("error while saving input data")
        }
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        let body = {
            "Name" : name1,
            "Address" : address,
            "Logo" : profilephoto,
            "CoverPhoto" : coverphoto,
            "Email" : email1,
            "Website" : website,
            "Fax" : fax,
            "PhoneNumber" : phonenumber,
            "AdditionalLinks" : inputList
        }
        axios.post("https://counsel.apimachine.com/api/addlawfirmdetails/" + props.lawid, body).then(res=>{
            if(res.data.success){
                message.success("Law Firm Profile Created Successfully")
            }else{
                message.error(res.data.message)
            }
        })
        console.log("the-body " + JSON.stringify(body))
    }
    return <div>
        <form onSubmit={handleSubmit}>
            <div className="main-style">
                <img src={Arrow} alt="arrow" style={{ cursor: "pointer" }} onClick={() => props.onClose()} />
  &nbsp;&nbsp;&nbsp;<span className="add-style">Add Law Profile</span>
            </div>
            {/* <p>{props.lawid}</p> */}
            <div className="crd-style">
                <p className="adjust-title">Name</p>
                <input type="text" name="name" className="inputtitle" placeholder="Ex: Shorupan" value={name1} onChange={handleChange} required />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Address</p>
                <textarea style={{ marginLeft: "25px", border: "none", width: "480px" }} placeholder="Enter Address..." className="inputtxtarea" rows="4" cols="50" value={address} name="address" onChange={handleChange} required></textarea>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Upload Logo</p><br />
                <div style={{ display: "flex" }}>
                    <input type="file" className="theimage-styy" name="profile" onChange={handleChange1} required />
                    <img src={profilephoto} style={{ display: thefile }} className=" img-stye" alt="profilephoto" />
                </div>
            </div>
            <div className="crd-style">
                <p className="adjust-title">Upload Cover Photo</p><br />
                <input type="file" className="theimage-styy" name="cover" onChange={handleChange1} required />
                <img src={coverphoto} style={{ display: coverfile }} className="img-stye1" alt="coverphoto" />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Email</p>
                <input type="text" name="email" className="inputtitle" placeholder="Ex: Shorupan@nvestbank.com" value={email1} onChange={handleChange} required />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Website</p>
                <input type="text" name="website" className="inputtitle" placeholder="Ex: www.nvestbank.com" value={website} onChange={handleChange} required />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Fax</p>
                <input type="number" name="fax" className="inputtitle" placeholder="9192939495" value={fax} onChange={handleChange} required />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Phone Number</p>
                <input type="text" name="phonenumber" className="inputtitle" placeholder="9192939495" value={phonenumber} onChange={handleChange} required />
            </div>
            <div className="crd-style">
                <p className="adjust-title">Additional Links</p>
                {inputList.map((x, i) => {
                    return (
                        <div>
                            <div style={{ display: "flex" }}>
                                <p className="theclausesty">Link</p>
                                <input
                                    name="Link"
                                    placeholder="link"
                                    className="theinpclausesty"
                                    value={x.Link}
                                    onChange={e => handleInputChange(e, i)}
                                />
                                {inputList.length !== 1 && <MinusCircleOutlined
                                    className="mr10 theminusbtn"
                                    onClick={() => handleRemoveClick(i)} />}
                                <div>
                                </div>
                            </div>
                            {inputList.length - 1 === i && <Button className="addclausebtn" type="dashed" onClick={handleAddClick}><PlusOutlined /> Add Link</Button>}
                        </div>
                    );
                })}
            </div>
            <button type="submit" style={{ position: "initial" }}
                className="footer btmste"
                disabled={loading}
            >
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : "Submit"}
            </button>
        </form>
    </div>
}

export default AddOns;
