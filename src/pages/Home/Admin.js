import React,{ useContext} from 'react';
import MainLayout from '../../Layouts/MainLayout';
import { CounselAppContext } from '../../Context_Api/Context';
import './pages.scss';
import ProfileTab from "./profileTab"
import Tabs11 from "./Tabs12"

function Admin() {
  const context = useContext(CounselAppContext);
  return <MainLayout active="Admin">
         {context.thebilltab.map((x) => {
        return (
          <div
            key={x.key}
            className={
              x.key === context.selectedTab.key ? context.view : 'd-none'
            }
          >
             {x.tabName === 'Profile' ?  <ProfileTab /> : ""}
          </div>
        )
      })}
  </MainLayout>;
}

export default Admin;
