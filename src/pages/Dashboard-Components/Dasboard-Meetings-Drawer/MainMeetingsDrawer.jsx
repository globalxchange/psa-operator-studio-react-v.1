import React, { useContext } from "react";
import { CounselAppContext } from "../../../Context_Api/Context";
import "./Meetings-Drawer.scss";
import StreamDrawer from "../../../Components/Streaming/Stream_Drawer/StreamDrawer";
import MeetingsDrawers from "./MeetingsDrawers";
const MainMeetingsDrawers = () => {
  const context = useContext(CounselAppContext);
  const mainfunction = () => {
    switch (context.drawerType) {
      case "stream":
        return <StreamDrawer />;
      case "group":
        
        return <MeetingsDrawers />;
      default:
        break;
    }
  };
  return <div style={{ height: "100%" }}>{mainfunction()}</div>;
};

export default MainMeetingsDrawers;
