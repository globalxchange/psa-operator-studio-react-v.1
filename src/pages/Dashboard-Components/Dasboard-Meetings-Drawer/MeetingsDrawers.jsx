import React, { useContext, useState } from 'react';
import { CounselAppContext } from '../../../Context_Api/Context';

import { arrow } from '../../../static/images/images';
import './Meetings-Drawer.scss';
import LiveMeetingFirst from './Components/LiveMeeting/LiveMeetingFirst';
import LiveMeetingSecond from './Components/LiveMeeting/LiveMeetingSecond';

import ScheduleMeetingFirst from './Components/ScheduleMeeting/ScheduleMeetingFirst';
const MeetingsDrawers = () => {
  const context = useContext(CounselAppContext);
  const mainfunction = () => {
    switch (context.step) {
      case 'base':
        return (
          <>
            <LiveMeetingFirst />
            <br />
            <ScheduleMeetingFirst />
          </>
        );
      case 'live':
        return <LiveMeetingSecond />;
      case 'schedule':
        return <ScheduleMeetingFirst />;
      default:
        break;
    }
  };
  return (
    <div className='meetings-drawer-configure'>
      <div className='meetings-heading-configure'>
        <img
          src={arrow}
          alt=''
          onClick={(e) => {
            if (context.currentDrawer === 'live') {
              if (context.Secondstep === 1) {
                context.setSecondStep(0);
              } else if (context.Secondstep === 2) {
                context.setSecondStep(1);
              } else {
                context.setStep('base');
                context.setCurrentDrawer('base');
              }
            } else if (context.currentDrawer === 'schedule') {
              context.setCurrentDrawer('schedule');
              context.setStep('base');
            } else if (context.currentDrawer === 'base') {
              context.setDrawerOpen(false);
            }
          }}
        />
        <h3>Meeting Configuration</h3>
      </div>
      <div className='meetings-body-configure'>{mainfunction()}</div>
    </div>
  );
};

export default MeetingsDrawers;
