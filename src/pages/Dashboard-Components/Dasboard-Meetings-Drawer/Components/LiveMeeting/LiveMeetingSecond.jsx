import React, { useContext, useState, useEffect } from "react";
import {
  calender,
  copySVG,
  editRoom,
  ellipse,
  ellipsePink2,
  insta,
  linkedin,
  loginLoader,
  logo,
  people,
  smiley,
  snap,
  system,
  twitter,
  video,
  youtube,
} from "../../../../../static/images/images";
import "../../Meetings-Drawer.scss";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { CounselAppContext } from "../../../../../Context_Api/Context";
import Axios from "axios";
import { message, Select } from "antd";
import { Link } from "react-router-dom";
const { Option } = Select;

const LiveMeetingSecond = (props) => {
  const context = useContext(CounselAppContext);
  const [flag, setFlag] = useState(true);
  const [name, setName] = useState();
  const [diffRoom, setDiffRoom] = useState("");
  const [subDomain, setSubDomain] = useState(null);
  const [pulbicURL, setpulbicURL] = useState("");
  const newTab = {
    tabName: "Profile",
    detail: "new-tab",
    img: context.profilePic,
    dashboard: "New-Tab",
    type: "New-Tab",
    imgTab: context.profilePic,
  };
  const getDetails = async () => {
    setFlag(true);
    console.log("in get details");
    console.log(context.subDomainData);
    try {
      console.log("in axios");
      let body = {
        meetingName: name,
        meetingCreatedBy: context.profileData.psa_app_userId,
        subdomain: context.subDomainData[0].subdomain,
        stream_type: "group_call",
      };
      console.log(body);

      // if (edit) {
      //   body.customId = diffRoom;
      // }
      let res = await Axios.post(
        "https://ms.apimachine.com/api/createmeeting",
        body,
        {
          headers: {
            email: context.email,
            token: context.token,
          },
        }
      );
      console.log("call complete");
      if (res.data.success) {
        context.setPresentMeeting(res.data.createdata);
        setDiffRoom(res.data.createdata.meetingId);
        setpulbicURL(
          `https://${context.subDomainData[0].subdomain}.pulse.stream/joinMeetingLink/?id=${res.data.createdata.meetingId}`
        );
        context.setSecondStep(1);
        setFlag(false);
      } else {
        console.log("somthing went wrong");
        message.error(res.data.message.checksubdomain.body.message);
        setFlag(false);
      }
    } catch (e) {
      console.log(e.message);
      message.error(e.message);
      setFlag(false);
    }
  };
  const updateDetails = async () => {
    setFlag(true);
    console.log("in update details");
    try {
      console.log("in axios");
      let body = {
        updatedMeetingId: diffRoom,
      };

      let res = await Axios.put(
        `https://ms.apimachine.com/api/updatemeeting/${context.presentMeeting.meetingId}`,
        body,
        {
          headers: {
            email: context.email,
            token: context.token,
          },
        }
      );
      console.log("call complete");
      if (res.data.success) {
        context.setPresentMeeting(res.data.data.updateMeeting);
        setDiffRoom(res.data.data.updateMeeting.optionalMeetingId);
        setpulbicURL(
          `https://${context.subDomainData[0].subdomain}.pulse.stream/joinMeetingLink/?id=${res.data.data.updateMeeting.optionalMeetingId}&roomID=${res.data.data.updateMeeting.meetingId}`
        );
        context.setSecondStep(1);
        setFlag(false);
      } else {
        console.log("somthing went wrong");
        setFlag(false);
      }
    } catch (e) {
      console.log(e.message);
      message.error(e.message);
      setFlag(false);
    }
  };
  const mainFunction = () => {
    switch (context.Secondstep) {
      case 0:
        return (
          <>
            <h2>Name Your Meeting</h2>
            <div className="copy-1">
              <img src={logo} alt="copybtn" />
              <input
                style={{
                  width: "100%",
                  background: "white",
                  border: "none",
                  borderLeft: "0.5px solid #dfdde2",
                  fontSize: "20px",
                  padding: "10px",
                  borderRadius: "0px",
                  outline: "none",
                  textAlign: "initial",
                }}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              ></input>
            </div>
            <div className="buttons">
              <button
                style={{
                  backgroundColor: "#FE5777",
                  color: "white",
                  width: "40%",
                }}
                onClick={(e) => {
                  getDetails();
                }}
              >
                <img alt="" src={ellipse} />
                Create Meeting
              </button>
            </div>
          </>
        );
      case 1:
        return (
          <>
            <h2>Share Your Meeting Link With The World</h2>
            <div className="copy-1">
              <img src={logo} alt="" />
              <CopyToClipboard
                text={pulbicURL}
                onCopy={() => {
                  message.success("copied");
                }}
              >
                <button
                  style={{
                    border: "none",
                    color: "grey",
                    marginTop: "0px",
                    borderLeft: "0.5px solid #dfdde2",
                    fontSize: "0.7vw",
                    outline: "none",
                  }}
                  className="thelinkgen"
                >
                  <p>{pulbicURL}</p>
                  <img
                    src={copySVG}
                    alt="copybtn"
                    className="thecpybtn"
                    style={{ marginRight: "0px", marginTop: "auto" }}
                  />
                </button>
              </CopyToClipboard>
            </div>
            <div className="buttons">
              <button
                onClick={(e) => {
                  context.setSecondStep(2);
                }}
              >
                <img src={editRoom} alt="" />
                Edit
              </button>
              <Link tp="/main">
                <button
                  style={{ backgroundColor: "#FE5777", color: "white" }}
                  onClick={(e) => {
                    let obj = {
                      id: context.presentMeeting.meetingId,
                      token: context.token,
                      email: context.email,
                      profileData: context.profileData,
                      name: context.profileName,
                      subDomainData: context.subDomainData,
                      presentMeeting: context.presentMeeting,
                      host: true,
                      meetingLink: pulbicURL,
                    };
                    let objectString = JSON.stringify(obj);
                    let encodedString = btoa(objectString);

                    window.open(
                      `https://${context.subDomainData[0].subdomain}.pulse.stream/joinMeeting/?id=${encodedString}`,
                      "_blank"
                    ); //to open new page
                  }}
                >
                  <img alt="" src={ellipse} />
                  Start Meeting
                </button>
              </Link>
            </div>
          </>
        );
      case 2:
        return (
          <>
            <h2>Edit Your Meeting Link </h2>
            <div className="copy-1">
              <Select
                style={{ height: "100%" }}
                placeholder="Select "
                onChange={(e) => {
                  setSubDomain(e);
                }}
              >
                <Option value="sk" key="sk">
                  {context.presentMeeting.subdomain}
                </Option>
              </Select>
              <input readOnly disabled={true} value={"pulse.stream"}></input>

              <input
                value={diffRoom}
                onChange={(e) => {
                  setDiffRoom(e.target.value);
                }}
              ></input>
            </div>
            <div className="buttons">
              <button
                onClick={(e) => {
                  if (subDomain === "" || diffRoom === "") {
                    message.error(
                      "Please Make Sure You Enter Room ID and Select Sub Domain"
                    );
                  } else {
                    updateDetails();
                    context.setSecondStep(1);
                  }
                }}
              >
                <img src={calender} alt="" />
                Save Link
              </button>
            </div>
          </>
        );
      default:
        break;
    }
  };
  useEffect(() => {
    getsubDomains();
  }, []);
  const getsubDomains = async () => {
    try {
      let res = await context.getSubDomain();
      if (res.data.success) {
        console.log(res.data.payload);
        context.setSubDomainData(res.data.payload);
      } else {
        if (res.data.message === "SubDomain List is unavalilable") {
          message.error("Please Register a Sub Domain");
          context.handleAddTab(newTab);
          context.setSecondStep(0);
          context.setStep("base");
          context.setDrawerOpen(false);
          console.log("yes");
        } else {
          message.error(res.data.message);
        }
      }
      setFlag(false);
    } catch (e) {
      if (e.message === "SubDomain List is unavalilable") {
        setFlag(false);
      }
    }
  };
  return (
    <>
      <div className="div-1">
        <div className="div-top">
          <h3>
            <img src={smiley} alt="" />
            Suprise Meeting
          </h3>
          <button>
            <img src={ellipsePink2} alt="" />
            Go Live now
          </button>
        </div>
        <p>
          Need to have a quick conversation with a couple of your associates. No
          problem. Just call a #SupriseMeeting
        </p>
        <div>
          <button>
            <img src={video} alt="" />
            Video
          </button>
          <button>
            <img src={people} alt="" />
            Multi Participant
          </button>
          <button>
            <img src={system} alt="" />
            Auto Record
          </button>
        </div>
      </div>
      <div className="div-2">
        {flag ? (
          <div
            style={{
              height: "2vh",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img
              src={loginLoader}
              alt="Loader"
              style={{ alignSelf: "center", height: "15px" }}
            />
          </div>
        ) : (
          <>
            <h3>#TheTimeIsNow</h3>
            {mainFunction()}
          </>
        )}
      </div>
      <div className="div-3">
        <p>Invite Your Guests Via These Platforms</p>
        <div>
          {buttons.map((item) => {
            return <img style={{ height: "50px" }} src={item} alt={item} />;
          })}
        </div>
      </div>
    </>
  );
};

const buttons = [insta, youtube, snap, linkedin, twitter];

export default LiveMeetingSecond;
