import React, {useContext} from "react";
import "./app.scss";
import AmerImage from "../../static/images/americon.png";
import {CounselAppContext} from "../../Context_Api/Context";
import {Link} from "react-router-dom";

function Gamer({history}) {
  const context = useContext(CounselAppContext);

  const backbtn = (e) => {
    e.preventDefault();
    history.push("/");
  };
  return (
    <div className="login-ag">
      <div className="form-wrapper">
        <img src={AmerImage} style={{width: "20rem"}} alt="trade_img" />
        <p className="thetext1 sty" style={{marginTop: "40px"}}>
          Amer.Stream Admin Dashboard Is Under Construction{" "}
        </p>
        <p className="thetext1 sty">Please Try Again Later</p>
        <div className="thebuttons">
          <button type="submit" className="thebtn-sty btn3" onClick={backbtn}>
            Back To All Apps
          </button>
          &nbsp;&nbsp;
          <button type="submit" className="thebtn-sty btn4">
            <Link
              style={{color: "#463ba1"}}
              to="/login"
              onClick={() => context.login("", "", "")}
            >
              Log out
            </Link>
          </button>
        </div>
      </div>
    </div>
  );
}

export default Gamer;
