import React, {useContext} from "react";
import "./app.scss";
import TradeImage from "../../static/images/tradeicon.png";
import {CounselAppContext} from "../../Context_Api/Context";
import {Link} from "react-router-dom";

function Trade({history}) {
  const context = useContext(CounselAppContext);

  const backbtn = (e) => {
    e.preventDefault();
    history.push("/");
  };
  return (
    <div className="login-ag">
      <div className="form-wrapper">
        <img src={TradeImage} style={{width: "20rem"}} alt="trade_img" />
        <p className="thetext1" style={{marginTop: "40px"}}>
          Trade.Stream Admin Dashboard Is Under Construction{" "}
        </p>
        <p className="thetext1">Please Try Again Later</p>
        <div className="thebuttons">
          <button type="submit" className="thebtn-sty btn1" onClick={backbtn}>
            Back To All Apps
          </button>
          &nbsp;&nbsp;
          <button type="submit" className="thebtn-sty btn2">
            <Link to="/login" onClick={() => context.login("", "", "")}>
              Log out
            </Link>
          </button>
        </div>
      </div>
    </div>
  );
}

export default Trade;
