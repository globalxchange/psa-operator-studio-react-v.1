import React, {useContext} from "react";
import PulseImage from "../../static/images/PulseLoginImage.png";
import {Row, Col, Card} from "antd";
import "./app.scss";
import MeetingImg from "../../static/images/meetingimg.png";
import TradeImg from "../../static/images/tradeimg.png";
import AmerImg from "../../static/images/amerimg.png";
import NewImg from "../../static/images/newimg.png";
import Desk from "../../static/images/desk2.png";

import {CounselAppContext} from "../../Context_Api/Context";
import {Redirect} from "react-router-dom";

function SelectApp({history}) {
  const {email} = useContext(CounselAppContext);
  const changeHandler = (string) => {
    switch (string) {
      case "desk":
        history.push("/deskstream");
        break;
      case "meetings":
        history.push("/appconfig");
        break;
      case "trade":
        history.push("/tradestream");
        break;
      case "gamer":
        history.push("/gamerstream");
        break;

      default:
        break;
    }
  };

  return (
    <div className="login-ag">
      {localStorage.getItem("CounselAppLoginAccount") === "" ? (
        <Redirect to="/landing" />
      ) : (
        ""
      )}{" "}
      {/* {email === undefined ? <Redirect to="/landing" /> : ''}{' '} */}
      <div className="form-wrapper">
        <img src={PulseImage} style={{width: "25rem"}} alt="no_img" />
        <p className="thetext">
          Which One Of Your Apps Do You Want To Interact With?
        </p>
        <div className="theimg">
          <img
            className="theappimg"
            src={MeetingImg}
            alt="meeting-img"
            onClick={(e) => {
              changeHandler("meetings");
            }}
          />
          <img
            className="theappimg"
            src={TradeImg}
            alt="trade-img"
            onClick={(e) => {
              changeHandler("trade");
            }}
          />
          <img
            className="theappimg"
            src={AmerImg}
            alt="amer-img"
            onClick={(e) => {
              changeHandler("gamer");
            }}
          />
          <img
            className="theappimg"
            src={Desk}
            alt="amer-img"
            style={{border: "0.5px solid #EBEBEB", padding: "48px 67px"}}
            onClick={(e) => {
              changeHandler("desk");
            }}
          />

          <img className="theappimg" src={NewImg} alt="new-img" />
        </div>
      </div>
    </div>
  );
}

export default SelectApp;
