import React from "react";
import gamericon from "../../static/images/americon.png"
import meetingicon from "../../static/images/meetingicon.png"
import tradeicon from "../../static/images/tradeicon.png"
import { Link } from 'react-router-dom';
import PulseTitleImg from "../../static/images/PulseTabIcon.png"




function Switchapps(){
    return (
    <div> 
    <p className = "header-profile1"><img src = {PulseTitleImg} className = "img-width" alt = "pulse-img"/>&nbsp;&nbsp;Streaming Apps</p>
    <p className = "under-line1"></p>
    <input type = "text" name = "searchbar" placeholder = "search all apps" className = "searchsty"/>
    <p className = "under-line1"></p>
    <img src = {meetingicon} alt = "meeting-icon" className = "the-account"/>
    <p className = "under-line1"></p>
    <Link to="/tradestream"><img src = {tradeicon} alt = "trade-icon" className = "the-account"/></Link>
    <p className = "under-line1"></p>
    <Link to="/gamerstream"><img src = {gamericon} alt = "gamer-icon" className = "the-account"/></Link>

    <p className = "under-line"></p></div>
    )
}

export default Switchapps;