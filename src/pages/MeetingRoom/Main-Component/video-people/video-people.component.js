import React, { useEffect } from 'react';
import './video-people.style.scss';
import Draggable from 'react-draggable';

const VideoPeopleComponent = ({
  people,
  setOptions,
  options,
  bodyConnections,
}) => {
  useEffect(() => {
    console.log('video ', people);
  }, []);
  return (
    <Draggable>
      <div
        className={`terminal-video-people ${
          options ? 'show-options' : 'hide-options'
        }`} 
      >
        <div
          className='terminal-video-people-body'
          id='terminal-body-others'
          ref={bodyConnections}
        ></div>
      </div>
    </Draggable>
  );
};
export default VideoPeopleComponent;
