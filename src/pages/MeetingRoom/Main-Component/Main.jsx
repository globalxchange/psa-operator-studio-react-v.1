import React, {useEffect, useContext, useState, useRef} from "react";
import io from "socket.io-client";
import * as mediasoupClient from "mediasoup-client";
import "./video.style.scss";
import {CounselAppContext} from "../../../Context_Api/Context";
import VideoOptionComponent from "./video-options/video-option.component";
import VideoPeopleComponent from "./video-people/video-people.component";
import {message, Radio} from "antd";
import Axios from "axios";
import {Redirect} from "react-router-dom";
import Draggable from "react-draggable";

// import { getSeekableBlob } from 'recordrtc';

import Modal from "antd/lib/modal/Modal";
// import Group from 'antd/lib/input/Group';
let socket;

const Main = React.memo((props) => {
  const context = useContext(CounselAppContext);
  let recordedBlobs = [];

  // const endpoint = "https://localhost:6016/";
  const endpoint = "https://msbackend.pulse.stream";

  // -------------------------------------------------------states----------------------------------------------------------------//
  const [options, setOptions] = useState(true);
  const [recordState, setReccordState] = useState(false);
  const [visible, setVisible] = useState(false);
  const [camera, setCamera] = useState(true);
  const [cameraOn, setCameraOn] = useState(true);
  const [mic, setMic] = useState(true);
  const [size, setSize] = useState({height: 0, width: 0});
  const bodyConnections = useRef(null);
  const [host, setHost] = useState(context.host);
  const [finalAudioType, setFinalAudioType] = useState(null);
  const [finalVideoType, setFinalVideoType] = useState(null);
  const [first, setFirst] = useState(false);
  const [meetingDetails, setMeetingDetails] = useState("");
  const [Name, setName] = React.useState(context.Name);
  const [audioTypes, setAudioTypes] = useState([]);
  const [videoTypes, setVideoTypes] = useState([]);
  const [optionRecord, setOptionRecord] = useState(false);

  const [roomID, setRoomID] = React.useState(context.meetingRoom);
  const [rc, setRC] = useState("");
  // const [recordButton, setRecordButton] = useState(false);
  const [ModalClick, setModalClick] = useState(null);
  const Recorder = context.Name === "meetingsstreamrecorder" ? true : false;
  const profiledata = {
    user_psa_id: context.profileData.psa_app_userId,
    psa_app_id: "ea422e04-d561-4f3f-951a-db183b699bf0",
    type: "grvc",
    username: context.Name,
  };
  const mainReferrenceref = useRef(null);
  const userReference = useRef(null);
  const vc = useRef();
  //------------------------------------------------------------------------------------------------------------------------------//

  //-----------------------------------------Functions----------------------------------------------------------------------------//
  const successCallback = () => {
    console.log("yes");
  };
  const redirectFunction = () => {
    console.log("----in redirect-----");
    window.close();
    if (context.coHosts !== "viewer") {
      return <Redirect to="/appconfig" />;
    } else {
      window.location.href = "https://studio.pulse.stream";
    }
  };
  const exitCall = async () => {
    let data = {
      room_id: roomID,
      type: "grvc",
    };
    let res = Axios.post(
      "https://ms.apimachine.com/api/getcallreceipts",
      data,
      {
        headers: {
          token: context.token,
          email: context.email,
        },
      }
    );
    console.log(res.data);
  };
  const getTrackDetails = async () => {
    return new Promise(async (resolve, reject) => {
      const mediaConstraintsAudio = {
        audio: {
          autoGainControl: false,
          echoCancellation: true,
          latency: 0,
          noiseSuppression: false,
          sampleRate: 48000,
          sampleSize: 16,
          volume: 1.0,
        },
      };
      const mediaConstraintsVideo = {
        video: {
          width: {
            min: 640,
            ideal: 1920,
            max: 3840,
          },
          height: {
            min: 400,
            ideal: 1080,
            max: 2160,
          },
          frameRate: {
            min: 15,
            max: 120,
          },
        },
      };
      let detailsAudio = await navigator.mediaDevices.getUserMedia(
        mediaConstraintsAudio
      );
      let detailsVideo = await navigator.mediaDevices.getUserMedia(
        mediaConstraintsVideo
      );

      let audioTracks = detailsAudio.getAudioTracks();
      console.log(audioTracks);
      let resAudio = audioTracks.map((item, i) => {
        return {key: i, name: item.label};
      });
      console.log("-------audio----devices-----");
      console.log(resAudio);
      setAudioTypes(resAudio);
      let videoTracks = detailsVideo.getVideoTracks();
      console.log(videoTracks);

      let resVideo = videoTracks.map((item, i) => {
        return {key: i, name: item.label};
      });
      console.log("-------video----devices-----");
      console.log(resVideo);
      setVideoTypes(resVideo);
      setVisible(true);
      resolve();
    });
  };
  const ModalCall = () => {
    console.log("in Modal call");
    console.log(profiledata);
    let room = new RoomClient(
      profiledata,
      socket,
      setReccordState,
      recordState,
      recordedBlobs,
      finalAudioType,
      finalVideoType,
      exitCall,
      host,
      mediasoupClient,
      roomID,
      Name,
      successCallback,
      bodyConnections,
      mainReferrenceref,
      userReference,
      Recorder,
      redirectFunction,
      setCamera,
      setFirst,
      first
    );

    setRC(room);
    socket.on(
      `joined_user_data${roomID}^^^^${profiledata.psa_app_id}`,
      (data) => {
        if (profiledata.user_psa_id === data.userid) {
        } else {
          setTimeout(() => {
            message.success(`${data.username} joined`);
          }, 2000);
        }
      }
    );
    socket.on(
      `left_user_data${roomID}^^^^${profiledata.psa_app_id}`,
      (data) => {
        message.info(`${data.username} Left`);
      }
    );

    setTimeout(() => {
      if (!Recorder) {
        room.produce(
          "videoType",
          userReference,
          rc.device,
          "camera",
          finalVideoType,
          finalAudioType
        );
        room.produce(
          "audioType",
          userReference,
          rc.device,
          "mic",
          finalVideoType,
          finalAudioType
        );
      } else {
        room.produce(
          "screenType",
          userReference,
          rc.device,
          "screen",
          finalVideoType,
          finalAudioType
        );
      }
      let conf_details = {
        psa_app_id: profiledata.psa_app_id,
        type: profiledata.type,
        room_id: context.meetingRoom,
      };
      socket.emit("conference_details", conf_details, async (data) => {
        setMeetingDetails(data.payload);
        console.log("------------conference details-------------");
        console.log(data.payload);
        context.setHostName(data.payload.username);
      });
    }, 4500);
  };

  useEffect(() => {
    if (recordState) {
      startRecordHit();
    }
  }, [recordState]);

  const startRecordHit = () => {
    let data = {
      psa_app_id: profiledata.psa_app_id,
      brain_user_id: context.brainDetails,
      type: "grvc",
    };
    rc.recordFunction(data); // mediaRecorder.start(5500);
    message.success("Recording started");
    setOptionRecord(true);
  };
  const record = (recordType) => {
    console.log(recordType);
    console.log("in record funtion");
    switch (recordType) {
      case "start_recording":
        console.log("in start" + context.brainDetails);
        rc.produceForScreenRecord(rc.device);

        break;
      case "stop_recording":
        console.log("in stop");
        // last_call = true;
        // mediaRecorder.stop();
        rc.stopRecording();
        message.success("Recording stoped");
        setReccordState(false);
        setOptionRecord(false);

        break;
      default:
        break;
    }
  };

  //----------------------------------------------------------------------------------------------------------------------------//

  //-------------------------------------use Effects----------------------------------------------------------------------------//
  useEffect(() => {
    console.log(context.Name + context.meetingRoom);
    socket = io(endpoint);
    socket.request = function request(type, data = {}) {
      return new Promise((resolve, reject) => {
        socket.emit(type, data, (data) => {
          if (data.error) {
            reject(data.error);
          } else {
            resolve(data);
          }
        });
      });
    };

    getTrackDetails();
    return () => {
      socket.request("disconnect");
      socket.off();
    };
  }, [endpoint, context.Name, context.meetingRoom]);

  useEffect(() => {
    const handleBeforeUnload = (event) => event.preventDefault();
    window.addEventListener("beforeunload", handleBeforeUnload);
    return () => window.removeEventListener("beforeunload", handleBeforeUnload);
  }, []);

  useEffect(() => {
    if (ModalClick === true) {
      ModalCall();
    }
  }, [ModalClick]);

  //---------------------------------------------------------------------------------------------------------------------------//

  return (
    <React.Fragment>
      <div
        className={
          context.chatOpen && window.innerWidth < 600
            ? "_hidden"
            : "terminal-live-stream"
        }
      >
        <div className="terminal-live-stream-video">
          <div ref={vc} id="video-container" className="terminal-video">
            <video
              width={size.width}
              height={size.height}
              className="bg-image"
              autoPlay
              id="video"
              ref={mainReferrenceref}
            />
            <Draggable>
              <video
                width={"100px"}
                height={"100px"}
                className="bg-image-profile"
                autoPlay
                id="video"
                onDoubleClick={(e) => {
                  mainReferrenceref.current.srcObject =
                    userReference.current.srcObject;
                }}
                ref={userReference}
              />
            </Draggable>
            {/* {contet ? (
              <div className='buttons'>
                <button
                  className={recordButton ? '' : 'grey'}
                  disabled={recordButton ? false : true}
                  onClick={(e) => {
                    setRecordButton(false);
                    record('stop-recording');
                  }}
                >
                  Stop Recording
                </button>
                <button
                  className={!recordButton ? '' : 'grey'}
                  disabled={!recordButton ? false : true}
                  onClick={(e) => {
                    setRecordButton(true);
                    record('start_recording');
                    // active(userReference.current.id);
                  }}
                >
                  Start Recording{' '}
                </button>{' '}
              </div> 
            ) : (
              ''
            )} */}

            <VideoOptionComponent
              first={first}
              setFirst={setFirst}
              setCameraOn={setCameraOn}
              socket={socket}
              roomReference={rc}
              cameraOn={cameraOn}
              camera={camera}
              setCamera={setCamera}
              reference={mainReferrenceref}
              mic={mic}
              setOptions={setOptions}
              options={options}
              setMic={setMic}
              finalVideoType={finalVideoType}
              finalAudioType={finalAudioType}
              record={record}
              optionRecord={optionRecord}
              setOptionRecord={setOptionRecord}
            />
            <VideoPeopleComponent
              setOptions={setOptions}
              bodyConnections={bodyConnections}
              options={options}
            />
          </div>
        </div>
        <Modal
          title="Device Selection"
          visible={visible}
          onOk={(e) => {
            if (finalAudioType === null || finalVideoType === null) {
              return;
            } else {
              setVisible(false);
              setModalClick(true);
            }
          }}
        >
          <div>
            <p>Select Video Source</p>
            <div>
              <Radio.Group
                onChange={(e) => {
                  console.log(
                    "---------------video source------------------" +
                      e.target.value
                  );
                  setFinalVideoType(e.target.value);
                }}
              >
                {videoTypes.map((item) => {
                  return <Radio value={item.key}>{item.name}</Radio>;
                })}
              </Radio.Group>
            </div>
          </div>
          <br></br>
          <div>
            <p>Select Audio Source</p>
            <div>
              <Radio.Group
                onChange={(e) => {
                  console.log(
                    "---------------audio source------------------" +
                      e.target.value
                  );

                  setFinalAudioType(e.target.value);
                }}
              >
                {audioTypes.map((item) => {
                  return <Radio value={item.key}>{item.name}</Radio>;
                })}
              </Radio.Group>
            </div>
          </div>
        </Modal>
      </div>
    </React.Fragment>
  );
});

export default Main;
class RoomClient {
  constructor(
    profiledata,
    socket,
    setReccordStates,
    recordState,
    recordedBlobs,
    finalAudioType,
    finalVideoType,
    exitCall,
    host,
    mediasoupClient,
    roomID,
    Name,
    successCallback,
    bodyConnections,
    mainReferrence1,
    userReferencef,
    Recorders,
    redirectFunction,
    setCameras,
    setFirsts,
    firsts
  ) {
    console.log("-------------------constructor-------------");
    console.log(socket);
    this.socket = socket;

    this.Host = host;
    // this.fileName = Date.now();
    console.log(Date.now());
    this.setReccordState = setReccordStates;
    this.finalAudioType = finalAudioType;
    this.finalVideoType = finalVideoType;
    this.MeetingList = [];
    this.producer = "";
    this.first = firsts;
    this.userReference = userReferencef;
    this.profileData = profiledata;
    this.recordState = recordState;
    this.setFirst = setFirsts;
    this.setCamera = setCameras;
    this.name = Name;
    this.exitCall = exitCall;
    this.Recorder = Recorders;
    this.redirectFunction = redirectFunction;
    this.mainReferrence = mainReferrence1;
    this.mediasoupClient = mediasoupClient;
    this.bodyConnections = bodyConnections;
    this.producerTransport = null;
    this.consumerTransport = null;
    this.device = null;
    this.room_id = roomID;
    console.log(roomID);

    console.log(profiledata);
    // this.fileName = Date.now();

    this.consumers = new Map();
    this.producers = new Map();
    this.producerLabel = new Map();
    this.currentType = "";
    console.log(this.room_id);

    this.createRoom(this.room_id).then(
      async function () {
        await this.join(this.room_id);
        this.initSockets();
        // successCallback(true);
      }.bind(this)
    );
  }

  ////////// INIT /////////

  async createRoom(room_id) {
    console.log(
      "--------------------------- Step-1 ---------------------------------"
    );
    // console.log(this.socket);
    let data = {
      room_id: room_id,
      ...this.profileData,
    };

    console.log("-----------------data print---------------");
    console.log(data);
    let res = await this.socket.request("createRoom", data);
    console.log(res);
    console.log("---------------result-------------------" + res);
  }

  async join(room_id) {
    console.log(
      "--------------------------- Step-2 ---------------------------------" +
        room_id
    );

    try {
      let e = await this.socket.request("join", {
        room_id,
        ...this.profileData,
      });
      console.log(e);
      const data = await this.socket.request("getRouterRtpCapabilities");
      // console.log(data);
      let device = await this.loadDevice(data);
      this.device = device;
      // console.log(device);
      console.log("--------------------device loaded-----------------");
      await this.initTransports(device);
      console.log("--------------------transports updated------------");
      this.socket.emit("getProducers");
    } catch (e) {
      console.log(e);
      if (e === "room does not exist") {
        console.log("room dosent exist");
        // this.setReloadFlag(true);
        // this.setFlag(true);
      }
    }
  }

  async loadDevice(routerRtpCapabilities) {
    // console.log(this.mediasoupClient);
    let device;
    try {
      device = new this.mediasoupClient.Device();
    } catch (error) {
      if (error.name === "UnsupportedError") {
        console.error("browser not supported");
      }
      console.error(error);
    }
    await device.load({
      routerRtpCapabilities,
    });
    return device;
  }

  async initTransports(device) {
    console.log(
      "--------------------------- Step-3 ---------------------------------"
    );
    // init producerTransport
    {
      const data = await this.socket.request("createWebRtcTransport", {
        forceTcp: false,
        rtpCapabilities: device.rtpCapabilities,
      });
      console.log(
        "------------------------- producer transport loaded--------------------------"
      );
      if (data.error) {
        console.error(data.error);
        return;
      }

      this.producerTransport = device.createSendTransport(data);
      // console.log(this.producerTransport);
      this.producerTransport.on(
        "connect",
        async function ({dtlsParameters}, callback, errback) {
          this.socket
            .request("connectTransport", {
              dtlsParameters,
              transport_id: data.id,
            })
            .then(callback)
            .catch(errback);
        }.bind(this)
      );

      this.producerTransport.on(
        "produce",
        async function ({kind, rtpParameters}, callback, errback) {
          try {
            console.log(this.Recorder);
            const {producer_id} = await this.socket.request("produce", {
              producerTransportId: this.producerTransport.id,
              kind,
              rtpParameters,
              dontAdd: this.Recorder ? true : false,
              ...this.profileData,
              producer_type: this.currentType,
            });
            callback({
              id: producer_id,
            });
          } catch (err) {
            errback(err);
          }
        }.bind(this)
      );

      this.producerTransport.on(
        "connectionstatechange",
        function (state) {
          switch (state) {
            case "connecting":
              console.log("connecting producer");
              break;
            case "connected":
              console.log("connected producer transport");
              break;
            case "failed":
              this.producerTransport.close();
              break;
            default:
              break;
          }
        }.bind(this)
      );
    }

    // init consumerTransport
    {
      const data = await this.socket.request("createWebRtcTransport", {
        forceTcp: false,
      });
      if (data.error) {
        console.error(data.error);
        return;
      }

      // only one needed
      this.consumerTransport = device.createRecvTransport(data);
      this.consumerTransport.on(
        "connect",
        function ({dtlsParameters}, callback, errback) {
          this.socket
            .request("connectTransport", {
              transport_id: this.consumerTransport.id,
              dtlsParameters,
            })
            .then(callback)
            .catch(errback);
        }.bind(this)
      );

      this.consumerTransport.on(
        "connectionstatechange",
        async function (state) {
          switch (state) {
            case "connecting":
              break;

            case "connected":
              console.log("connected consumer transport");
              break;

            case "failed":
              this.consumerTransport.close();
              break;

            default:
              break;
          }
        }.bind(this)
      );
      console.log(
        "------------------------- consumer transport loaded--------------------------"
      );
    }
  }

  initSockets() {
    console.log(
      "--------------------------- Step-4 ---------------------------------"
    );

    this.socket.on(
      "consumerClosed",
      function ({consumer_id}) {
        console.log("closing consumer:", consumer_id);
        this.removeConsumer(consumer_id);
        // if (this.consumers.size < 1) {
        //   console.log("exiting");
        //   this.exit(true);
        // }
      }.bind(this)
    );

    this.socket.on(
      "newProducers",
      async function (data) {
        console.log("new producers", data);
        let sendData = {
          psa_app_id: this.profileData.psa_app_id,
          type: this.profileData.type,
          room_id: this.room_id,
          user_psa_id: this.profileData.user_psa_id,
        };
        this.socket.emit("conference_participant_list", sendData, (data) => {
          console.log("-----new participants list----");
          console.log(data);
          this.MeetingList = data.payload;
        });
        for (let {producer_id, dontAddProducer} of data) {
          if (this.producers.get(producer_id)) {
            console.log(
              "-------------returning due to produce found-----------------"
            );
            return;
          } else {
            if (dontAddProducer) {
              return;
            } else {
              setTimeout(async () => {
                await this.consume(producer_id);
              }, 1000);
            }
          }
        }
      }.bind(this)
    );

    this.socket.on("activeChange", async (producer_id) => {
      console.log(
        `---------------change in active stream with producer-id + ${producer_id}--------------`
      );
      console.log(this.consumers);
      let array = Array.from(this.consumers, ([name, value]) => ({
        name,
        value,
      }));
      console.log(array);
      let requiredConsumer = array.find(
        (item) => item.value.producerId === producer_id
      );
      let secondaryConsumer = "";
      if (array.length > 2) {
        secondaryConsumer = array.find(
          (item) =>
            item.value.producerId !== producer_id &&
            item.value.consumer.kind !== "audio"
        );
      }
      console.log(requiredConsumer);
      console.log(secondaryConsumer);
      if (this.host === false) {
        this.mainReferrence.current.srcObject =
          requiredConsumer.value.stream.stream;
      }
    });
  }

  //////// MAIN FUNCTIONS /////////////

  async produce(
    type,
    reference,
    deviceId = null,
    producer_type,
    finalAudioType,
    finalVideoType
  ) {
    console.log(producer_type);
    this.currentType = producer_type;
    let record = this.Recorder;
    console.log(
      "-----------------------in produce----------------------------"
    );
    console.log(record);
    let mediaConstraints = {};
    console.log(this.producerTransport);
    let audio = false;
    let screen = false;
    switch (type) {
      case mediaType.audio:
        mediaConstraints = {
          audio: {
            autoGainControl: false,
            echoCancellation: true,
            latency: 0,
            noiseSuppression: false,
            sampleRate: 48000,
            sampleSize: 16,
            volume: 1.0,
          },
          video: false,
        };
        audio = true;
        break;
      case mediaType.video:
        mediaConstraints = {
          video: {
            width: {
              min: 640,
              ideal: 1920,
              max: 3840,
            },
            height: {
              min: 400,
              ideal: 1080,
              max: 2160,
            },
            frameRate: {
              min: 15,
              max: 120,
            },
            deviceId: deviceId,
          },
        };
        break;
      case mediaType.screen:
        mediaConstraints = false;
        screen = true;
        audio = false;
        break;
      default:
        return;
    }
    console.log(this.device);
    if (!this.device.canProduce("video") && !audio) {
      console.error("cannot produce video");
      return;
    }
    if (this.producerLabel.has(type)) {
      console.log("producer already exists for this type " + type);
      return;
    }
    console.log("mediacontraints:", mediaConstraints);

    let stream;
    try {
      stream = screen
        ? await navigator.mediaDevices.getDisplayMedia({
            video: true,
            audio: {
              autoGainControl: false,
              echoCancellation: false,
              googAutoGainControl: false,
              noiseSuppression: false,
            },
          })
        : await navigator.mediaDevices.getUserMedia(mediaConstraints);
      // prompt();
      let track;

      if (audio) {
        console.log("-------in producer audio---" + finalAudioType);
        track = stream.getAudioTracks()[finalAudioType];
      } else {
        console.log("-------in producer audio---" + finalVideoType);
        track = stream.getVideoTracks()[finalVideoType];
      }

      let params = {
        track,
      };
      if (screen) {
        console.log("-------------coz im recorder----------------");
        params = {track: stream.getVideoTracks()[0]};
      }
      if (!audio && !screen) {
        params.encodings = [
          {
            rid: "r0",
            maxBitrate: 100000,
            //scaleResolutionDownBy: 10.0,
            scalabilityMode: "S1T3",
          },
          {
            rid: "r1",
            maxBitrate: 300000,
            scalabilityMode: "S1T3",
          },
          {
            rid: "r2",
            maxBitrate: 900000,
            scalabilityMode: "S1T3",
          },
        ];
        params.codecOptions = {
          videoGoogleStartBitrate: 1000,
        };
      }
      let producer = "";
      let audioProducer = "";

      console.log(this.producerTransport);
      producer = await this.producerTransport.produce(params);
      this.socket.request("set_producer_type", {
        producer_id: producer.id,
        producer_type,
      });
      let audioRestricted = false;
      if (screen) {
        if (stream.getAudioTracks()[0]) {
          console.log("stream audios", stream.getAudioTracks());
          let params = {track: stream.getAudioTracks()[0]};
          audioProducer = await this.producerTransport.produce(params);
          this.socket.request("set_producer_type", {
            producer_id: audioProducer.id,
            producer_type: "screen_audio",
          });
        } else {
          audioRestricted = true;
        }
      }
      this.producers.set(producer.id, producer);

      let elem;
      console.log(stream);

      if (!audio) {
        if (screen) {
          if (record) {
            this.mainReferrence.current.id = producer.id;
          } else {
            console.log("in screen block");
            this.mainReferrence.current.srcObject = stream;
            this.mainReferrence.current.id = producer.id;
          }
        } else {
          reference.current.srcObject = stream;
          if (!screen) {
            this.mainReferrence.current.srcObject = stream;
            this.mainReferrence.current.playsinline = false;
            this.mainReferrence.current.autoplay = true;
            this.mainReferrence.current.id = producer.id;
          }
          reference.current.playsinline = false;
          reference.current.autoplay = true;
          reference.current.id = producer.id;
          reference.current.className = "bg-image-profile";
        }
      }
      if (screen) {
        this.producerLabel.set(type, producer.id);
        if (!audioRestricted) {
          this.producerLabel.set("screenAudio", audioProducer.id);
        }
      } else {
        console.log("--------------- general producer----------------------");
        this.producerLabel.set(type, producer.id);
      }
      console.log(this.producerLabel);
      producer.on("trackended", () => {
        this.closeProducer(type);
      });

      producer.on("transportclose", () => {
        console.log("producer transport close");
        if (!audio) {
          elem.srcObject.getTracks().forEach(function (track) {
            track.stop();
          });
          elem.parentNode.removeChild(elem);
        }
        this.producers.delete(producer.id);
      });

      producer.on("close", () => {
        console.log("closing producer");
        if (!audio) {
          elem.srcObject.getTracks().forEach(function (track) {
            track.stop();
          });
          elem.parentNode.removeChild(elem);
        }
        this.producers.delete(producer.id);
      });

      if (screen && !audioRestricted) {
        audioProducer.on("trackended", () => {
          this.closeProducer(type);
        });
        audioProducer.on("transportclose", () => {
          console.log("producer transport close");
          if (!audio) {
            elem.srcObject.getTracks().forEach(function (track) {
              track.stop();
            });
            elem.parentNode.removeChild(elem);
          }
          this.producers.delete(producer.id);
        });
        audioProducer.on("close", () => {
          console.log("closing producer");
          if (!audio) {
            elem.srcObject.getTracks().forEach(function (track) {
              track.stop();
            });
            elem.parentNode.removeChild(elem);
          }
          this.producers.delete(producer.id);
        });
      }
    } catch (err) {
      console.log(err);
      if (err.message.includes("Permission denied")) {
        console.log(this.first);
        this.setCamera(false);
        this.setFirst(false);
        console.log(this.first);
        if (this.Recorder) {
          this.produce(type, reference, deviceId);
        } else {
          throw new Error("ulala");
        }
      }
    }
  }
  async consume(producer_id) {
    this.getConsumeStream(producer_id).then(
      function ({consumer, stream, kind}) {
        let consumerObj = {consumer};
        consumerObj.stream = {stream};
        consumerObj.kind = kind;
        consumerObj.producerId = producer_id;
        this.consumers.set(consumer.id, consumerObj);
        let selected_item;
        console.log(producer_id);
        console.log(this.MeetingList);
        for (let item of this.MeetingList) {
          if (item.producers === undefined) {
          } else {
            console.log("-----in producers exits------");
            console.log(producer_id);
            console.log(item);
            let res = item.producers.find(
              (e) => e.producer_id === producer_id && e.end_ts === null
            );
            console.log(res);
            if (res) {
              selected_item = item;
            } else {
              console.log("=======in item but producer not found-----------");
            }
          }
        }
        if (!selected_item) {
          return;
        }
        console.log(selected_item);
        console.log(
          "---consumer-id---" +
            consumer.id +
            "--------producer_id---" +
            producer_id
        );
        let elem;
        if (kind === "video") {
          elem = document.createElement("div");
          elem.className = "terminal-video-individual-outer";
          let video = document.createElement("video");
          video.srcObject = stream;
          video.src = stream;
          video.id = consumer.id;
          video.producer_id = producer_id;
          elem.id = consumer.id + "one";
          video.playsinline = false;
          video.autoplay = true;
          let appe = this.mainReferrence;
          video.onclick = () => {
            appe.current.srcObject = stream;
          };
          video.className = "terminal-video-individual";
          let p = document.createElement("p");
          p.className = "terminal-video-p";
          p.textContent = selected_item.username;
          elem.appendChild(p);
          elem.appendChild(video);
          // this.userReference.current.appendChild(elem);
          this.bodyConnections.current.appendChild(elem);
        } else {
          let elem = document.createElement("div");
          let video = document.createElement("video");
          video.srcObject = stream;
          video.src = stream;
          elem.id = consumer.id + "one";
          video.producer_id = producer_id;
          video.id = consumer.id;
          video.playsinline = false;
          video.autoplay = true;
          video.className = "none";
          elem.appendChild(video);
          this.bodyConnections.current.appendChild(elem);
        }
        consumer.on(
          "trackended",
          function () {
            this.removeConsumer(consumer.id, producer_id);
          }.bind(this)
        );
        consumer.on(
          "transportclose",
          function () {
            this.removeConsumer(consumer.id, producer_id);
          }.bind(this)
        );
      }.bind(this)
    );
  }
  async produceForScreenRecord(deviceId = null) {
    this.Recorder = true;
    this.currentType = "screen_record_video";
    console.log(
      "-----------------------in screen record produce----------------------------"
    );
    // this.Recorder = true
    let new_stream;
    try {
      let stream = await navigator.mediaDevices.getDisplayMedia({
        video: true,
        audio: {
          autoGainControl: false,
          echoCancellation: false,
          googAutoGainControl: false,
          noiseSuppression: false,
        },
      });
      let audio_stream = await navigator.mediaDevices.getUserMedia({
        audio: {
          autoGainControl: false,
          echoCancellation: true,
          latency: 0,
          noiseSuppression: false,
          sampleRate: 48000,
          sampleSize: 16,
          volume: 1.0,
        },
      });

      new_stream = new MediaStream();
      new_stream.addTrack(stream.getVideoTracks()[0]);

      const audioContext = new AudioContext();
      if (stream.getAudioTracks()[0]) {
        const voice = audioContext.createMediaStreamSource(audio_stream);
        const desktop = audioContext.createMediaStreamSource(stream);
        const destination = audioContext.createMediaStreamDestination();

        const desktopGain = audioContext.createGain();
        const voiceGain = audioContext.createGain();

        desktopGain.gain.value = 0.7;
        voiceGain.gain.value = 0.9;

        voice.connect(voiceGain).connect(destination);
        desktop.connect(desktopGain).connect(destination);

        new_stream.addTrack(destination.stream.getAudioTracks()[0]);
      } else {
        new_stream.addTrack(audio_stream.getAudioTracks()[0]);
      }

      let producer = "";
      let audioProducer = "";

      console.log(this.producerTransport);
      producer = await this.producerTransport.produce({
        track: stream.getVideoTracks()[0],
      });
      this.socket.request("set_producer_type", {
        producer_id: producer.id,
        producer_type: "screen_record_video",
      });
      audioProducer = await this.producerTransport.produce({
        track: stream.getAudioTracks()[0],
      });
      this.socket.request("set_producer_type", {
        producer_id: audioProducer.id,
        producer_type: "screen_record_audio",
      });

      this.producers.set(producer.id, producer);
      this.producers.set(audioProducer.id, producer);
      this.producerLabel.set("screen_record_audio", audioProducer.id);
      this.producerLabel.set("screen_record_video", producer.id);

      console.log(this.producerLabel);
      producer.on("trackended", () => {
        this.closeProducer("screen_record_video");
      });
      producer.on("transportclose", () => {
        this.producers.delete(producer.id);
      });
      producer.on("close", () => {
        console.log("closing producer");
        this.producers.delete(producer.id);
      });

      audioProducer.on("trackended", () => {
        this.closeProducer("screen_record_video");
      });
      audioProducer.on("transportclose", () => {
        this.producers.delete(producer.id);
      });
      audioProducer.on("close", () => {
        console.log("closing producer");
        this.producers.delete(producer.id);
      });

      this.setReccordState(!this.recordState);
      // }
      // }
    } catch (err) {
      console.log(err);
      if (err.message.includes("Permission denied")) {
        console.log(this.first);
        this.setCamera(false);
        this.setFirst(false);
        console.log(this.first);
        if (this.Recorder) {
          this.produceForScreenRecord(deviceId);
        } else {
          throw new Error("ulala");
        }
      }
    }
  }
  async recordFunction(data) {
    console.log(this.producerLabel);

    let producer_id1 = this.producerLabel.get("screen_record_video");
    let producer_id2 = this.producerLabel.get("screen_record_audio");
    console.log(producer_id1 + "---" + producer_id2);
    let producers = [producer_id1, producer_id2];
    socket.request("start_record", {data, producers}, (result) => {
      console.log(result);
    });
  }
  async stopRecording() {
    socket.request("stop_record");
    this.closeProducer("screen_record_video");
    this.closeProducer("screen_record_audio");
  }

  async getConsumeStream(producerId) {
    const {rtpCapabilities} = this.device;
    const data = await this.socket.request("consume", {
      rtpCapabilities,
      consumerTransportId: this.consumerTransport.id, // might be
      producerId,
    });
    const {id, kind, rtpParameters} = data;

    let codecOptions = {};
    const consumer = await this.consumerTransport.consume({
      id,
      producerId,
      kind,
      rtpParameters,
      codecOptions,
    });
    const stream = new MediaStream();
    stream.addTrack(consumer.track);
    return {
      consumer,
      stream,
      kind,
    };
  }
  pauseProducer(type) {
    console.log("in pause " + this.producerLabel);
    if (!this.producerLabel.has(type)) {
      console.log("there is no producer for this type " + type);
      return;
    }
    let producer_id = this.producerLabel.get(type);
    let data = {
      ...this.profileData,
      room_id: this.room_id,
      producer_id: producer_id,
      producer_type: type,
    };
    console.log(data);
    this.socket.request("paused_producer", data, (data) => {
      console.log("-----in pause producer function------" + data);
    });
    this.producers.get(producer_id).pause();
  }
  closeProducer(type) {
    if (!this.producerLabel.has(type)) {
      console.log("there is no producer for this type " + type);
      return;
    }
    console.log("in close producer");
    let producer_id = this.producerLabel.get(type);
    console.log(producer_id);
    this.socket.emit("producerClosed", {
      producer_id,
      ...this.profileData,
      room_id: this.room_id,
    });
    this.producers.get(producer_id).close();
    this.producers.delete(producer_id);
    this.producerLabel.delete(type);

    if (type !== mediaType.audio) {
      if (type === mediaType.screen) {
        this.setFirst(false);
        this.mainReferrence.current.srcObject
          .getTracks()
          .forEach(function (track) {
            track.stop();
          });
        console.log(this.userReference);
        this.mainReferrence.current.srcObject = this.userReference.current.srcObject;
      }
    }
    console.log("-------------closed producer----------------");
  }
  removeConsumer(consumer_id) {
    if (this.host) {
      this.mainReferrence.current.srcObject
        .getTracks()
        .forEach(function (track) {
          track.stop();
        });
      this.mainReferrence = "";
      console.log("in main reference");
    } else {
      let elem = document.getElementById(consumer_id);
      let elemone = document.getElementById(consumer_id + "one");
      if (elem === null) {
        return;
      }
      elem.srcObject.getTracks().forEach(function (track) {
        track.stop();
      });
      elem.remove();
      elemone.remove();
    }
    this.consumers.delete(consumer_id);
  }
  resumeProducer(type) {
    console.log("In resume" + this.producerLabel);

    if (!this.producerLabel.has(type)) {
      console.log("there is no producer for this type " + type);
      return;
    }
    let producer_id = this.producerLabel.get(type);
    this.socket.request(
      "resume_producer",
      {
        ...this.profileData,
        room_id: this.room_id,
        producer_id: producer_id,
        producer_type: type,
      },
      (data) => {
        console.log("-----in resume producer function------" + data);
      }
    );
    this.producers.get(producer_id).resume();
  }

  exit(offline = false) {
    console.log("---in exit---");
    this.socket.request("user_left", {
      ...this.profileData,
    });
    if (this.Host) {
      this.exitCall();
    }
    let clean = function () {
      console.log("---clean---");
      this.consumerTransport.close();
      this.producerTransport.close();
      this.socket.off("disconnect");
      this.socket.off("newProducers");
      this.socket.off("consumerClosed");
      this.redirectFunction();
    }.bind(this);

    if (!offline) {
      this.socket
        .request("exitRoom")
        .then((e) => console.log(e))
        .catch((e) => console.warn(e))
        .finally(
          function () {
            clean();
          }.bind(this)
        );
    } else {
      clean();
    }

    // this.event(_EVENTS.exitRoom);
  }

  static get mediaType() {
    return mediaType;
  }
}

const mediaType = {
  audio: "audioType",
  video: "videoType",
  screen: "screenType",
};
