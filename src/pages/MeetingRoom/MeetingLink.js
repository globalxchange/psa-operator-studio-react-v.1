import React, { useState } from "react";
import "srcstaticimagesCounselLogo.svg";
import MeetingImage from "../../static/images/MainImage.svg";
import { Link } from "react-router-dom";
import "./meeting.scss";
import CopyBtn from "../../static/images/copybtn.png";
import message from "antd/lib/message";
import { CopyToClipboard } from "react-copy-to-clipboard";
import uuid from "react-uuid";
import { CounselAppContext } from "../../Context_Api/Context";

const cpylink = (e) => {
  message.success("Link copied to clipboard");
};

const MeetingLink = () => {
  const [copy, setcopy] = useState(false);
  const [meetingLink, setMeetingLink] = useState("");
  const context = React.useContext(CounselAppContext);
  React.useEffect(() => {
    getMeetingID();
  }, []);
  const getMeetingID = () => {
    let id = uuid();
    setMeetingLink(`https://pulse.stream/joinMettingLink/?id=${id}`);
    context.setMeetingRoom(id);
  };
  return (
    <div className="meeting-ag">
      <div className="wrapper">
        <img src={MeetingImage} alt="no_img" />
        <p className="titletext">Here Is Your Meeting Room</p>
        <CopyToClipboard text={meetingLink} onCopy={() => setcopy(true)}>
          <button className="thelinkgen" onClick={cpylink}>
            {meetingLink}
            <img src={CopyBtn} alt="copybtn" className="thecpybtn" />
          </button>
        </CopyToClipboard>
        <Link to="/JoinMeeting" className="join-srt-btn">
          Enter Meeting 
        </Link>
        <div className="customize-link">
          <h5>
            <Link className="under-line">Click Here</Link> To Customize Your
            Link
          </h5>
        </div>
      </div>
    </div>
  );
};

export default MeetingLink;
