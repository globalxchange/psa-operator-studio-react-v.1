import React, { useContext } from 'react';
import { CounselAppContext } from '../../../Context_Api/Context';
import { arrow } from '../../../static/images/images';
import LoginSider from './Sider-Components/Sider-Login/LoginSider';
import MeetingImage from '../../../static/images/MainImage.svg';
import '../meeting.scss';
import SiderHome from './Sider-Components/Sider-Home/SiderHome';
const Sider = React.memo(() => {
  const context = useContext(CounselAppContext);
  const displayFunction = () => {
    switch (context.chatboxType) {
      case 'login':
        return <LoginSider setdisplayType={context.setChatBoxType} />;
      case 'home':
        return <SiderHome />;
      default:
        break;
    }
  };
  React.useEffect(() => {
    typeCheckFunction();
  }, []);
  const typeCheckFunction = () => {
    console.log('----Sider login----');
    if (localStorage.getItem('chatToken')) {
      context.setChatBoxType('home');
    } else {
      context.setChatBoxType('login');
    }
  };
  return (
    <div className={window.innerWidth < 600 ? 'Sider _100' : 'Sider '}>
      <div className='top-image'>
        {window.innerWidth < 600 ? (
          <img
            src={arrow}
            alt=''
            className='image-no'
            onClick={(e) => {
              context.setChatOpen(false);
            }}
          />
        ) : (
          ''
        )}
        <img src={MeetingImage} alt='' />
      </div>
      <p>Hosted by @{context.hostName}</p>
      <div className='chatbox'>{displayFunction()}</div>
    </div>
  );
});
export default Sider;
