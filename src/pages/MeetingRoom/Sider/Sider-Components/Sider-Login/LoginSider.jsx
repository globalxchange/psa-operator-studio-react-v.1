import React, {useState, useContext} from "react";
import message from "antd/lib/message";
import Axios from "axios";
import Fade from "react-reveal/Fade";
import {CounselAppContext} from "../../../../../Context_Api/Context";
import {gx, loginLoader} from "../../../../../static/images/images";
import "./LoginSider.scss";

const LoginSider = React.memo((props, setdisplayType) => {
  const [password, setPassword] = useState("");
  const {login, email} = useContext(CounselAppContext);
  const [mfaEnabled, setMfaEnabled] = useState(false);
  const [googlePin, setGooglePin] = useState("");
  const [loading, setLoading] = useState(false);
  const context = useContext(CounselAppContext);
  const [emailId, setemailId] = useState(context.email);

  const userLogin = async (e) => {
    e.preventDefault();
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailId)) {
      setLoading(true);
      if (mfaEnabled) {
        Axios.post("https://gxauth.apimachine.com/gx/user/login", {
          email: emailId,
          password: password,
          totp_code: googlePin,
        })
          .then((response) => {
            const {data} = response; 
            if (data.status) {
              let accessToken = data.accessToken;
              localStorage.setItem("access_token", accessToken);
              localStorage.setItem("user_account", emailId.toLowerCase());

              var idtoken = data.idToken;
              localStorage.setItem("chatToken", idtoken);

              if (context.accountArr.length > 0) { 
                localStorage.setItem(
                  "accountArr",
                  JSON.stringify([
                    ...context.accountArr,
                    {email: email, token: idtoken},
                  ])
                );
              } else {
                localStorage.setItem(
                  "accountArr",
                  JSON.stringify([{email: email, token: idtoken}])
                );
              }

              getNewData();

              context.setChatBoxType("home");
            } else {
              message.error(data.message);
            }
          })
          .finally(() => {
            setLoading(false);
          });
      } else {
        Axios.post("https://gxauth.apimachine.com/gx/user/login", {
          email: emailId,
          password: password,
        })
          .then((response) => {
            const {data} = response;
            if (data.status) {
              let accessToken = data.accessToken;
              localStorage.setItem("access_token", accessToken);
              localStorage.setItem("user_account", emailId.toLowerCase());

              var idtoken = data.idToken;
              localStorage.setItem("chatToken", idtoken);

              if (context.accountArr.length > 0) {
                localStorage.setItem(
                  "accountArr",
                  JSON.stringify([
                    ...context.accountArr,
                    {email: email, token: idtoken},
                  ])
                );
              } else {
                localStorage.setItem(
                  "accountArr",
                  JSON.stringify([{email: email, token: idtoken}])
                );
              }
              getNewData(data.user);
            } else if (data.mfa) {
              setMfaEnabled(true);
            } else {
              setLoading(false);
              message.error(data.message);
            }
          })
          .catch((error) => {
            message.error("Some Thing Went Wrong!");
            setLoading(false);
          });
      }
    } else {
      message.error("Enter Valid Email");
    }
  };
  const registerUserToApp = (item) => {
    Axios.post(`https://testchatsioapi.globalxchange.io/register_user`, {
      email: localStorage.getItem("user_account"),
      app_id: "559af22d-5e79-4948-acf6-e48733ee1daa",
    }).then((res) => {
      if (res.data.status) {
        message.success("Registered to Meeting.stream Chats");
        getNewData();
      }
    });
  };

  const getNewData = (item) => {
    Axios.post(
      `https://testchatsioapi.globalxchange.io/get_user`,
      {
        email: localStorage.getItem("user_account"),
        app_id: "559af22d-5e79-4948-acf6-e48733ee1daa",
      },
      {
        headers: {
          email: localStorage.getItem("user_account"),
          token: localStorage.getItem("chatToken"),
          app_id: "559af22d-5e79-4948-acf6-e48733ee1daa",
        },
      }
    ).then((res) => {
      if (!res.data.status) {
        handleNewSignup(item);
      } else {
        context.setUserObj(res.data.payload);
        console.log(res.data.payload);
        setLoading(false);
        context.setChatBoxType("home");
      }
    });
  };
  const handleNewSignup = (item) => {
    console.log(item);
    Axios.post(
      `https://testchatsioapi.globalxchange.io/register_with_chatsio`,
      {
        first_name: item.username, // required
        last_name: " ",
        username: item.username, // required
        bio: "Hey there, I am using Chats",
        email: localStorage.getItem("user_account"), // required
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        avatar: gx,
      }
    ).then((res) => {
      if (res.data.status) {
        registerUserToApp();
      } else {
        registerUserToApp();
        // message.error('Profile Updation failed');
        setLoading(false);
      }
    });
  };

  return (
    <div className="login-ag-sider">
      {loading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={loginLoader}
            alt="Loader"
            style={{alignSelf: "center"}}
            height="150px"
            width="150px"
          />
        </div>
      ) : (
        <div className="form-wrapper-sider">
          <form className="login-form-sider mx-auto" onSubmit={userLogin}>
            <Fade bottom>
              <div className="group">
                <input
                  type="text"
                  name="email"
                  value={emailId}
                  onChange={(e) => setemailId(e.target.value)}
                  required="required"
                />
                <span className="highlight" />
                <span className="bar" />
                <label>Email</label>
              </div>
            </Fade>
            <Fade bottom>
              <div className="group">
                <input
                  type="password"
                  name="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required="required"
                />
                <span className="highlight" />
                <span className="bar" />
                <label>Password</label>
              </div>
            </Fade>
            <Fade bottom>
              <button
                type="submit"
                disabled={context.loading}
                className="btn btn-darkblue mb-5"
                style={{fontFamily: "Montserrat"}}
              >
                <span>LOGIN</span>
                {/* {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'UNLOCK'} */}
              </button>
            </Fade>
          </form>
        </div>
      )}
    </div>
  );
});
export default LoginSider;
