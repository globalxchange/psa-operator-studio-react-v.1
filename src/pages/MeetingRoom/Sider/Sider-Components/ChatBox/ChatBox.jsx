import React, {useContext, useState} from "react";
import {Col, Badge, Input, Divider, Drawer, Popover, Avatar} from "antd";
import {CounselAppContext} from "../../../../../Context_Api/Context";
import Editor from "./Editor";
import AllChats from "./AllChats";
import {useEffect} from "react";
import {
  TeamOutlined,
  UserOutlined,
  MoreOutlined,
  SearchOutlined,
} from "@ant-design/icons";

const ChatBox = () => {
  const {
    allUserList,
    selectedGroup,
    selectedFriend,
    groupMembers,
    handleDroppedFile,
    onlineUsers,
    setChatBoxType,
  } = useContext(CounselAppContext);
  const [visibleGroupMembers, setVisibleGroupMembers] = useState(false);
  const [visibleAddMembers, setVisibleAddMembers] = useState(false);
  const [dragEntered, setDragEntered] = useState(false);
  const [selectedItem, setSelectedItem] = useState({});
  const [itemType, setItemType] = useState("");
  const [query, setQuery] = useState("");

  // let filteredUsersSearch = allUserList.filter((item) => {
  //   const lowquery = query.toLowerCase();
  //   return (
  //     (item.username + item.first_name).toLowerCase().indexOf(lowquery) >= 0
  //   );
  // });

  useEffect(() => {
    if (selectedGroup !== null) {
      setSelectedItem(selectedGroup);
      setItemType("group");
    }
  }, [selectedGroup]);

  const conditionalChatTitle = () => {
    if (selectedGroup !== null) {
      return (
        <>
          <TeamOutlined type="team" style={{color: "gray"}} />
          <span
            style={{
              margin: "0px",
              marginLeft: "5px",
              fontSize: "1vw",
              color: "grey",
            }}
          >
            {selectedGroup.group_name}
          </span>
        </>
      );
    }
  };

  return (
    <Col
      className="hidden-sidebar gray-scroll"
      style={{
        height: "100%",
        textAlign: "left",
        width: "100% !important",
        maxWidth: "100% !important",
        background:
          localStorage.getItem("mode") === "light"
            ? "white"
            : localStorage.getItem("mode") === "dark"
            ? "#202020"
            : "",
      }}
    >
      <>
        <div
          style={{
            padding: "20px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            paddingTop: "20px",
            background:
              localStorage.getItem("mode") === "light"
                ? "white"
                : localStorage.getItem("mode") === "dark"
                ? "#2E2E2E"
                : "",
          }}
        >
          <div style={{display: "flex", alignItems: "center"}}>
            <span
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color:
                  localStorage.getItem("mode") === "light"
                    ? "black"
                    : localStorage.getItem("mode") === "dark"
                    ? "lightgray"
                    : "",
              }}
            >
              {conditionalChatTitle()}
            </span>
          </div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            {selectedGroup !== null ? (
              <div
                style={{color: "#A4A4A4", cursor: "pointer"}}
                onClick={(e) => setVisibleGroupMembers(true)}
              >
                <UserOutlined /> {groupMembers.length}
              </div>
            ) : (
              ""
            )}

            <Popover
              placement="bottomRight"
              content={
                <>
                  <span
                    style={{cursor: "pointer"}}
                    onClick={(e) => {
                      localStorage.removeItem("chatToken");
                      setChatBoxType("login");
                    }}
                  >
                    Logout
                  </span>
                </>
              }
              trigger="click"
            >
              <MoreOutlined
                style={{
                  fontSize: "20px",
                  color: "#A4A4A4",
                  cursor: "pointer",
                }}
              />
            </Popover>
          </div>
        </div>
        <Divider
          style={{
            margin: "0px",
            background:
              localStorage.getItem("mode") === "light"
                ? "lightgray"
                : localStorage.getItem("mode") === "dark"
                ? "#2E2E2E"
                : "",
          }}
        />
        <div
          style={{
            border: !dragEntered ? "none" : "2px dashed lightgray",
            height: "86.5%",
          }}
          onDragEnter={(e) => setDragEntered(true)}
          onDragLeave={(e) => setDragEntered(false)}
          onDropCapture={(e) => {
            setDragEntered(false);
          }}
          onDragOver={(e) => {
            e.preventDefault();
            e.stopPropagation();
          }}
          onDrop={handleDroppedFile}
        >
          {dragEntered ? (
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                fontSize: "20px",
                color: "lightgray",
                height: "78vh",
              }}
            >
              <b>Drop</b>the file here
            </div>
          ) : (
            <div style={{height: "100%"}}>
              <AllChats />
            </div>
          )}
        </div>

        <Editor />
      </>

      <Drawer
        bodyStyle={{padding: "0px"}}
        title="Group Members"
        placement={"right"}
        closable={false}
        onClose={(e) => setVisibleGroupMembers(false)}
        visible={visibleGroupMembers}
        height="100vh"
        width="30vw"
        style={{
          boxShadow: "3px 0px 7px 1px !important",
          border: "1px solid #f0f0f0",
        }}
      >
        {groupMembers.map((item, index) => {
          return (
            <div
              key={index}
              className={
                selectedFriend === item.username
                  ? "selected-friend"
                  : "friend-name"
              }
            >
              <Avatar src={item.avatar} />

              {onlineUsers.indexOf(item) !== -1 ? (
                <Badge color={"#70CC16"} />
              ) : (
                <Badge color={"lightgray"} />
              )}
              <span>
                {item.first_name}
                {item.last_name}
              </span>
            </div>
          );
        })}
      </Drawer>

      {/* Add User to Group */}

      <Drawer
        autoFocus
        width={window.innerWidth > 600 ? 380 : window.innerWidth}
        placement="right"
        title="Add User to Group"
        onClose={(e) => setVisibleAddMembers(false)}
        visible={visibleAddMembers}
      >
        <Input
          autoFocus
          size="large"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          type="text"
          style={{marginBottom: "10px"}}
          placeholder="Search User"
          suffix={<SearchOutlined />}
        />
      </Drawer>
    </Col>
  );
};

export default ChatBox;
