import React, {useContext, useEffect, useState} from "react";
import {Avatar, message, Menu, Dropdown, Divider, Empty} from "antd";
import {CounselAppContext} from "../../../../../Context_Api/Context";
// import ReactPlayer from "react-player";
import {LoadingOutlined, RollbackOutlined} from "@ant-design/icons";
const AllChats = () => {
  const {
    socket,
    bodyRef,
    messagearray,
    setmessagearray,
    setgroupMembers,
    selectedFriend,
    selectedGroup,
    selectedSupport,
    setSelectedReplyMessage,
    renderMessage,
    messageLoading,
    setMessageLoading,
    typingUser,
    setTypingUser,
    typingFlag,
    selectedType,
    setSelectedType,
    setTypingFlag,
    adminUsernames,
    app_id,
  } = useContext(CounselAppContext);
  const [pageNumber, setPageNumber] = useState(1);
  const [loading, setLoading] = useState(false);
  const [noMessages, setNoMessages] = useState(false);

  // useEffect(() => {
  //   if (selectedFriend) {
  //     socket.emit(
  //       'get_user_interaction_list_read_unread_list',
  //       userObj,
  //       app_id,
  //       async (data) => {
  //         console.log('ddddddddddddd', data);
  //         setcurrentUserList(data.interacting_list);
  //         let tempArr = [];
  //         await Promise.all(
  //           data.read_unread_list.map((item) => {
  //             tempArr.push(item.sender);
  //           })
  //         );
  //         console.log('fffff', tempArr);
  //         setuserReadUnreadList(tempArr);
  //         setUnreadInteractionList(data.newInteractionList);
  //         // setmessagearray([]);
  //         // setVisibleListUsers(false);
  //       }
  //     );
  //   }
  // }, [selectedFriend]);

  // useEffect(() => {
  //   if (selectedFriend !== null) {
  //     socket.emit(
  //       'check_user_interaction_list',
  //       userObj,
  //       selectedFriend.username,
  //       app_id,
  //       (response) => {
  //         setMessageLoading(false);
  //         console.log('chat_history', response);
  //         let chat_history = [...response];
  //         setmessagearray([...chat_history.reverse()]);
  //       }
  //     );
  //   }
  // }, [selectedFriend]);

  useEffect(() => {
    console.log("selected group", selectedGroup);
    if (selectedGroup) {
      if (selectedGroup.thread_id) {
        socket.emit(
          "get_group_chat_history",
          selectedGroup.group_name,
          selectedGroup.thread_id,
          app_id,
          (resp) => {
            setMessageLoading(false);
            console.log(resp, "chat_history");
            setmessagearray([...resp.reverse()]);
          }
        );
        socket.emit("get_group_members", selectedGroup, app_id, (response) => {
          console.log(response, "resssss");
          setgroupMembers([...response]);
        });
      }
    }
  }, [selectedGroup]);

  // Msg Notify.. set messagearray
  useEffect(() => {
    if (selectedGroup !== null) {
      socket.on("msg_notify", (data) => {
        // console.log(data);
        if (data.thread_id === selectedGroup.thread_id) {
          // console.log(messagearray);
          if (messagearray[messagearray.length - 1] !== data) {
            setmessagearray([...messagearray, data]);
          }
        }
      });
      return () => {
        socket.off("msg_notify");
      };
    }
  }, [selectedFriend, messagearray, selectedSupport]);

  useEffect(() => {
    if (selectedGroup !== null) {
      setSelectedType(selectedGroup);
    }
  }, [selectedGroup]);

  //useeffect to notify that other party which the current user is interaction is typing
  useEffect(() => {
    if (selectedType) {
      socket.on("someone_typing", (user, thread_id1) => {
        console.log("typing thread: ", thread_id1);
        if (thread_id1 === selectedType.thread_id) {
          console.log("typing");
          setTypingUser(user);
          setTypingFlag(true);
          setTimeout(() => {
            setTypingFlag(false);
          }, 1800);
        } else {
          setTypingFlag(false);
        }
      });
    }

    return () => {
      socket.off("someone_typing");
    };
  }, [selectedType]);
  const conditionalShowOldMessages = () => {
    console.log("condotional");
    if (selectedGroup !== null) {
      showGroupOldMessage(selectedGroup.thread_id);
    }
  };

  const showGroupOldMessage = (threadId) => {
    setLoading(true);
    console.log("closed");
    console.log("something", threadId, pageNumber);
    setPageNumber(pageNumber + 1);
    socket.emit(
      "groupchat_older_messages",
      JSON.stringify({thread_id: threadId, page: pageNumber + 1}),
      (response) => {
        if (response !== "error") {
          console.log("chat_history", response);
          let chat_history = [...response].reverse();
          if (!chat_history.length > 0) {
            setNoMessages(true);
          }
          console.log("something", chat_history);
          setmessagearray([...chat_history, ...messagearray]);
          console.log([...chat_history, ...messagearray]);
          // setsetFlag(true);
          setLoading(false);
        } else {
          message.error("Fetching older messages failed", 3);
          setLoading(false);
        }
      }
    );
  };

  //   message object sample

  // message: "ok"
  // sender: "techkrtk"
  // thread_id: "8b35c5c1-f39c-4121-9915-d36a2dc587a7"
  // timestamp: 1590123118442
  // _id: "5ec75a6df019376c423f52d5"

  const conditionalMessageView = (item) => {
    // console.log("messageview", item);
    if (!item.replied_msg) {
      return renderMessage(item);
    } else {
      return (
        <>
          <div>{renderMessage(item)}</div>
          <div
            style={{
              borderLeft: "2px solid gray",
              paddingLeft: "10px",
              paddingTop: "5px",
              marginTop: "3px",
            }}
          >
            <div style={{display: "flex"}}>
              <Avatar size="small" src={item.replied_to_data.sender.avatar} />

              <div>
                <span style={{fontWeight: "bold"}}>
                  {selectedSupport !== null
                    ? adminUsernames.length > 0
                      ? adminUsernames.indexOf(
                          item.replied_to_data.sender.username
                        ) > -1
                        ? "Support"
                        : item.replied_to_data.sender.username
                      : ""
                    : item.replied_to_data.sender.username}
                </span>{" "}
                <span style={{color: " #8D8D8D"}}>
                  <small>
                    {new Date(item.replied_to_data.timestamp).toLocaleString()}
                  </small>
                </span>
              </div>
            </div>

            <div style={{padding: "5px 0px"}}>
              {renderMessage(item.replied_to_data)}
            </div>
          </div>
        </>
      );
    }
  };

  useEffect(() => {
    setNoMessages(false);
    setPageNumber(1);
  }, [selectedGroup]);

  useEffect(() => {
    if (messagearray.length > 1) {
      if (pageNumber === 1) {
        var objDiv = document.getElementById("your_div");
        console.log("top: ", objDiv.scrollTop, "height: ", objDiv.scrollHeight);
        objDiv.scrollTop = objDiv.scrollHeight;
        console.log(objDiv);
      }
    }
  }, [messagearray, typingFlag]);

  const showConditionalDate = (item, index, prevItem) => {
    if (index > 0) {
      if (
        new Date(item.timestamp).toDateString() !==
        new Date(prevItem.timestamp).toDateString()
      ) {
        return (
          <Divider
            style={{
              color:
                localStorage.getItem("mode") === "dark" ? "gray" : "lightgray",
            }}
          >
            {new Date(item.timestamp).toDateString()}
          </Divider>
        );
      } else {
        return null;
      }
    } else {
      return (
        <Divider
          className={
            localStorage.getItem("mode") === "dark"
              ? "divider-dark"
              : "divider-light"
          }
          style={{
            color:
              localStorage.getItem("mode") === "dark" ? "gray" : "lightgray",
          }}
        >
          {new Date(item.timestamp).toDateString()}
        </Divider>
      );
    }
  };

  const showConditionalShowOldMessageButton = () => {
    if (messagearray.length > 29) {
      if (!noMessages) {
        return (
          <div
            style={{
              textAlign: "center",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
            onClick={(e) => conditionalShowOldMessages()}
          >
            <div
              style={{
                backgroundColor: "#EEEEEE",
                cursor: "pointer",
                borderRadius: "50px",
                padding: "8px",
              }}
            >
              {loading ? <LoadingOutlined /> : ""} Show Older Messages
            </div>
          </div>
        );
      }
    }
  };

  return (
    <>
      <div
        // className="gray-scroll"
        className={
          localStorage.getItem("mode") === "dark"
            ? "hidden-sidebar black-scroll2"
            : "hidden-sidebar white-scroll"
        }
        id="your_div"
        style={{
          padding: "20px",
          paddingBottom: "60px",
          overflowY: "scroll",
          height: "100%",
        }}
      >
        {showConditionalShowOldMessageButton()}
        {!messageLoading ? (
          messagearray.length > 0 ? (
            messagearray.map((item, index) => {
              return (
                <div key={index}>
                  {showConditionalDate(item, index, messagearray[index - 1])}
                  <Dropdown
                    overlay={
                      <Menu>
                        <Menu.Item
                          key="0"
                          onClick={(e) => {
                            setSelectedReplyMessage(item);
                            bodyRef.current.focus();
                          }}
                        >
                          <RollbackOutlined />
                          Quote & Reply
                        </Menu.Item>
                        <Menu.Divider />
                        <Menu.Item key="3">Dismiss</Menu.Item>
                      </Menu>
                    }
                    trigger={["contextMenu"]}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                      className={
                        localStorage.getItem("mode") === "dark"
                          ? "each-chat-dark"
                          : "each-chat"
                      }
                    >
                      <div style={{display: "flex", padding: "10px 0px"}}>
                        <div>
                          <Avatar src={item.sender.avatar} />
                        </div>
                        <div style={{paddingLeft: "10px"}}>
                          <div>
                            <span
                              style={{
                                fontWeight: "bold",
                                color:
                                  localStorage.getItem("mode") === "dark"
                                    ? "white"
                                    : "",
                              }}
                            >
                              {selectedSupport !== null
                                ? adminUsernames.length > 0
                                  ? adminUsernames.indexOf(
                                      item.sender.username
                                    ) > -1
                                    ? "Support"
                                    : item.sender.username
                                  : ""
                                : item.sender.username}
                            </span>{" "}
                            <span style={{color: " #8D8D8D"}}>
                              <small>
                                {new Date(item.timestamp).toLocaleString()}
                              </small>
                            </span>
                          </div>
                          <div
                            style={{
                              paddingTop: "5px",
                              color:
                                localStorage.getItem("mode") === "dark"
                                  ? "white"
                                  : "",
                            }}
                          >
                            {conditionalMessageView(item)}
                          </div>
                        </div>
                      </div>
                    </div>
                  </Dropdown>
                </div>
              );
            })
          ) : (
            <Empty />
          )
        ) : (
          <LoadingOutlined />
        )}
      </div>
      <div
        style={{
          padding: "20px",
          paddingBottom: "60px",
          overflowY: "scroll",
          marginTop: "-15vh",
        }}
      >
        {typingFlag ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              padding: "5px 25px",
              borderRadius: "20px",
              textAlign: "justify",
              color: "gray",
            }}
          >
            {typingUser}is typing
            <div class="ticontainer">
              <div class="tiblock">
                <div class="tidot"></div>
                <div class="tidot"></div>
                <div class="tidot"></div>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default AllChats;
