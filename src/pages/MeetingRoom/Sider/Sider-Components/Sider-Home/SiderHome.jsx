import {message} from "antd";
import Axios from "axios";
import React, {useEffect, useContext, useState} from "react";
import ioClient from "socket.io-client";
import {CounselAppContext} from "../../../../../Context_Api/Context";
import {gx} from "../../../../../static/images/images";
import ChatBox from "../ChatBox/ChatBox";

let socket;
const SiderHome = React.memo(() => {
  const {
    userObj,
    setUserObj,
    setChatBoxType,
    meetingRoom,
    app_id,
    setSelectedGroup,
    setallUserList,
    selectedGroup,
    host,
    groupReadUnreadList,
    setgroupReadUnreadList,
    setOnlineUsers,
    getMeetingDetails,
  } = useContext(CounselAppContext);
  const {setSocket} = useContext(CounselAppContext);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    console.log("-----in socket connection------");
    socket = ioClient("https://testsockchatsio.globalxchange.io", {
      reconnection: false,
      query: {
        email: localStorage.getItem("user_account"),
        token: localStorage.getItem("chatToken"),
        tokfortest: "nvestisgx",
      },
    });
    setSocket(socket);
    getMeetingDetails();
  }, []);

  useEffect(() => {
    if (!localStorage.getItem("mode")) {
      localStorage.setItem("mode", "light");
    }
    if (!localStorage.getItem("theme")) {
      localStorage.setItem("theme", "rocket-chat");
    }
  }, []);
  useEffect(() => {
    socket.emit("get_online_user_list", (response) => {
      console.log(response);
      setOnlineUsers([...response]);
    });
    return () => {
      socket.off("get_online_user_list");
    };
  }, []);
  useEffect(() => {
    const handleRedirect = async () => {
      if (
        localStorage.getItem("chatToken") &&
        localStorage.getItem("chatToken").length > 0
      ) {
        var dd = await Axios.post(
          "https://comms.globalxchange.com/brokerage/verify_token",
          {
            token: localStorage.getItem("chatToken"),
            userpoolid: "us-east-2_ALARK1RAa",
          }
        );
        var user;
        if (dd.data) user = dd.data;
        else user = null;

        if (user !== null && Date.now() < user.exp * 1000) {
          console.log("bhai tu andar he");
          getNewData(user);
        } else {
          localStorage.setItem("chatToken", "");
          localStorage.setItem("access_token", "");
          localStorage.setItem("user_account", "");
          localStorage.setItem("accountArr", "");
          setChatBoxType("login");
        }
      } else {
        console.log("else block token null");
      }
    };
    handleRedirect();
  }, []);
  useEffect(() => {
    socket.emit(
      "get_all_user_list",
      "559af22d-5e79-4948-acf6-e48733ee1daa",
      (data) => {
        setallUserList(data);
        console.log("all userssss", data);
      }
    );

    return () => {
      socket.off("get_all_user_list");
    };
  }, []);
  useEffect(() => {
    if (groupReadUnreadList.length > 0) {
      setSelectedGroup(groupReadUnreadList[0]);
      setLoading(true);
    }
  }, [groupReadUnreadList]);

  const registerUserToApp = (item) => {
    Axios.post(`https://testchatsioapi.globalxchange.io/register_user`, {
      email: localStorage.getItem("user_account"),
      app_id: "559af22d-5e79-4948-acf6-e48733ee1daa",
    }).then((res) => {
      if (res.data.status) {
        message.success("Registered to Meeting.stream Chats");
        getNewData();
      }
    });
  };

  const handleNewSignup = (item) => {
    console.log(item);
    Axios.post(
      `https://testchatsioapi.globalxchange.io/register_with_chatsio`,
      {
        first_name: item.username, // required
        last_name: " ",
        username: item.username, // required
        bio: "Hey there, I am using Chats",
        email: localStorage.getItem("user_account"), // required
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        avatar: gx,
      }
    ).then((res) => {
      if (res.data.status) {
        registerUserToApp();
      } else {
        message.error("Profile Updation failed");
        // setLoading(false);
      }
    });
  };

  const getNewData = (item) => {
    console.log("------in get data------");
    Axios.post(
      `https://testchatsioapi.globalxchange.io/get_user`,
      {
        email: localStorage.getItem("user_account"),
        app_id: "559af22d-5e79-4948-acf6-e48733ee1daa",
      },
      {
        headers: {
          email: localStorage.getItem("user_account"),
          token: localStorage.getItem("chatToken"),
          app_id: "559af22d-5e79-4948-acf6-e48733ee1daa",
        },
      }
    ).then((res) => {
      if (!res.data.status) {
        handleNewSignup(item);
      } else {
        console.log("-----about to register group------");
        console.log("about to print" + res.data.payload);
        setUserObj(res.data.payload);

        if (host) {
          console.log("----host---");
          registerGroup(false, res.data.payload);
        } else {
          console.log("----non-host---");

          registerGroup(true, res.data.payload);
        }
      }
    });
  };

  const addUserToGroup = (item) => {
    console.log(item, selectedGroup, "params");
    console.log(userObj);

    socket.emit(
      "add_user_to_group",
      selectedGroup.thread_id,
      userObj.username,
      app_id,
      (response) => {
        console.log(response);
      }
    );
  };

  const registerGroup = async (direct, data) => {
    console.log("----register group---" + direct);
    console.log("sssas" + data);
    if (direct) {
      socket.emit(
        "add_user_to_group",
        meetingRoom,
        data.username,
        app_id,
        (response) => {
          console.log(response);
        }
      );

      setTimeout(() => {
        socket.emit(
          "get_user_group_interaction_list",
          data,
          app_id,
          (response) => {
            console.log("group", response);
            setgroupReadUnreadList([...response]);
            // setmessagearray([]);
          }
        );
      }, 1000);
    } else {
      console.log("------in register-group------");
      let group_data = {
        id: meetingRoom,
        group_name: meetingRoom,
        timestamp: Date.now(),
        creator: data.username,
      };
      console.log(data);
      socket.emit(
        "new_group_registration",
        group_data,
        data,
        app_id,
        (response) => {
          console.log(response, "newgroup");
          if (response) {
            console.log("----about to emit list----");
            socket.emit(
              "get_user_group_interaction_list",
              data,
              app_id,
              (response) => {
                console.log("group", response);
                setgroupReadUnreadList([...response]);
                addUserToGroup();

                // setmessagearray([]);
              }
            );
          }
        }
      );
    }
  };

  return <>{loading ? <ChatBox /> : ""} </>;
});
export default SiderHome;
