import React, { useContext } from 'react';
import 'srcstaticimagesCounselLogo.svg';
import MeetingImage from '../../static/images/MainImage.svg';
import { Link } from 'react-router-dom';
import { CounselAppContext } from '../../Context_Api/Context';

const MeetingRoom = (props) => {
  const context = useContext(CounselAppContext);
  console.log(context);
  return (
    <div className='login-ag'>
      <div className='wrapper'>
        <img src={MeetingImage} alt='no_img' />
        <Link
          to='/startmeeting'
          className='login-btn-ag'
          onClick={(e) => {
            context.setHost(true);  
          }}
        >
          Start A Meeting
        </Link>
        <Link
          to='/joinMettingLink'
          className='get-srt-btn'
          onClick={(e) => {
            context.setHost(false);
          }}
        >
          Join A Meeting
        </Link>
      </div>
    </div>
  );
};

export default MeetingRoom;
