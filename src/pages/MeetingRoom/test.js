function test() {
  for (let i = 8300; i < 9200; i++) {
    let n = i;
    let a = sumDigits(n);
    if (a === 5 || a === 3) {
      console.log(i);
    }
  }
}
function sumDigits(n) {
  if (n < 10) return n;
  return sumDigits((n % 10) + sumDigits(Math.floor(n / 10)));
}
test();
