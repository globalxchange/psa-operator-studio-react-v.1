import React from "react";
import "srcstaticimagesCounselLogo.svg";
import MeetingImage from "../../static/images/MainImage.svg";
import {Link} from "react-router-dom";
import "./meeting.scss";
import {CounselAppContext} from "../../Context_Api/Context";

function StartMeeting(props) {
  const context = React.useContext(CounselAppContext);
  return (
    <div className="meeting-ag">
      <div className="wrapper">
        <img src={MeetingImage} alt="no_img" />
        <p className="titletext">What Is The Name Of Your Meeting?</p>
        <input
          type="text"
          className="theinput"
          placeholder="Ex. Board Meeting With Alex"
          onChange={(e) => {
            context.setDescription(e.target.value);
          }}
        />
        <p className="titletext" style={{marginTop: "4vh"}}>
          Enter Your Name
        </p>
        <input
          type="text"
          className="theinput"
          placeholder="Ex. Alex"
          onChange={(e) => {
            context.setName(e.target.value);
          }}
        />
        <p className="linktitle" style={{marginTop: "8vh"}}>
          Who Is Able To Access The Meeting?
        </p>
        <p
          onClick={(e) => {
            if (context.Name.length === 0 || context.description.length === 0) {
              return;
            } else {
              props.history.push("/meetinglink");
            }
          }}
          className="meeting-btn-ag"
        >
          Anyone With Link
        </p>
        <p
          onClick={(e) => {
            if (context.Name.length === 0 || context.description.length === 0) {
              return;
            } else {
              props.history.push("/generatelink");
            }
          }}
          className="meeting-btn-ag"
        >
          Set Password
        </p>
      </div>
    </div>
  );
}

export default StartMeeting;
