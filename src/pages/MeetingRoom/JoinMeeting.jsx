import React, {useState, useContext, useEffect} from "react";
import "srcstaticimagesCounselLogo.svg";
import MeetingImage from "../../static/images/MainImage.svg";
import "./meeting.scss";
import {CounselAppContext} from "../../Context_Api/Context";
import queryString from "query-string";
import {message} from "antd";
import {loginLoader} from "../../static/images/images";

const JoinMeeting = (props) => {
  const context = useContext(CounselAppContext);
  const [flag, setFlag] = useState(true);
  useEffect(() => {
    setFlag(true);
    let params = queryString.parse(props.location.search);
    if (params.roomID) {
      context.setMeetingRoom(params.roomID);
    } else {
      context.setMeetingRoom(params.id);
    }
    context.setMeetingLink(window.location.href);
    context.setHost(false);
    setFlag(false);
  }, []);

  return (
    <div className="meeting-ag">
      {flag ? (
        <div
          style={{
            height: "100vh",
            width: "100vw",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={loginLoader}
            alt="Loader"
            style={{alignSelf: "center", height: "10vw"}}
          />
        </div>
      ) : (
        <div className="wrapper">
          <img src={MeetingImage} alt="no_img" />
          <p className="titletext" style={{marginTop: "6vh"}}>
            Enter Your Name
          </p>
          <input
            type="text" 
            className="theinput"
            placeholder="Ex. Alex"
            onChange={(e) => {
              context.setName(e.target.value);
            }}
          />
          <p className="titletext" style={{marginTop: "4vh"}}>
            Enter Room Number
          </p>
          <input
            id="meetingRoomID"
            type="text"
            className="theinput"
            placeholder="Ex. 372123"
            value={context.meetingRoom}
            onChange={(e) => {
              context.setMeetingRoom(e.target.value);
            }}
            readOnly
          />

          <p className="titletext" style={{marginTop: "4vh"}}>
            Enter Your Email Id
          </p>
          <input
            type="text"
            className="theinput"
            placeholder="Ex. email@gmail.com"
            value={context.email}
            onChange={(e) => {
              context.setEmail(e.target.value);
            }}
          />

          <p
            className="get-btn"
            onClick={(e) => {
              if (
                context.Name.length !== 0 ||
                context.meetingRoom.length !== 0 ||
                context.email.length !== 0
              ) {
                props.history.push(
                  `/meetingroomLink/${context.meetingRoom}/${context.Name}/${context.email}`
                );
              } else {
                message.error("Please Enter all the Details");
              }
            }}
          >
            Join A Meeting
          </p>
        </div>
      )}
    </div>
  );
};
export default JoinMeeting;
