import {message} from "antd";
import Axios from "axios";
import React from "react";
import {CounselAppContext} from "../../Context_Api/Context";
import {loginLoader} from "../../static/images/images";
import Main from "./Main-Component/Main";
import "./meeting.scss";
import Sider from "./Sider/Sider";

const MeetingViewerComponent = (props) => {
  const context = React.useContext(CounselAppContext);
  const [flag, setFlag] = React.useState(true);

  React.useEffect(() => {
    setFlag(true);
    console.log("in meeting room link");
    context.setMeetingRoom(props.match.params.roomID);
    context.setName(props.match.params.Name);
    context.setHost(false);
    functionCall();
    // setFlag(false);
  }, []);

  const pushFunction = async () => {
    setFlag(true);
    try {
      let res = await Axios.get(
        `https://ms.apimachine.com/api/get_by_email/${props.match.params.emailID}`
      );
      if (res.data.success) {
        context.setProfileData(res.data.thefetch);
        context.getBrain();
        // props.history.push('/meetingroomLink');
        setFlag(false);
      } else {
        message.error("Please Register For meetings.stream");
        window.location.href = "https://studio.pulse.stream";
        setFlag(false);
      }
    } catch (e) {
      console.log(e);
      message.error(e.message);
      setFlag(false);
    }
  };
  const functionCall = async () => {
    await context.getBrain();
    // await getMeetingDetailsFunction();
    await pushFunction();
  };
  return (
    <>
      {" "}
      {flag ? (
        <div
          style={{
            height: "100vh",
            width: "100vw",

            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={loginLoader}
            alt="Loader"
            style={{alignSelf: "center", height: "10vw"}}
          />
        </div>
      ) : (
        <div className="meeting-Main">
          <div
            className={
              context.chatOpen
                ? window.innerWidth < 600
                  ? "_hidden"
                  : "Main _80"
                : "Main _100"
            }
          >
            <Main />
          </div>
          {context.chatOpen ? <Sider /> : ""}
        </div>
      )}
    </>
  );
};
export default MeetingViewerComponent;
