import React, { useContext, useEffect } from 'react';
import { CounselAppContext } from '../../Context_Api/Context';
// import Main from './Main-Component/Main';
import './meeting.scss';
import queryString from 'query-string';
import Sider from './Sider/Sider';
import Main from './Main-Component/Main';

const MeetingComponent = (props) => {
  const context = useContext(CounselAppContext);
  const [flag, setFlag] = React.useState(true);
  useEffect(() => {
    setFlag(true);
    let params = queryString.parse(props.location.search);
    // console.log(params.id);
    context.getBrain(); 
    let decodedString = atob(params.id);
    let decodedObj = JSON.parse(decodedString);
    console.log(decodedObj);
    context.setMeetingRoom(decodedObj.id);
    context.setName(decodedObj.name);
    context.setToken(decodedObj.token);
    context.setEmail(decodedObj.email);
    context.setProfileData(decodedObj.profileData);
    context.setSubDomainData(decodedObj.subDomainData);
    context.setPresentMeeting(decodedObj.presentMeeting);
    context.setMeetingLink(decodedObj.meetingLink); 
    context.setHost(decodedObj.host);
    setFlag(false);
  }, []);
  return (
    <>
      {flag ? (
        ''
      ) : (
        <div className='meeting-Main'>
          <div
            className={
              context.chatOpen
                ? window.innerWidth < 600
                  ? ''
                  : 'Main _80'
                : 'Main _100'
            }
          >
            <Main />{' '}
          </div>
          {context.chatOpen ? <Sider /> : ''}
        </div>
      )}
    </>
  );
}; 

export default MeetingComponent;
