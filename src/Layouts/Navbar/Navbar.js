import React, { useState, useContext, useEffect } from "react";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { Menu, Dropdown } from 'antd';
// import DraggableTab from "react-draggable-tab"
// import { faPlus, faCaretDown } from '@fortawesome/free-solid-svg-icons';
// import { faBell } from '@fortawesome/free-regular-svg-icons';
// import { ReactComponent as IconBoards } from '../../static/images/navbar-icons/boards.svg';
// import { ReactComponent as IconList } from '../../static/images/navbar-icons/list.svg';
import { CounselAppContext } from "../../Context_Api/Context";
// import { Vault } from '../../Context_Api/my-vault.context';
// import PulseTitleImg from "../../static/images/PulseTabIcon.png"
import blackSVG from "../../static/images/navbar-icons/blackPlus.svg";
import meetingimg from "../../static/images/meetingicon.png";
// import nextId from 'react-id-generator';
import { Drawer } from "antd";
import SwitchApps from "../../pages/Apps/switchapps";
import meeticon from "../../static/images/meeticon.png";
import { streamTriangle } from "../../static/images/images";

const getName = (active) => {
  switch (active) {
    case "customer-relations": {
      return "Manage";
    }
    case "appconfig": {
      return "App Config";
    }
    case "marketing": {
      return "Analytics";
    }
    case "CRM": {
      return "vaults";
    }
    case "management": {
      return "Dev Tools";
    }
    case "employees": {
      return "Stream Academy";
    }
    case "ITndData": {
      return "Documentation";
    }
    case "Admin": {
      return "Support";
    }
    case "myapps1": {
      return "Myapps 1";
    }
    case "addons": {
      return "Pulse Apps";
    }
    case "cryptolaw": {
      return "Crypto Law";
    }
    case "mycounsel": {
      return "My Counsel";
    }
    case "procounsel": {
      return "Pro Counsel";
    }
    case "regulator": {
      return "Regulator";
    }
    default: {
      return " Name Of Page";
    }
  }
};

function Navbar({ active }) {
  const [visible, setVisible] = useState(false);
  const context = useContext(CounselAppContext);
  // const [endselected, setEndselected] = useState('Toggle 1');
  const [tabSelected, setTabSelected] = useState("sub1");
  // const { profilePic } = useContext(CounselAppContext);

  // const [billsTab, setBillsTab] = useState([{
  //   key: 1,
  //   tabName: getName(active),
  //   currency: '',
  //   tab: 'pulse',
  //   imgTab: PulseTitleImg
  // }])
  // const [selectedTab, setSelectedTab] = useState(billsTab[billsTab.length - 1])
  // const [dashboardComponent, setDashboardComponent] = useState({
  //   key: '1',
  //   name: 'bills-full-search-lx',
  //   type: 'bills'
  // })
  // useEffect(() => {
  //   setTimeout(() => {
  //   }, 500);
  // }, [billsTab, selectedTab])

  // const thebillstab = (array) => {
  //   setBillsTab([...array], () =>
  //     console.log('bills tab', billsTab)
  //   );
  // }
  // const theSelectedTab = (obj) => {
  //   setSelectedTab(obj);

  // };
  // const handleAddTab = (data) => {
  //   console.log('data', data);
  //   let id = nextId();
  //   setDashboardComponent({
  //     name: data.dashboard,
  //     type: data.type,
  //     key: id,
  //   });
  //   if (data.type.length !== 0) {
  //     setSelectedTab({ key: id, ...data })
  //     thebillstab([...billsTab,
  //     {
  //       key: id,
  //       tabName: data.tabName,
  //       imgTab: data.imgTab,
  //       tab: data.dashboard,
  //     },
  //     ])
  //   }
  //   console.log(selectedTab)
  // }
  // const changeDashboardComponent = (obj) => {
  //   console.log('sel', JSON.stringify(selectedTab))
  //   console.log(obj.key, JSON.stringify(selectedTab))
  //   setDashboardComponent(obj, () =>
  //     console.log('x', dashboardComponent)
  //   );
  // };

  const onClose = async () => {
    setVisible(false);
  };
  const opentab = () => {
    setVisible(true);
  };
  // const handleSameTab = (data) => {
  //   console.log('data', data);
  //   changeDashboardComponent({
  //     name: data.dashboard,
  //     type: data.type,
  //     key: data.key,
  //   })
  //   setSelectedTab(data)
  //   let newDup = billsTab.map((item) => {
  //     if (item.key === data.key) {
  //       return {
  //         key: data.key,
  //         imgTab: data.imgTab,
  //         tab: data.dashboard,
  //         tabName: data.tabName,
  //       };
  //     }
  //   });
  //   setBillsTab(newDup)
  // }
  const removeTab = (k) => {
    // setSelectedTab('')
    console.log("key", context.thebilltab);
    if (context.thebilltab.length <= 1) return;
    let num = 0;
    let temp = context.thebilltab;
    console.log(temp);
    let updateTabs = temp.filter((x, i) => {
      if (x.key === k) {
        if (i === 0) num = 1;
        else num = i - 1;
      }
      return x.key !== k;
    });
    let dashboard = context.thebilltab[num].tab;
    context.theSelectedTab(context.thebilltab[num]);
    // setSelectedTab(billsTab[billsTab.length-1])
    context.changeDashboardComponent({
      key: "",
      name: dashboard,
      type: "bills",
    });
    context.billstab1(updateTabs);
    console.log(updateTabs);
  };
  const newTab = {
    tabName: getName(active),
    detail: "new-tab",
    img: meeticon,
    dashboard: "New-Tab",
    type: "New-Tab",
    imgTab: meeticon,
  };
  return (
    <nav>
      <div className="bets-dash-navbar">
        <div className="tabsplus-align">
          <div className="thetabs-align">
            {context.thebilltab.map((x) => (
              <div
                key={x.key}
                className={`tabs-styles ${
                  context.selectedTab.key === x.key ? "tabSelected" : ""
                }`}
              >
                <div
                  onClick={() => {
                    context.setSelectedTab(x);
                    context.changeDashboardComponent({
                      key: x.key,
                      name: x.tab,
                      type: "bills",
                    });
                  }}
                >
                  {x.imgTab.length === 0 ? (
                    ""
                  ) : (
                    <img
                      className={
                        context.selectedTab.key === x.key
                          ? "inverted"
                          : "tabsimg1"
                      }
                      src={x.imgTab}
                      alt="meetingimg"
                    />
                  )}
                  <h5
                    className={
                      context.selectedTab.key === x.key ? "theactiveclr" : ""
                    }
                  >
                    {x.tabName}
                  </h5>
                </div>
                <span
                  onClick={() => removeTab(x.key)}
                  style={{ zIndex: "auto" }}
                >
                  x
                </span>
              </div>
            ))}
          </div>

          <div
            style={{ backgroundColor: "white", color: "white" }}
            onClick={() => context.handleAddTab(newTab)}
          >
            <img className="tabsimg" src={blackSVG} alt="plusico" />
          </div>
        </div>

        <div className="vl1" />
        <Drawer
          width={360}
          placement="right"
          closable={false}
          onClose={onClose}
          visible={visible}
          style={{ marginTop: "-130px" }}
          maskStyle={{ opacity: "0" }}
          bodyStyle={{ marginBottom: "0px" }}
        >
          <SwitchApps />
        </Drawer>
        {/* <div className="notifications">
          <FontAwesomeIcon icon={faBell} />
          <div className="count">0</div>
        </div> */}
        <div className="thegoogle-ico">
          <img
            src={active === "deskstream" ? streamTriangle : meetingimg}
            onClick={opentab}
            style={{ margin: "7px 7px auto auto" }}
            alt=""
          />
        </div>
        {/* <div
          className="new-project"
        >
          <h5>
            <FontAwesomeIcon icon={faPlus} />
            <div className=" my-auto ml-2">New App</div>
          </h5>
        </div> */}
      </div>

      {/* <div className="nav-bottom">
        <div className="tab">
          <div
            className={'tab-itm ' + (tabSelected === 'sub1')}
            onClick={() => setTabSelected('sub1')}
          >
            <h6>Marketplace</h6>
          </div>
          <div
            className={'tab-itm ' + (tabSelected === 'sub2')}
            onClick={() => setTabSelected('sub2')}
          >
            <h6>My Services</h6>
          </div>
        </div>

        <div className="ml-auto boards active">
          <IconBoards />
          <div className="itm my-auto ml-2">View 1</div>
        </div>
        <div className="divider" />
        <div className="boards">
          <IconList />
          <div className="itm my-auto ml-2">View 2</div>
        </div>
      </div> */}
    </nav>
  );
}

export default Navbar;
