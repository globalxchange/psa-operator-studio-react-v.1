import React, {useContext} from "react";
import "./main-layout.scss";
import Sidebar from "./Sidebar/Sidebar";
import Navbar from "./Navbar/Navbar";
import {CounselAppContext} from "../Context_Api/Context";
import {Redirect} from "react-router-dom";
// import MainMeetingsDrawer from '../pages/Dashboard-Components/Dasboard-Meetings-Drawer/MainMeetingsDrawer';
// // import AllprofilesTab from '../pages/Profile-Components/AllprofilesTab';
// // import message from 'antd/lib/message';
import {loginLoader} from "../static/images/images";

function MainLayout({children, active}) {
  const {email, loading} = useContext(CounselAppContext);

  return (
    <div style={{minHeight: "100vh"}} className="main-layout">
      {email === "" ? <Redirect to="/landing" /> : ""}{" "}
      {email === "undefined" ? <Redirect to="/landing" /> : ""}{" "}
      {loading ? (
        <div
          style={{
            height: "100vh",
            width: "100vw",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <img
            src={loginLoader}
            alt="Loader"
            style={{alignSelf: "center", height: "10vw"}}
          />
        </div>
      ) : (
        <>
          <Sidebar active={active} />
          <div className="site-layout">
            <Navbar active={active} openFunc={() => {}} getTabNEnd={() => {}} />
            <div className="content-container">{children}</div>
          </div>
        </>
      )}
    </div>
  );
}

export default MainLayout;
