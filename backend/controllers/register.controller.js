const express =require('express');
const router= express.Router();
const RegisterService = require("../service/register.service")

//Add USER
router.post('/register/user', RegisterService.addUser);

//GET ALL USERS
router.get('/counselappuserslist',RegisterService.getUser);

//ADD CORPORATE PROFILE
router.post('/addcorporateprofile',RegisterService.AddCorporateProfile);

//ADD LEGAL FIRM
router.post('/addlegalfirm',RegisterService.AddLegalFirm);

//GIVE AUTHORITY AS REGULATOR
router.get('/addregulator',RegisterService.addRegulator);








module.exports=router;